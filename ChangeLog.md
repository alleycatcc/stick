## v0.6.1

*2024-04-15*

- Added more /*#__PURE__*/ annotations and now including comments in the bundles.
- No longer using @babel/env for es builds.

## v0.6.0

*2024-04-15*

- Updated export style internally.
- Removed all default exports.
- Fixed bug in `reverse` (accumulator continued accumulating, because
  point-free style doesn't currently work with our `reduce` family of
  functions.

## v0.5.3

*2024-02-25*

- Default empty object is now `Object.create (null)` instead of
  `Object.create (Object.freeze ({}))`. Both defend against prototype
  pollution, but the second form causes some libraries (like react-redux) to
  complain.

## v0.5.2

*2023-06-13*

- *Breaking*: `Object.prototype` is no longer frozen. This reverts the
  first breaking change of v0.5.1.

  Freezing `Object.prototype` is useful but leads to some surprising
  problems. For example you can no longer alter a property called `toString`
  on *any* object, because the original `toString` is a member of the frozen
  object. Even shadowing it is prohibited. (You can get around this by using
  `Object.defineProperty` instead, but this is pretty obscure and libraries
  will break left & right.)

## v0.5.1

*2023-06-13*

- Hardened against prototype pollution attacks.
- *Breaking*: `Object.prototype` is frozen, which will affect all objects,
  also outside this library.
- *Breaking*: `decorateException` removed (use `decorateRejection` from
  alleycat-js instead)

## v0.5.0

*2022-01-19*

- *Breaking*: all of the 'own' merge functions (the non-'in' variant) now internally use
  `Reflect.ownKeys` instead of a `for ... in` loop, so that they also properly handle symbol keys.
  Previously symbol keys were ignored, hence this is a breaking change.

## v0.4.8

*2021-07-11*

- Internal changes.
- Remove old chrome target from babel config.

## v0.4.7

*2021-07-09*

- Added extra paths to 'exports'.
  It seems eslint can't resolve named imports using this style:
      import { abc, } from 'stick-js'
  but with this style, it can:
      import { abc, } from 'stick-js/es'
  So we're explicitly allowing the paths
      /es
      /es/manual
      /es/map-manual
  using the 'exports' key in package.json, which eslint seems to ignore.
  Hopefully this mess'll all be cleaned up at some point.

## v0.4.6

*2021-07-09*

- Internal changes, docs.

## v0.4.0

*2020-07-13*

- Updated readme.

## v0.3.4

*2020-07-13*

- Added “main” field to package.json to support legacy common-js imports.

  Use like:
  
  `const { ... } = require ('stick-js')`

  or for specific modules:

  `const { ... } = require ('stick-js/cjs/manual.js')`

## v0.3.0

*2020-07-12*

- We now provide conditional dual packaging for esm/common-js style imports.
  If you use `import { ... } from 'stick-js'` you'll get esm-style modules,
  and if use `const { ... } = require ('stick-js')` you'll get common-js.
  Import them straight from 'stick-js' as it's no longer necessary to use
  a suffix (e.g. 'stick-js/es'). This is a potentially breaking change
  because if you use import statements then your runtime must be able to
  handle esm modules. You can use `node -r esm` or `webpack -r esm` if it
  can't. Node 14+ handles them natively (but it's still considered
  experimental).

## v0.2.1

*2020-07-05*

- The first argument to `factoryProps` can now be a function as well as an object.

## v0.2.0

*2020-05-16*

- `ifPredicateResults` and `whenPredicateResults` are now called `ifPredicateWithResults` and
  `whenPredicateWithResults`.
- Added `ifPredicateResults` and `whenPredicateResults`, where the condition functions take only one
  argument, the result of the predicate.
- Added `spreadTo` as a synonym for `passToN`.

## v0.1.0

*2020-05-16*

- `contains` is now called `containsV`.
- `contains` has been added, which takes a predicate in first position instead of a value.
- Added new functions `findIndex` and `findWithIndex`.

## v0.0.65

*2020*

- Added `and`, `andNot`, `or`, `orNot`.

## v0.0.64

*2020*

- Added `ifNil` and `whenNil`.

## v0.0.63

*2020*

- `isFunction` has been updated to now return true for 'Function', 'GeneratorFunction',
  'AsyncFunction', and 'Proxy', instead of only for 'Function'.
- Beginning of changelog.
