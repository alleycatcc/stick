var ref$, test, xtest, expectToEqual, expectToBe, bitwiseAnd, bitwiseOr, bitwiseXor, bitwiseNot, bitwiseLeft, bitwiseRight, bitwiseRightZeroFill, bitwiseLeftBy, bitwiseRightBy, bitwiseRightZeroFillBy;
ref$ = require('./common'), test = ref$.test, xtest = ref$.xtest, expectToEqual = ref$.expectToEqual, expectToBe = ref$.expectToBe;
ref$ = require('../cjs/index'), bitwiseAnd = ref$.bitwiseAnd, bitwiseOr = ref$.bitwiseOr, bitwiseXor = ref$.bitwiseXor, bitwiseNot = ref$.bitwiseNot, bitwiseLeft = ref$.bitwiseLeft, bitwiseRight = ref$.bitwiseRight, bitwiseRightZeroFill = ref$.bitwiseRightZeroFill, bitwiseLeftBy = ref$.bitwiseLeftBy, bitwiseRightBy = ref$.bitwiseRightBy, bitwiseRightZeroFillBy = ref$.bitwiseRightZeroFillBy;
describe('bitwise', function(){
  describe('bitwiseAnd', function(){
    test(1, function(){
      return expectToEqual(3)(
      bitwiseAnd(7, 3));
    });
    test('curried', function(){
      return expectToEqual(3)(
      bitwiseAnd(7)(
      3));
    });
  });
  describe('bitwiseOr', function(){
    test(1, function(){
      return expectToEqual(7)(
      bitwiseOr(7, 3));
    });
    test('curried', function(){
      return expectToEqual(7)(
      bitwiseOr(7)(
      3));
    });
  });
  describe('bitwiseXor', function(){
    test(1, function(){
      return expectToEqual(4)(
      bitwiseXor(7, 3));
    });
    test(2, function(){
      return expectToEqual(6)(
      bitwiseXor(7, 1));
    });
    test('curried', function(){
      return expectToEqual(4)(
      bitwiseXor(7)(
      3));
    });
  });
  describe('bitwiseNot', function(){
    var niet;
    niet = function(x){
      return -(x + 1);
    };
    test(1, function(){
      return expectToEqual(niet(7))(
      bitwiseNot(7));
    });
    test(2, function(){
      return expectToEqual(niet(4))(
      bitwiseNot(4));
    });
  });
  describe('bitwiseLeft', function(){
    test(1, function(){
      return expectToEqual(14)(
      bitwiseLeft(7, 1));
    });
    test(2, function(){
      return expectToEqual(20)(
      bitwiseLeft(5, 2));
    });
    test('curried', function(){
      return expectToEqual(14)(
      bitwiseLeft(7)(
      1));
    });
  });
  describe('bitwiseRight', function(){
    test(1, function(){
      return expectToEqual(7)(
      bitwiseRight(14, 1));
    });
    test(2, function(){
      return expectToEqual(5)(
      bitwiseRight(20, 2));
    });
    test(3, function(){
      return expectToEqual(-4)(
      bitwiseRight(-7, 1));
    });
    test('curried', function(){
      return expectToEqual(7)(
      bitwiseRight(14)(
      1));
    });
  });
  describe('bitwiseRightZeroFill', function(){
    test(1, function(){
      return expectToEqual(7)(
      bitwiseRightZeroFill(14, 1));
    });
    test(2, function(){
      return expectToEqual(5)(
      bitwiseRightZeroFill(20, 2));
    });
    test(3, function(){
      return expectToEqual(2147483644)(
      bitwiseRightZeroFill(-7, 1));
    });
    test('curried', function(){
      return expectToEqual(7)(
      bitwiseRightZeroFill(14)(
      1));
    });
  });
  describe('bitwiseLeftBy', function(){
    test(1, function(){
      return expectToEqual(14)(
      bitwiseLeftBy(1, 7));
    });
    test(2, function(){
      return expectToEqual(20)(
      bitwiseLeftBy(2, 5));
    });
    test('curried', function(){
      return expectToEqual(14)(
      bitwiseLeftBy(1)(
      7));
    });
  });
  describe('bitwiseRightBy', function(){
    test(1, function(){
      return expectToEqual(7)(
      bitwiseRightBy(1, 14));
    });
    test(2, function(){
      return expectToEqual(5)(
      bitwiseRightBy(2, 20));
    });
    test(3, function(){
      return expectToEqual(-4)(
      bitwiseRightBy(1, -7));
    });
    test('curried', function(){
      return expectToEqual(7)(
      bitwiseRightBy(1)(
      14));
    });
  });
  describe('bitwiseRightZeroFillBy', function(){
    test(1, function(){
      return expectToEqual(7)(
      bitwiseRightZeroFillBy(1, 14));
    });
    test(2, function(){
      return expectToEqual(5)(
      bitwiseRightZeroFillBy(2, 20));
    });
    test(3, function(){
      return expectToEqual(2147483644)(
      bitwiseRightZeroFillBy(1, -7));
    });
    test('curried', function(){
      return expectToEqual(7)(
      bitwiseRightZeroFillBy(1)(
      14));
    });
  });
  describe('combine', function(){
    test(1, function(){
      var i;
      i = 12345;
      return expectToEqual(bitwiseNot(i))(
      bitwiseXor(i)(
      -1));
    });
  });
});