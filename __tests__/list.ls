{
    test, xtest,
    expect-to-equal, expect-to-be,
    expect-not-to-be,
} = require './common'

{
    array, compact, compact-ok,
    list, last,
    reverse, reverse-m,
    head, tail,
} = require '../cjs/index'

describe 'list' !->
    test 1 ->
        list 3 4 5
        |> expect-to-equal [3 to 5]
    test 'empty' ->
        list()
        |> expect-to-equal []

describe 'compact*' !->
    mixed = [1 '' 0 '0' void false true 2]
    falsey = [false void null '' 0 NaN]
    truthy = [true '0' [] {} -1 Infinity]
    describe 'compact' !->
        test 1 ->
            mixed
            |> compact
            |> expect-to-equal [
                1 '0' true 2
            ]
        test 'all falsey' ->
            falsey
            |> compact
            |> expect-to-equal [
            ]
        test 'all truthy' ->
            truthy
            |> compact
            |> expect-to-equal truthy
    describe 'compactOk' !->
        test 1 ->
            mixed
            |> compact-ok
            |> expect-to-equal [
                1 '' 0 '0' false true 2
            ]
        test 'all falsey' ->
            falsey
            |> compact-ok
            |> expect-to-equal [
                false '' 0 NaN
            ]
        test 'all truthy' ->
            truthy
            |> compact-ok
            |> expect-to-equal truthy

describe 'reverse(M)' !->
    describe 'reverse' !->
        test 1 ->
            x = [1 2 3]
            y = reverse x
            y |> expect-to-equal [3 2 1]
        test 2 ->
            x = [1 2 3]
            y = reverse x
            y |> expect-to-equal [3 2 1]
    describe 'reverseM' !->
        test 1 ->
            x = [1 2 3]
            y = reverse-m x
            y |> expect-to-equal [3 2 1]
        test 2 ->
            x = [1 2 3]
            y = reverse-m x
            y |> expect-to-be x

describe 'last' !->
    x = [1 to 10]
    y = []
    test 1 -> x |> last |> expect-to-equal 10
    test 'undef on empty' -> y |> last |> expect-to-equal void

describe 'head, tail' !->
    x = [1 to 10]
    y = []
    test 'head' ->
        x |> head |> expect-to-equal 1
        y |> head |> expect-to-equal void
    test 'tail' ->
        x |> tail |> expect-to-equal [2 to 10]
        y |> tail |> expect-to-equal []
