var ref$, rMap, rTap, rFilter, rRepeat, rTimes, rJoin, rSplit, rReject, rZip, rSum, rEquals, rIdentical, rCurry, list, test, xtest, expectToEqual, expectToBe, expectNotToEqual, expectToThrow, expectNotToThrow, expectToBeInstanceOf, main, eq, ne, gt, gte, lt, lte, bindPropTo, bindProp, bindTo, bind, bindTryPropTo, bindTryProp, bindTryTo, bindTry, noop, stickNot, roll, recurry, tap, cascade, flip, flip3, flip4, flip5, sprintf1, sprintfN, letV, letNV, lets, lets2, lets3, lets4, lets5, lets6, letN, letS, zipAll, isType, getType, isFunction, isArray, isObject, isNumber, isRegExp, isBoolean, isString, isSymbol, isInteger, rangeFrom, rangeTo, rangeFromBy, rangeToBy, rangeFromByAsc, rangeFromByDesc, neu, neu1, neu2, neu3, neu4, neu5, neuN, xRegExp, xRegExpStr, xRegExpFlags, xMatch, xMatchStr, xMatchStrFlags, stickMatch, xMatchGlobal, xReplace, xReplaceStr, xReplaceStrFlags, ifReplace, ifXReplace, ifXReplaceStr, ifXReplaceStrFlags, blush, always, T, F, bindLatePropTo, bindLateProp, subtractFrom, subtract, minus, add, plus, multiply, divideBy, divideInto, modulo, moduloWholePart, toThe, repeatV, repeatF, repeatSide, timesV, timesF, timesSide, stickAnd, stickOr, andNot, orNot, sumAll, toString$ = {}.toString, slice$ = [].slice, arrayFrom$ = Array.from || function(x){return slice$.call(x);};
ref$ = require('ramda'), rMap = ref$.map, rTap = ref$.tap, rFilter = ref$.filter, rRepeat = ref$.repeat, rTimes = ref$.times, rJoin = ref$.join, rSplit = ref$.split, rReject = ref$.reject, rZip = ref$.zip, rSum = ref$.sum, rEquals = ref$.equals, rIdentical = ref$.identical, rCurry = ref$.curry;
ref$ = require('./common'), list = ref$.list, test = ref$.test, xtest = ref$.xtest, expectToEqual = ref$.expectToEqual, expectToBe = ref$.expectToBe, expectNotToEqual = ref$.expectNotToEqual, expectToThrow = ref$.expectToThrow, expectNotToThrow = ref$.expectNotToThrow, expectToBeInstanceOf = ref$.expectToBeInstanceOf;
ref$ = main = require('../cjs/index'), eq = ref$.eq, ne = ref$.ne, gt = ref$.gt, gte = ref$.gte, lt = ref$.lt, lte = ref$.lte, bindPropTo = ref$.bindPropTo, bindProp = ref$.bindProp, bindTo = ref$.bindTo, bind = ref$.bind, bindTryPropTo = ref$.bindTryPropTo, bindTryProp = ref$.bindTryProp, bindTryTo = ref$.bindTryTo, bindTry = ref$.bindTry, noop = ref$.noop, stickNot = ref$.not, roll = ref$.roll, recurry = ref$.recurry, tap = ref$.tap, cascade = ref$.cascade, flip = ref$.flip, flip3 = ref$.flip3, flip4 = ref$.flip4, flip5 = ref$.flip5, sprintf1 = ref$.sprintf1, sprintfN = ref$.sprintfN, letV = ref$.letV, letNV = ref$.letNV, lets = ref$.lets, lets2 = ref$.lets2, lets3 = ref$.lets3, lets4 = ref$.lets4, lets5 = ref$.lets5, lets6 = ref$.lets6, letN = ref$.letN, letS = ref$.letS, zipAll = ref$.zipAll, isType = ref$.isType, getType = ref$.getType, isFunction = ref$.isFunction, isArray = ref$.isArray, isObject = ref$.isObject, isNumber = ref$.isNumber, isRegExp = ref$.isRegExp, isBoolean = ref$.isBoolean, isString = ref$.isString, isSymbol = ref$.isSymbol, isInteger = ref$.isInteger, rangeFrom = ref$.rangeFrom, rangeTo = ref$.rangeTo, rangeFromBy = ref$.rangeFromBy, rangeToBy = ref$.rangeToBy, rangeFromByAsc = ref$.rangeFromByAsc, rangeFromByDesc = ref$.rangeFromByDesc, neu = ref$.neu, neu1 = ref$.neu1, neu2 = ref$.neu2, neu3 = ref$.neu3, neu4 = ref$.neu4, neu5 = ref$.neu5, neuN = ref$.neuN, xRegExp = ref$.xRegExp, xRegExpStr = ref$.xRegExpStr, xRegExpFlags = ref$.xRegExpFlags, xMatch = ref$.xMatch, xMatchStr = ref$.xMatchStr, xMatchStrFlags = ref$.xMatchStrFlags, stickMatch = ref$.match, xMatchGlobal = ref$.xMatchGlobal, xReplace = ref$.xReplace, xReplaceStr = ref$.xReplaceStr, xReplaceStrFlags = ref$.xReplaceStrFlags, ifReplace = ref$.ifReplace, ifXReplace = ref$.ifXReplace, ifXReplaceStr = ref$.ifXReplaceStr, ifXReplaceStrFlags = ref$.ifXReplaceStrFlags, blush = ref$.blush, always = ref$.always, T = ref$.T, F = ref$.F, bindLatePropTo = ref$.bindLatePropTo, bindLateProp = ref$.bindLateProp, subtractFrom = ref$.subtractFrom, subtract = ref$.subtract, minus = ref$.minus, add = ref$.add, plus = ref$.plus, multiply = ref$.multiply, divideBy = ref$.divideBy, divideInto = ref$.divideInto, modulo = ref$.modulo, moduloWholePart = ref$.moduloWholePart, toThe = ref$.toThe, repeatV = ref$.repeatV, repeatF = ref$.repeatF, repeatSide = ref$.repeatSide, timesV = ref$.timesV, timesF = ref$.timesF, timesSide = ref$.timesSide, stickAnd = ref$.and, stickOr = ref$.or, andNot = ref$.andNot, orNot = ref$.orNot;
sumAll = compose$(list, rSum);
describe('noop, not', function(){
  test('noop', function(){
    expectToEqual(void 8)(
    noop());
    expectToEqual(void 8)(
    noop(5));
    return expectToEqual(void 8)(
    noop([4]));
  });
  test('not', function(){
    expectToEqual(false)(
    stickNot(
    3));
    expectToEqual(false)(
    stickNot(
    true));
    expectToEqual(true)(
    stickNot(
    false));
    expectToEqual(true)(
    stickNot(
    ''));
    return expectToEqual(false)(
    stickNot(
    '0'));
  });
});
describe('recurry, roll', function(){
  var f, g, h;
  f = function(a){
    return function(b){
      return function(c){
        return function(d){
          return function(e){
            return function(f){
              return a + b + c + d + e + f;
            };
          };
        };
      };
    };
  };
  g = rCurry(function(a, b, c, d, e, f){
    return a + b + c + d + e + f;
  });
  h = function(a){
    return function(b){
      return function(c){
        return function(d){
          return a + b + c + d;
        };
      };
    };
  };
  describe('roll a manually curried function', function(){
    var r;
    r = roll(f);
    test(1, function(){
      return expectToEqual(33)(
      r(3, 4, 5, 6, 7, 8));
    });
    test('too few args, result can be called using manual style', function(){
      expectToEqual(33)(
      r(3, 4, 5, 6, 7)(8));
      return expectToEqual(33)(
      r(3, 4, 5)(6)(7)(8));
    });
    test('too few args, result can not be called using normal style', function(){
      return expectToBeInstanceOf(Function)(
      r(3, 4, 5)(6, 7, 8));
    });
    test('arity of rolled is 0 (not well-defined)', function(){
      return expectToEqual(0)(
      r.length);
    });
    test('arity of partial is always 1 (due to manual currying)', function(){
      return expectToEqual(1)(
      r(3, 4, 5).length);
    });
  });
  describe('roll a ramda curried function', function(){
    var r;
    r = roll(g);
    test(1, function(){
      return expectToEqual(33)(
      r(3, 4, 5, 6, 7, 8));
    });
    test('too few args, result can be called using manual style', function(){
      expectToEqual(33)(
      r(3, 4, 5, 6, 7)(8));
      return expectToEqual(33)(
      r(3, 4, 5)(6)(7)(8));
    });
    test('too few args, result can be called using normal style', function(){
      return expectToEqual(33)(
      r(3, 4, 5)(6, 7, 8));
    });
    test('arity works the normal way', function(){
      return expectToEqual(4)(
      r(3, 4).length);
    });
  });
  describe('recurry a manually curried function (not exhaustive)', function(){
    var r;
    r = recurry(6)(f);
    test('manual style', function(){
      return expectToEqual(33)(
      r(3)(4)(5)(6)(7)(8));
    });
    test('normal style', function(){
      expectToEqual(33)(
      r(3, 4, 5)(6, 7, 8));
      expectToEqual(33)(
      r(3, 4, 5)(6, 7)(8));
      return expectToEqual(33)(
      r(3)(4, 5)(6)(7, 8));
    });
    test('arity is 0 (not well-defined)', function(){
      return expectToEqual(0)(
      r.length);
    });
  });
  describe('recurry a manually curried function (exhaustive)', function(){
    var t;
    t = recurry(4)(h);
    test(1, function(){
      expectToEqual(18)(
      t(3)(4)(5)(6));
      expectToEqual(18)(
      t(3)(4, 5)(6));
      expectToEqual(18)(
      t(3)(4)(5, 6));
      expectToEqual(18)(
      t(3, 4)(5, 6));
      expectToEqual(18)(
      t(3, 4)(5)(6));
      expectToEqual(18)(
      t(3, 4, 5)(6));
      return expectToEqual(18)(
      t(3, 4, 5, 6));
    });
  });
  describe('recurry a manually curried function using too low arity, all combinations', function(){
    var t, f;
    t = recurry(3)(h);
    f = function(){
      return [t(3, 4, 5, 6), t(3, 4, 5)(6), t(3, 4)(5)(6), t(3, 4)(5, 6), t(3)(4, 5, 6), t(3)(4, 5)(6), t(3)(4)(5, 6), t(3)(4)(5)(6)];
    };
    expectToThrow(
    f);
  });
  describe('recurry a manually curried function using too high arity (n+1), all combinations', function(){
    var t;
    t = recurry(5)(h);
    test(1, function(){
      return expectNotToEqual([])(
      rReject(compose$((function(it){
        return toString$.call(it).slice(8, -1);
      }), (function(it){
        return it === 'Number';
      })))(
      [t(3)(4)(5)(6), t(3)(4, 5)(6), t(3)(4)(5, 6), t(3, 4)(5, 6), t(3, 4)(5)(6), t(3, 4, 5)(6), t(3, 4, 5, 6)]));
    });
  });
  describe('recurry a manually curried function using too high arity (n+2), all combinations', function(){
    var t;
    t = recurry(5)(h);
    test(1, function(){
      return expectNotToEqual([])(
      rReject(compose$((function(it){
        return toString$.call(it).slice(8, -1);
      }), (function(it){
        return it === 'Number';
      })))(
      [t(3)(4)(5)(6), t(3)(4, 5)(6), t(3)(4)(5, 6), t(3, 4)(5, 6), t(3, 4)(5)(6), t(3, 4, 5)(6), t(3, 4, 5, 6)]));
    });
  });
  describe('calling with empty arguments returns the function', function(){
    var f, r;
    f = function(x){
      return function(y){
        return function(z){
          return [x, y, z];
        };
      };
    };
    r = recurry(3)(f);
    test(1, function(){
      var g;
      g = r(1, 2);
      return expectToEqual([1, 2, 3])(
      g()(3));
    });
    test(2, function(){
      var g;
      g = r(1)(2);
      return expectToEqual([1, 2, 3])(
      g()(3));
    });
    test(3, function(){
      var g, args;
      g = r(1)(2);
      args = [];
      return expectToEqual([1, 2, 3])(
      g.apply(null, args)(3));
    });
  });
});
describe('tap', function(){
  var y, f;
  y = {
    y: []
  };
  f = bind$(y.y, 'push');
  expectToEqual(17)(
  (function(it){
    return it - 1;
  })(
  tap(f)(
  (function(it){
    return it * 3;
  })(
  tap(f)(
  (function(it){
    return it + 2;
  })(
  (function(it){
    return it * 2;
  })(
  2)))))));
  expectToEqual([6, 18])(
  y.y);
});
describe('comparisons', function(){
  describe('eq', function(){
    test(1, function(){
      expectToEqual(true)(
      eq(3)(
      3));
      return expectToEqual(false)(
      eq(3)(
      4));
    });
    test('unlike ramda equals', function(){
      expectToEqual(true)(
      rEquals([])(
      []));
      return expectToEqual(false)(
      eq([])(
      []));
    });
    test('unlike ramda identical', function(){
      expectToEqual(false)(
      rIdentical(-0)(
      0));
      return expectToEqual(true)(
      eq(-0)(
      0));
    });
  });
  describe('ne', function(){
    test(1, function(){
      expectToEqual(false)(
      ne(3)(
      3));
      return expectToEqual(true)(
      ne(3)(
      4));
    });
  });
  describe('gt', function(){
    test(1, function(){
      expectToEqual(true)(
      gt(2)(
      3));
      expectToEqual(false)(
      gt(3)(
      3));
      return expectToEqual(false)(
      gt(4)(
      3));
    });
  });
  describe('gte', function(){
    test(1, function(){
      expectToEqual(true)(
      gte(2)(
      3));
      expectToEqual(true)(
      gte(3)(
      3));
      return expectToEqual(false)(
      gte(4)(
      3));
    });
  });
  describe('lt', function(){
    test(1, function(){
      expectToEqual(false)(
      lt(2)(
      3));
      expectToEqual(false)(
      lt(3)(
      3));
      return expectToEqual(true)(
      lt(4)(
      3));
    });
  });
  describe('lte', function(){
    test(1, function(){
      expectToEqual(false)(
      lte(2)(
      3));
      expectToEqual(true)(
      lte(3)(
      3));
      return expectToEqual(true)(
      lte(4)(
      3));
    });
  });
});
describe('math', function(){
  test('subtract', function(){
    expectToEqual(2)(
    subtractFrom(5)(
    3));
    expectToEqual(2)(
    subtract(3)(
    5));
    return expectToEqual(2)(
    minus(3)(
    5));
  });
  test('add', function(){
    expectToEqual(8)(
    plus(5)(
    3));
    return expectToEqual(8)(
    add(5)(
    3));
  });
  test('multiply', function(){
    return expectToEqual(15)(
    multiply(5)(
    3));
  });
  test('divide', function(){
    expectToEqual(0.6)(
    divideBy(5)(
    3));
    return expectToEqual(2)(
    divideInto(6)(
    3));
  });
  test('modulo', function(){
    expectToEqual(1)(
    modulo(3)(
    10));
    expectToEqual(3)(
    moduloWholePart(3)(
    10));
    return expectToEqual(-3)(
    moduloWholePart(3)(
    -10));
  });
  test('exp', function(){
    return expectToEqual(100)(
    toThe(2)(
    10));
  });
});
describe('cascade', function(){
  test(1, function(){
    var odd;
    odd = function(x){
      return x % 2 !== 0;
    };
    return expectToEqual([2, 6, 10])(
    cascade([1, 2, 3, 4, 5], rFilter(odd), rMap((function(it){
      return it * 2;
    }))));
  });
});
describe('bind*', function(){
  var dog, dogSpeak, dogGarble;
  dog = {
    name: 'dog',
    speak: function(){
      return 'my name is ' + this.name;
    },
    garble: function(){
      var args, res$, i$, to$;
      res$ = [];
      for (i$ = 0, to$ = arguments.length; i$ < to$; ++i$) {
        res$.push(arguments[i$]);
      }
      args = res$;
      return rJoin('!', args);
    }
  };
  dogSpeak = dog.speak;
  dogGarble = dog.garble;
  describe('bind prop to', function(){
    test(2, function(){
      var f;
      f = bindPropTo(dog)(
      'speak');
      return expectToEqual('my name is dog')(
      f());
    });
    test('passes args', function(){
      var garble;
      garble = bindPropTo(dog)(
      'garble');
      return expectToEqual('a!1!c')(
      garble('a', 1, 'c'));
    });
    test('dies', function(){
      return expectToThrow(
      function(){
        return bindProp(dog)(
        'nothing');
      });
    });
  });
  describe('bind prop from', function(){
    test(2, function(){
      var f;
      f = bindProp('speak')(
      dog);
      return expectToEqual('my name is dog')(
      f());
    });
    test('passes args', function(){
      var garble;
      garble = bindProp('garble')(
      dog);
      return expectToEqual('a!1!c')(
      garble('a', 1, 'c'));
    });
    test('dies', function(){
      return expectToThrow(
      function(){
        return bindProp('nothing')(
        dog);
      });
    });
  });
  describe('bind func to', function(){
    test(2, function(){
      var f;
      f = bindTo(dog)(
      dogSpeak);
      return expectToEqual('my name is dog')(
      f());
    });
    test('passes args', function(){
      var garble;
      garble = bindTo(dog)(
      dogGarble);
      return expectToEqual('a!1!c')(
      garble('a', 1, 'c'));
    });
    test('dies', function(){
      return expectToThrow(
      function(){
        return bindTo(dog)(
        null);
      });
    });
  });
  describe('bind func from', function(){
    test(2, function(){
      var f;
      f = bind(dogSpeak)(
      dog);
      return expectToEqual('my name is dog')(
      f());
    });
    test('passes args', function(){
      var garble;
      garble = bind(dogGarble)(
      dog);
      return expectToEqual('a!1!c')(
      garble('a', 1, 'c'));
    });
    test('dies', function(){
      return expectToThrow(
      function(){
        return bind(null)(
        dog);
      });
    });
  });
  describe('blush, always, T, F', function(){
    test('blush', function(){
      var f;
      f = blush(
      42);
      expectToEqual(42)(
      f(1));
      expectToEqual(42)(
      f(null));
      expectToEqual(42)(
      f(1, 2, 3));
      return expectToEqual(42)(
      f([1, 2, 3]));
    });
    test('always', function(){
      return expectToEqual(always)(
      blush);
    });
    test('T', function(){
      var f;
      f = T;
      expectToEqual(true)(
      f(1));
      expectToEqual(true)(
      f(null));
      expectToEqual(true)(
      f(1, 2, 3));
      return expectToEqual(true)(
      f([1, 2, 3]));
    });
    test('F', function(){
      var f;
      f = F;
      expectToEqual(false)(
      f(1));
      expectToEqual(false)(
      f(null));
      expectToEqual(false)(
      f(1, 2, 3));
      return expectToEqual(false)(
      f([1, 2, 3]));
    });
  });
  describe('bind late', function(){
    test('prop to', function(){
      var obj2, bound;
      obj2 = {};
      bound = bindLatePropTo(obj2)(
      'speak');
      expect(function(){
        return bound();
      }).toThrow(TypeError);
      obj2.speak = function(){
        return 'spoke';
      };
      return expect(bound()).toEqual('spoke');
    });
    test('prop from', function(){
      var obj2, bound;
      obj2 = {};
      bound = bindLateProp('speak')(
      obj2);
      expect(function(){
        return bound();
      }).toThrow(TypeError);
      obj2.speak = function(){
        return 'spoke';
      };
      return expect(bound()).toEqual('spoke');
    });
  });
  describe('bind try *', function(){
    describe('bind try prop to', function(){
      test(1, function(){
        var f;
        f = bindTryPropTo(dog)(
        'speak');
        return expectToEqual('my name is dog')(
        f());
      });
      test('returns null on bad bind', function(){
        var f;
        f = bindTryPropTo(dog)(
        'bleak');
        return expectToEqual(null)(
        f);
      });
    });
    describe('bind try prop', function(){
      test(1, function(){
        var f;
        f = bindTryProp('speak')(
        dog);
        return expectToEqual('my name is dog')(
        f());
      });
      test('returns null on bad bind', function(){
        var f;
        f = bindTryProp('bleak')(
        dog);
        return expectToEqual(null)(
        f);
      });
    });
    describe('bind try func to', function(){
      test(1, function(){
        var f;
        f = bindTryTo(dog)(
        dogSpeak);
        return expectToEqual('my name is dog')(
        f());
      });
      test('returns null on bad bind', function(){
        var f;
        f = bindTryTo(dog)(
        null);
        return expectToEqual(null)(
        f);
      });
    });
    describe('bind try func', function(){
      test(1, function(){
        var f;
        f = bindTry(dogSpeak)(
        dog);
        return expectToEqual('my name is dog')(
        f());
      });
      test('returns null on bad bind', function(){
        var f;
        f = bindTry(null)(
        dog);
        return expectToEqual(null)(
        f);
      });
    });
  });
});
describe('flip', function(){
  describe('target manually curried', function(){
    var divide, divideAndAddThreeArgs, divideAndAddFourArgs, divideFlipped, divideAndAddThreeArgsFlipped, divideAndAddFourArgsFlipped;
    divide = function(a){
      return function(b){
        return a / b;
      };
    };
    divideAndAddThreeArgs = function(a){
      return function(b){
        return function(c){
          return a / b + c;
        };
      };
    };
    divideAndAddFourArgs = function(a){
      return function(b){
        return function(c){
          return function(d){
            return a / b + c + d;
          };
        };
      };
    };
    divideFlipped = flip(divide);
    divideAndAddThreeArgsFlipped = flip3(divideAndAddThreeArgs);
    divideAndAddFourArgsFlipped = flip4(divideAndAddFourArgs);
    test('init', function(){
      expect(divide(10)(5)).toEqual(2);
      expect(divideAndAddThreeArgs(10)(5)(1)).toEqual(3);
      return expect(divideAndAddFourArgs(10)(5)(1)(2)).toEqual(5);
    });
    describe('2 args', function(){
      test(1, function(){
        return expect(divideFlipped(10, 5)).toEqual(0.5);
      });
      test('result is curried', function(){
        return expect(divideFlipped(10)(5)).toEqual(0.5);
      });
    });
    describe('2 + 1 args', function(){
      test(1, function(){
        return expect(divideAndAddThreeArgsFlipped(10, 5, 1)).toEqual(1.5);
      });
      test('result is curried', function(){
        return expect(divideAndAddThreeArgsFlipped(10)(5)(1)).toEqual(1.5);
      });
      test('result is curried part deux', function(){
        return expect(divideAndAddThreeArgsFlipped(10, 5)(1)).toEqual(1.5);
      });
    });
    describe('2 + > 1 args', function(){
      test(1, function(){
        return expect(divideAndAddFourArgsFlipped(10, 5, 1, 2)).toEqual(3.5);
      });
      test('result is curried', function(){
        return expect(divideAndAddFourArgsFlipped(10)(5)(1)(2)).toEqual(3.5);
      });
      test('result is curried part deux', function(){
        return expect(divideAndAddFourArgsFlipped(10, 5, 1)(2)).toEqual(3.5);
      });
      test('result is curried part trois', function(){
        return expect(divideAndAddFourArgsFlipped(10, 5)(1, 2)).toEqual(3.5);
      });
      test('result is curried part quatre', function(){
        return expect(divideAndAddFourArgsFlipped(10)(5, 1, 2)).toEqual(3.5);
      });
    });
  });
  describe('target created with LS curry function', function(){
    var divide, divideAndAddThreeArgs, divideAndAddFourArgs, divideFlipped, divideAndAddThreeArgsFlipped, divideAndAddFourArgsFlipped;
    divide = curry$(function(a, b){
      return a / b;
    });
    divideAndAddThreeArgs = curry$(function(a, b, c){
      return a / b + c;
    });
    divideAndAddFourArgs = curry$(function(a, b, c, d){
      return a / b + c + d;
    });
    divideFlipped = flip(divide);
    divideAndAddThreeArgsFlipped = flip3(divideAndAddThreeArgs);
    divideAndAddFourArgsFlipped = flip4(divideAndAddFourArgs);
    test('init', function(){
      expect(divide(10)(5)).toEqual(2);
      expect(divideAndAddThreeArgs(10)(5)(1)).toEqual(3);
      return expect(divideAndAddFourArgs(10)(5)(1)(2)).toEqual(5);
    });
    describe('2 args', function(){
      test(1, function(){
        return expect(divideFlipped(10, 5)).toEqual(0.5);
      });
      test('result is curried', function(){
        return expect(divideFlipped(10)(5)).toEqual(0.5);
      });
    });
    describe('2 + 1 args', function(){
      test(1, function(){
        return expect(divideAndAddThreeArgsFlipped(10, 5, 1)).toEqual(1.5);
      });
      test('result is curried', function(){
        return expect(divideAndAddThreeArgsFlipped(10)(5)(1)).toEqual(1.5);
      });
      test('result is curried part deux', function(){
        return expect(divideAndAddThreeArgsFlipped(10, 5)(1)).toEqual(1.5);
      });
    });
    describe('2 + > 1 args', function(){
      test(1, function(){
        return expect(divideAndAddFourArgsFlipped(10, 5, 1, 2)).toEqual(3.5);
      });
      test('result is curried', function(){
        return expect(divideAndAddFourArgsFlipped(10)(5)(1)(2)).toEqual(3.5);
      });
      test('result is curried part deux', function(){
        return expect(divideAndAddFourArgsFlipped(10, 5, 1)(2)).toEqual(3.5);
      });
      test('result is curried part trois', function(){
        return expect(divideAndAddFourArgsFlipped(10, 5)(1, 2)).toEqual(3.5);
      });
      test('result is curried part quatre', function(){
        return expect(divideAndAddFourArgsFlipped(10)(5, 1, 2)).toEqual(3.5);
      });
    });
  });
  describe('target created with ramda curry function', function(){
    var divide, divideAndAddThreeArgs, divideAndAddFourArgs, divideFlipped, divideAndAddThreeArgsFlipped, divideAndAddFourArgsFlipped;
    divide = rCurry(function(a, b){
      return a / b;
    });
    divideAndAddThreeArgs = rCurry(function(a, b, c){
      return a / b + c;
    });
    divideAndAddFourArgs = rCurry(function(a, b, c, d){
      return a / b + c + d;
    });
    divideFlipped = flip(divide);
    divideAndAddThreeArgsFlipped = flip3(divideAndAddThreeArgs);
    divideAndAddFourArgsFlipped = flip4(divideAndAddFourArgs);
    test('init', function(){
      expect(divide(10)(5)).toEqual(2);
      expect(divideAndAddThreeArgs(10)(5)(1)).toEqual(3);
      return expect(divideAndAddFourArgs(10)(5)(1)(2)).toEqual(5);
    });
    describe('2 args', function(){
      test(1, function(){
        return expect(divideFlipped(10, 5)).toEqual(0.5);
      });
      test('result is curried', function(){
        return expect(divideFlipped(10)(5)).toEqual(0.5);
      });
    });
    describe('2 + 1 args', function(){
      test(1, function(){
        return expect(divideAndAddThreeArgsFlipped(10, 5, 1)).toEqual(1.5);
      });
      test('result is curried', function(){
        return expect(divideAndAddThreeArgsFlipped(10)(5)(1)).toEqual(1.5);
      });
      test('result is curried part deux', function(){
        return expect(divideAndAddThreeArgsFlipped(10, 5)(1)).toEqual(1.5);
      });
    });
    describe('2 + > 1 args', function(){
      test(1, function(){
        return expect(divideAndAddFourArgsFlipped(10, 5, 1, 2)).toEqual(3.5);
      });
      test('result is curried', function(){
        return expect(divideAndAddFourArgsFlipped(10)(5)(1)(2)).toEqual(3.5);
      });
      test('result is curried part deux', function(){
        return expect(divideAndAddFourArgsFlipped(10, 5, 1)(2)).toEqual(3.5);
      });
      test('result is curried part trois', function(){
        return expect(divideAndAddFourArgsFlipped(10, 5)(1, 2)).toEqual(3.5);
      });
      test('result is curried part quatre', function(){
        return expect(divideAndAddFourArgsFlipped(10)(5, 1, 2)).toEqual(3.5);
      });
    });
  });
});
describe('letV', function(){
  describe('let-v', function(){
    test(1, function(){
      return expectToEqual(41)(
      letV(10, 12, 19, sumAll));
    });
    test('last arg must be a function', function(){
      expectToThrow(
      function(){
        return letV(10);
      });
      expectToThrow(
      function(){
        return letV(10, 12, 19);
      });
      return expectNotToThrow(
      function(){
        return letV(sumAll);
      });
    });
  });
  describe('letNV', function(){
    test(1, function(){
      return expectToEqual(41)(
      letNV([10, 12, 19], sumAll));
    });
  });
});
describe('lets', function(){
  test('main', function(){
    return expectToEqual(41)(
    lets(function(){
      return 10;
    }, function(){
      return 12;
    }, function(){
      return 19;
    }, sumAll));
  });
  describe('specific versions', function(){
    test('lets2', function(){
      return expectToEqual(11)(
      lets2(function(){
        return 10;
      }, (function(it){
        return it + 1;
      })));
    });
    test('lets3', function(){
      return expectToEqual(21)(
      lets3(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll));
    });
    test('lets4', function(){
      return expectToEqual(42)(
      lets4(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll, sumAll));
    });
    test('lets5', function(){
      return expectToEqual(84)(
      lets5(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll, sumAll, sumAll));
    });
    test('lets6', function(){
      return expectToEqual(168)(
      lets6(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll, sumAll, sumAll, sumAll));
    });
    test('letN', function(){
      return expectToEqual(168)(
      letN([
        function(){
          return 10;
        }, (function(it){
          return it + 1;
        }), sumAll, sumAll, sumAll, sumAll
      ]));
    });
    test('letS', function(){
      return expectToEqual(84)(
      letS([
        function(t){
          return t + 1;
        }, function(t, f){
          return t + f;
        }, function(t, f, s){
          return t * f * s;
        }
      ])(
      3));
    });
  });
  describe('generic version', function(){
    test('lets (2)', function(){
      return expectToEqual(11)(
      lets(function(){
        return 10;
      }, (function(it){
        return it + 1;
      })));
    });
    test('lets (3)', function(){
      return expectToEqual(21)(
      lets(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll));
    });
    test('lets (6)', function(){
      return expectToEqual(168)(
      lets(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll, sumAll, sumAll, sumAll));
    });
    test('lets (7)', function(){
      return expectToEqual(336)(
      lets(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll, sumAll, sumAll, sumAll, sumAll));
    });
    test('lets (10)', function(){
      return expectToEqual(2688)(
      lets(function(){
        return 10;
      }, (function(it){
        return it + 1;
      }), sumAll, sumAll, sumAll, sumAll, sumAll, sumAll, sumAll, sumAll));
    });
  });
  test('single function', function(){
    return expectToEqual(11)(
    lets(function(){
      return 11;
    }));
  });
  test('fibonacci', function(){
    var fibonacci;
    fibonacci = function(n){
      var sumLastTwo, entry, refs, args;
      sumLastTwo = function(xs){
        return xs[xs.length - 1] + xs[xs.length - 2];
      };
      entry = function(){
        var prev, res$, i$, to$, m;
        res$ = [];
        for (i$ = 0, to$ = arguments.length; i$ < to$; ++i$) {
          res$.push(arguments[i$]);
        }
        prev = res$;
        m = prev.length;
        switch (false) {
        case m !== 0:
          return 1;
        case m !== 1:
          return 1;
        default:
          return sumLastTwo(prev);
        }
      };
      refs = rRepeat(entry, n + 1);
      args = arrayFrom$(refs).concat([list]);
      return lets.apply(null, args);
    };
    expect(fibonacci(0)).toEqual([1]);
    expect(fibonacci(1)).toEqual([1, 1]);
    expect(fibonacci(2)).toEqual([1, 1, 2]);
    return expect(fibonacci(4)).toEqual([1, 1, 2, 3, 5]);
  });
});
describe('sprintf*', function(){
  describe('sprintf1', function(){
    test(1, function(){
      return expectToEqual('my name is dog')(
      sprintf1('my name is %s')(
      'dog'));
    });
    test(2, function(){
      return expectToEqual('my name is 3.33')(
      sprintf1('my name is %0.2f')(
      10 / 3));
    });
  });
  describe('sprintfn', function(){
    test(1, function(){
      return expectToEqual('my name is not dog')(
      sprintfN('my name %s not %s')(
      ['is', 'dog']));
    });
    test(2, function(){
      return expectToEqual('my name is 3.33')(
      sprintfN('my name %s %0.2f')(
      ['is', 10 / 3]));
    });
  });
});
describe('zip-all', function(){
  test(1, function(){
    return expectToEqual([[1, 'un'], [2, 'deux'], [3, 'trois']])(
    zipAll([1, 2, 3], ['un', 'deux', 'trois']));
  });
  test(2, function(){
    return expectToEqual([['un', 'yek', 'egy'], ['deux', 'do', 'ketto'], ['trois', 'seh', 'harom']])(
    zipAll(['un', 'deux', 'trois'], ['yek', 'do', 'seh'], ['egy', 'ketto', 'harom']));
  });
  test("two args equivalent to ramda's zip", function(){
    return expectToEqual(rZip(['un', 'yek', 'egy'], ['yek', 'do', 'seh']))(
    zipAll(['un', 'yek', 'egy'], ['yek', 'do', 'seh']));
  });
});
describe('new', function(){
  var C;
  C = (function(){
    C.displayName = 'C';
    var prototype = C.prototype, constructor = C;
    function C(){
      var args, res$, i$, to$;
      res$ = [];
      for (i$ = 0, to$ = arguments.length; i$ < to$; ++i$) {
        res$.push(arguments[i$]);
      }
      args = res$;
      this.nums = args;
    }
    C.prototype.speak = function(){
      return rJoin(' ', ['hulu'].concat(arrayFrom$(this.nums)));
    };
    return C;
  }());
  test('neu', function(){
    return expectToEqual('hulu')(
    function(it){
      return it.speak();
    }(
    neu(
    C)));
  });
  test('neu1', function(){
    return expectToEqual('hulu 10')(
    function(it){
      return it.speak();
    }(
    neu1(C)(
    10)));
  });
  test('neu2', function(){
    return expectToEqual('hulu 20 30')(
    function(it){
      return it.speak();
    }(
    neu2(C)(20, 30)));
  });
  test('neu3', function(){
    return expectToEqual('hulu 2 4 6')(
    function(it){
      return it.speak();
    }(
    neu3(C)(2, 4, 6)));
  });
  test('neu4', function(){
    return expectToEqual('hulu 2 4 6 8')(
    function(it){
      return it.speak();
    }(
    neu4(C)(2, 4, 6, 8)));
  });
  test('neu5', function(){
    return expectToEqual('hulu 2 4 6 8 10')(
    function(it){
      return it.speak();
    }(
    neu5(C)(2, 4, 6, 8, 10)));
  });
  test('neuN', function(){
    return expectToEqual('hulu 4 8 9')(
    function(it){
      return it.speak();
    }(
    neuN(C)(
    [4, 8, 9])));
  });
});
describe('match/regex', function(){
  test('x-regexp', function(){
    var re;
    re = xRegExp(new RegExp(' (ses) $', 'm'));
    return expectToEqual('ses')(
    function(m){
      return m[1];
    }(
    function(it){
      return it.match(re);
    }(
    'horses\npigs')));
  });
  test('x-regexp-flags', function(){
    var re;
    re = xRegExpFlags(new RegExp(' (SeS) $'), 'mi');
    return expectToEqual('ses')(
    function(m){
      return m[1];
    }(
    function(it){
      return it.match(re);
    }(
    'horses\npigs')));
  });
  test('x-regexp-flags, overrides existing flags', function(){
    var re;
    re = xRegExpFlags(new RegExp(' (SeS) $', 'i'), 'm');
    return expectToEqual(null)(
    function(it){
      return it.match(re);
    }(
    'horses\npigs'));
  });
  test('x-regexp-str, no flags', function(){
    var re;
    re = xRegExpStr(' (igs) $');
    return expectToEqual('igs')(
    function(m){
      return m[1];
    }(
    function(it){
      return it.match(re);
    }(
    'horses\npigs')));
  });
  test('x-regexp-str, flags', function(){
    var re;
    re = xRegExpStr(' (ses) $', 'm');
    return expectToEqual('ses')(
    function(m){
      return m[1];
    }(
    function(it){
      return it.match(re);
    }(
    'horses\npigs')));
  });
  test('xmatch-str', function(){
    return expectToEqual('ors')(
    function(it){
      return it[1];
    }(
    xMatchStr(' ( o . s ) ')(
    'horses')));
  });
  test('xmatch-str-flags', function(){
    return expectToEqual('ses')(
    function(it){
      return it[1];
    }(
    xMatchStrFlags(' (ses) $', 'm')(
    'horses\npigs')));
  });
  test('xmatch', function(){
    return expectToEqual('ors')(
    function(m){
      return m[1];
    }(
    xMatch(new RegExp(' ( o . s ) '))(
    'horses')));
  });
  test('xmatch-global', function(){
    var y, re;
    y = [];
    re = new RegExp(' (s .) ');
    xMatchGlobal(re, function(m){
      return y.push(m);
    })(
    'shorses and shoes');
    return expectToEqual(['sh', 'se', 's ', 'sh'])(
    y);
  });
  test('match', function(){
    return expectToEqual('ors')(
    function(m){
      return m[1];
    }(
    stickMatch(new RegExp('(o.s)'))(
    'horses')));
  });
  test('x-replace', function(){
    return expectToEqual('lpos of pigs')(
    xReplace(new RegExp('(o .)'), 'po')(
    'lots of pigs'));
  });
  test('x-replace global', function(){
    return expectToEqual('lpos po pigs')(
    xReplace(new RegExp('(o .)', 'g'), 'po')(
    'lots of pigs'));
  });
  test('x-replace-str', function(){
    return expectToEqual('lots stickigs')(
    xReplaceStr(' (o .. p) ', 'stick')(
    'lots of pigs'));
  });
  test('x-replace-str-flags', function(){
    return expectToEqual('lpos of pigs')(
    xReplaceStrFlags(' (o .) ', '', 'po')(
    'lots of pigs'));
  });
  test('x-replace-str-flags global', function(){
    return expectToEqual('lpos po pigs')(
    xReplaceStrFlags(' (o .) ', 'g', 'po')(
    'lots of pigs'));
  });
});
describe('ifReplace*', function(){
  var doTest;
  doTest = function(expectResult, expectReplCount, success, func){
    var jaRes, neeRes, replCount, x$, ja, y$, nee, ref$, jaCalls, neeCalls, result;
    x$ = ja = jest.fn();
    x$.mockImplementation(function(x, cnt){
      jaRes = x;
      return replCount = cnt;
    });
    y$ = nee = jest.fn();
    y$.mockImplementation(function(x){
      neeRes = x;
      return replCount = 0;
    });
    func(ja, nee);
    ref$ = success
      ? [1, 0, jaRes]
      : [0, 1, neeRes], jaCalls = ref$[0], neeCalls = ref$[1], result = ref$[2];
    expectToEqual(jaCalls)(
    ja.mock.calls.length);
    expectToEqual(neeCalls)(
    nee.mock.calls.length);
    expectToEqual(expectResult)(
    result);
    return expectToEqual(expectReplCount)(
    replCount);
  };
  test('ifReplace succesful', function(){
    var ref$, re, target, replacement;
    ref$ = [/s/g, 'sandmishes', 't'], re = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('tandmithet', 3, true, function(ja, nee){
      return ifReplace(ja, nee, re, replacement)(
      target);
    });
  });
  test('ifReplace succesful, repl is a function', function(){
    var ref$, re, target, replacement;
    ref$ = [
      /s/g, 'sandmishes', function(it){
        return it.toUpperCase();
      }
    ], re = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('SandmiSheS', 3, true, function(ja, nee){
      return ifReplace(ja, nee, re, replacement)(
      target);
    });
  });
  test('ifReplace not succesful', function(){
    var ref$, re, target, replacement;
    ref$ = [/xxxx/g, 'sandmishes', 't'], re = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('sandmishes', 0, false, function(ja, nee){
      return ifReplace(ja, nee, re, replacement)(
      target);
    });
  });
  test('ifXReplace succesful', function(){
    var ref$, re, target, replacement;
    ref$ = [new RegExp(' s ', 'g'), 'sandmishes', 't'], re = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('tandmithet', 3, true, function(ja, nee){
      return ifXReplace(re, replacement, ja, nee)(
      target);
    });
  });
  test('ifXReplace not succesful', function(){
    var ref$, re, target, replacement;
    ref$ = [new RegExp(' xxxx ', 'g'), 'sandmishes', 't'], re = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('sandmishes', 0, false, function(ja, nee){
      return ifXReplace(re, replacement, ja, nee)(
      target);
    });
  });
  test('ifXReplaceStr succesful', function(){
    var ref$, reStr, target, replacement;
    ref$ = [' s ', 'sandmishes', 't'], reStr = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('tandmishes', 1, true, function(ja, nee){
      return ifXReplaceStr(reStr, replacement, ja, nee)(
      target);
    });
  });
  test('ifXReplaceStr not succesful', function(){
    var ref$, reStr, target, replacement;
    ref$ = [' xxxx ', 'sandmishes', 't'], reStr = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('sandmishes', 0, false, function(ja, nee){
      return ifXReplaceStr(reStr, replacement, ja, nee)(
      target);
    });
  });
  test('ifXReplaceStrFlags succesful', function(){
    var ref$, reStr, target, replacement;
    ref$ = [' s ', 'sandmishes', 't'], reStr = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('tandmithet', 3, true, function(ja, nee){
      return ifXReplaceStrFlags(reStr, 'g', replacement, ja, nee)(
      target);
    });
  });
  test('ifXReplaceStrFlags not succesful', function(){
    var ref$, reStr, target, replacement;
    ref$ = [' xxxx ', 'sandmishes', 't'], reStr = ref$[0], target = ref$[1], replacement = ref$[2];
    return doTest('sandmishes', 0, false, function(ja, nee){
      return ifXReplaceStrFlags(reStr, 'g', replacement, ja, nee)(
      target);
    });
  });
});
describe('repeat, times', function(){
  var y, ping;
  y = {
    y: void 8
  };
  ping = function(x){
    return y.y.push(x);
  };
  beforeEach(function(){
    return y.y = [];
  });
  describe('repeatV', function(){
    test(1, function(){
      return expectToEqual(['thing', 'thing', 'thing', 'thing', 'thing'])(
      repeatV('thing')(
      5));
    });
  });
  describe('repeatF', function(){
    test(1, function(){
      return expectToEqual(['thing0', 'thing1', 'thing2'])(
      repeatF(function(n){
        return "thing" + n;
      })(
      3));
    });
  });
  describe('repeatSide', function(){
    test(1, function(){
      expectToEqual(void 8)(
      repeatSide(function(n){
        return ping("thing" + n);
      })(
      3));
      return expectToEqual(['thing0', 'thing1', 'thing2'])(
      y.y);
    });
  });
  describe('timesV', function(){
    test(1, function(){
      return expectToEqual(['thing', 'thing', 'thing', 'thing', 'thing'])(
      timesV(5)(
      'thing'));
    });
  });
  describe('timesF', function(){
    test(1, function(){
      return expectToEqual(['thing0', 'thing1', 'thing2'])(
      timesF(3)(
      function(n){
        return "thing" + n;
      }));
    });
  });
  describe('timesSide', function(){
    test(1, function(){
      expectToEqual(void 8)(
      timesSide(3)(
      function(n){
        return ping("thing" + n);
      }));
      return expectToEqual(['thing0', 'thing1', 'thing2'])(
      y.y);
    });
  });
});
describe('types', function(){
  test('isType', function(){
    expectToEqual(true)(
    isType('Number')(
    3));
    expectToEqual(false)(
    isType('Boolean')(
    3));
    expectToEqual(false)(
    isType('String')(
    3));
    expectToEqual(false)(
    isType(Number)(
    3));
    expectToEqual(false)(
    isType(Boolean)(
    3));
    return expectToEqual(false)(
    isType(String)(
    3));
  });
  test('getType', function(){
    expectToEqual('Number')(
    getType(
    3));
    expectToEqual('Boolean')(
    getType(
    true));
    expectToEqual('String')(
    getType(
    '3'));
    expectToEqual('Undefined')(
    getType(
    void 8));
    return expectToEqual('Null')(
    getType(
    null));
  });
  test('isFunction', function(){
    expectToEqual(true)(
    isFunction(
    (function(it){
      return it + 3;
    })));
    expectToEqual(false)(
    isFunction(
    3));
    expectToEqual(false)(
    isFunction(
    '3'));
    expectToEqual(true)(
    isFunction(
    function*(){
      return (yield null);
    }));
    return expectToEqual(true)(
    isFunction(
    new Proxy(function(){}, {})));
  });
  test('isArray', function(){
    expectToEqual(true)(
    isArray(
    []));
    expectToEqual(false)(
    isArray(
    {}));
    return expectToEqual(false)(
    isArray(
    3));
  });
  test('isObject', function(){
    expectToEqual(true)(
    isObject(
    {}));
    expectToEqual(false)(
    isObject(
    []));
    return expectToEqual(false)(
    isObject(
    null));
  });
  test('isNumber', function(){
    expectToEqual(true)(
    isNumber(
    3));
    expectToEqual(true)(
    isNumber(
    NaN));
    expectToEqual(false)(
    isNumber(
    '2'));
    return expectToEqual(false)(
    isNumber(
    true));
  });
  test('isRegExp', function(){
    expectToEqual(true)(
    isRegExp(
    /abc/));
    expectToEqual(false)(
    isRegExp(
    3));
    return expectToEqual(false)(
    isRegExp(
    null));
  });
  test('isBoolean', function(){
    expectToEqual(true)(
    isBoolean(
    true));
    expectToEqual(true)(
    isBoolean(
    false));
    expectToEqual(false)(
    isBoolean(
    0));
    return expectToEqual(false)(
    isBoolean(
    null));
  });
  test('isString', function(){
    expectToEqual(true)(
    isString(
    '3'));
    expectToEqual(false)(
    isString(
    3));
    return expectToEqual(false)(
    isString(
    true));
  });
  test('isSymbol', function(){
    expectToEqual(true)(
    isSymbol(
    Symbol()));
    expectToEqual(false)(
    isSymbol(
    3));
    return expectToEqual(false)(
    isSymbol(
    true));
  });
  test('isInteger', function(){
    expectToEqual(false)(
    isInteger(
    '3.2'));
    expectToEqual(false)(
    isInteger(
    '3'));
    expectToEqual(false)(
    isInteger(
    3.2));
    expectToEqual(false)(
    isInteger(
    -3.2));
    expectToEqual(true)(
    isInteger(
    3));
    expectToEqual(true)(
    isInteger(
    0));
    expectToEqual(true)(
    isInteger(
    -0));
    return expectToEqual(true)(
    isInteger(
    -3));
  });
});
describe('range, compact', function(){
  describe('rangeFromBy', function(){
    test(1, function(){
      return expectToEqual([0, 2, 4, 6, 8])(
      rangeFromBy(2, 0, 10));
    });
    test(3, function(){
      return expectToEqual([10, 8, 6, 4, 2])(
      rangeFromBy(-2, 10, 0));
    });
  });
  describe('rangeFromBy specific', function(){
    test(1, function(){
      return expectToEqual([0, 2, 4, 6, 8])(
      rangeFromByAsc(2, 0, 10));
    });
    test(3, function(){
      return expectToEqual([10, 8, 6, 4, 2])(
      rangeFromByDesc(-2, 10)(
      0));
    });
  });
  describe('rangeToBy', function(){
    test(1, function(){
      return expectToEqual([0, 2, 4, 6, 8])(
      rangeToBy(2, 10, 0));
    });
    test(2, function(){
      return expectToEqual([0, 2, 4, 6, 8])(
      rangeToBy(2, 10)(
      0));
    });
    test(3, function(){
      return expectToEqual([10, 8, 6, 4, 2])(
      rangeToBy(-2, 0, 10));
    });
  });
  describe('rangeFrom', function(){
    test(1, function(){
      return expectToEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])(
      rangeFrom(0)(
      10));
    });
  });
  describe('rangeTo', function(){
    test(1, function(){
      return expectToEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])(
      rangeTo(10)(
      0));
    });
  });
});
describe('and, or, ...', function(){
  describe('and', function(){
    test(1, function(){
      expectToEqual(3)(
      stickAnd(3, 4));
      expectToEqual(false)(
      stickAnd(false, 4));
      expectToEqual(null)(
      stickAnd(null, 4));
      expectToEqual('')(
      stickAnd('', 4));
      return expectToEqual(' ')(
      stickAnd(' ', 4));
    });
    test(2, function(){
      expectToEqual(4)(
      stickAnd(4, 3));
      expectToEqual(false)(
      stickAnd(4, false));
      expectToEqual(null)(
      stickAnd(4, null));
      expectToEqual('')(
      stickAnd(4, ''));
      return expectToEqual(4)(
      stickAnd(4, ' '));
    });
    test('boolean', function(){
      expectToEqual(true)(
      Boolean(
      stickAnd(3, 4)));
      expectToEqual(false)(
      Boolean(
      stickAnd(false, 4)));
      expectToEqual(false)(
      Boolean(
      stickAnd(null, 4)));
      expectToEqual(false)(
      Boolean(
      stickAnd('', 4)));
      return expectToEqual(true)(
      Boolean(
      stickAnd(' ', 4)));
    });
    test('curried', function(){
      expectToEqual(3)(
      stickAnd(3)(
      4));
      expectToEqual(false)(
      stickAnd(false)(
      4));
      expectToEqual(null)(
      stickAnd(null)(
      4));
      expectToEqual('')(
      stickAnd('')(
      4));
      return expectToEqual(' ')(
      stickAnd(' ')(
      4));
    });
  });
  describe('or', function(){
    test(1, function(){
      expectToEqual(4)(
      stickOr(3, 4));
      expectToEqual(4)(
      stickOr(false, 4));
      expectToEqual(4)(
      stickOr(null, 4));
      expectToEqual(4)(
      stickOr('', 4));
      return expectToEqual(4)(
      stickOr(' ', 4));
    });
    test(2, function(){
      expectToEqual(3)(
      stickOr(4, 3));
      expectToEqual(4)(
      stickOr(4, false));
      expectToEqual(4)(
      stickOr(4, null));
      expectToEqual(4)(
      stickOr(4, ''));
      return expectToEqual(' ')(
      stickOr(4, ' '));
    });
    test('boolean', function(){
      expectToEqual(true)(
      Boolean(
      stickOr(3, 4)));
      expectToEqual(true)(
      Boolean(
      stickOr(false, 4)));
      expectToEqual(true)(
      Boolean(
      stickOr(null, 4)));
      expectToEqual(true)(
      Boolean(
      stickOr('', 4)));
      expectToEqual(true)(
      Boolean(
      stickOr(' ', 4)));
      return expectToEqual(false)(
      Boolean(
      stickOr(false, null)));
    });
    test('curried', function(){
      expectToEqual(3)(
      stickOr(4)(
      3));
      expectToEqual(4)(
      stickOr(4)(
      false));
      expectToEqual(4)(
      stickOr(4)(
      null));
      expectToEqual(4)(
      stickOr(4)(
      ''));
      return expectToEqual(' ')(
      stickOr(4)(
      ' '));
    });
  });
  describe('andNot', function(){
    test(1, function(){
      expectToEqual(false)(
      andNot(3, 4));
      expectToEqual(true)(
      andNot(false, 4));
      expectToEqual(true)(
      andNot(null, 4));
      expectToEqual(true)(
      andNot('', 4));
      return expectToEqual(false)(
      andNot(' ', 4));
    });
    test(2, function(){
      expectToEqual(false)(
      andNot(4, 3));
      expectToEqual(false)(
      andNot(4, false));
      expectToEqual(null)(
      andNot(4, null));
      expectToEqual('')(
      andNot(4, ''));
      return expectToEqual(false)(
      andNot(4, ' '));
    });
    test('boolean', function(){
      expectToEqual(false)(
      Boolean(
      andNot(3, 4)));
      expectToEqual(true)(
      Boolean(
      andNot(false, 4)));
      expectToEqual(true)(
      Boolean(
      andNot(null, 4)));
      expectToEqual(true)(
      Boolean(
      andNot('', 4)));
      return expectToEqual(false)(
      Boolean(
      andNot(' ', 4)));
    });
    test('curried', function(){
      expectToEqual(false)(
      andNot(3)(
      4));
      expectToEqual(true)(
      andNot(false)(
      4));
      expectToEqual(true)(
      andNot(null)(
      4));
      expectToEqual(true)(
      andNot('')(
      4));
      return expectToEqual(false)(
      andNot(' ')(
      4));
    });
  });
  describe('orNot', function(){
    test(1, function(){
      expectToEqual(4)(
      orNot(3, 4));
      expectToEqual(4)(
      orNot(false, 4));
      expectToEqual(4)(
      orNot(null, 4));
      expectToEqual(4)(
      orNot('', 4));
      return expectToEqual(4)(
      orNot(' ', 4));
    });
    test(2, function(){
      expectToEqual(3)(
      orNot(4, 3));
      expectToEqual(false)(
      orNot(4, false));
      expectToEqual(false)(
      orNot(4, null));
      expectToEqual(false)(
      orNot(4, ''));
      return expectToEqual(' ')(
      orNot(4, ' '));
    });
    test('boolean', function(){
      expectToEqual(true)(
      Boolean(
      orNot(3, 4)));
      expectToEqual(true)(
      Boolean(
      orNot(false, 4)));
      expectToEqual(true)(
      Boolean(
      orNot(null, 4)));
      expectToEqual(true)(
      Boolean(
      orNot('', 4)));
      expectToEqual(true)(
      Boolean(
      orNot(' ', 4)));
      expectToEqual(true)(
      Boolean(
      orNot(null, false)));
      expectToEqual(false)(
      Boolean(
      orNot(4, null)));
      return expectToEqual(false)(
      Boolean(
      orNot(true, false)));
    });
    test('curried', function(){
      expectToEqual(4)(
      orNot(3)(
      4));
      expectToEqual(4)(
      orNot(false)(
      4));
      expectToEqual(4)(
      orNot(null)(
      4));
      expectToEqual(4)(
      orNot('')(
      4));
      return expectToEqual(4)(
      orNot(' ')(
      4));
    });
  });
});
function compose$() {
  var functions = arguments;
  return function() {
    var i, result;
    result = functions[0].apply(this, arguments);
    for (i = 1; i < functions.length; ++i) {
      result = functions[i](result);
    }
    return result;
  };
}
function bind$(obj, key, target){
  return function(){ return (target || obj)[key].apply(obj, arguments) };
}
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}