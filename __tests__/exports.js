var ref$, list, test, xtest, expectToEqual, expectToBe, expectPredicate, expectToBeTruthy, expectToBeFalsy, expectToHaveTypeof, expectToHaveTypeOf, expectToContainObject, expectNotToEqual, expectNotToBe, map, each, api, index;
ref$ = require('./common'), list = ref$.list, test = ref$.test, xtest = ref$.xtest, expectToEqual = ref$.expectToEqual, expectToBe = ref$.expectToBe, expectPredicate = ref$.expectPredicate, expectToBeTruthy = ref$.expectToBeTruthy, expectToBeFalsy = ref$.expectToBeFalsy, expectToHaveTypeof = ref$.expectToHaveTypeof, expectToHaveTypeOf = ref$.expectToHaveTypeOf, expectToContainObject = ref$.expectToContainObject, expectNotToEqual = ref$.expectNotToEqual, expectNotToBe = ref$.expectNotToBe;
ref$ = require('prelude-ls'), map = ref$.map, each = ref$.each;
api = list({
  path: '../cjs/curry',
  tag: 'curry',
  symbols: ['recurry', 'roll']
}, {
  path: '../cjs/manual',
  tag: 'manual',
  symbols: ['add', 'addCollection', 'addCollection2', 'addIndex', 'addIndex2', 'againstAll', 'againstAny', 'againstBoth', 'againstEither', 'allAgainst', 'always', 'ampersandN', 'and', 'andNot', 'anyAgainst', 'append', 'appendM', 'appendTo', 'appendToM', 'applyTo1', 'applyTo2', 'applyTo3', 'applyTo4', 'applyTo5', 'applyToN', 'assoc', 'assocM', 'assocPath', 'assocPathM', 'asterisk1', 'asterisk2', 'asterisk3', 'asterisk4', 'asterisk5', 'asteriskN', 'bind', 'bindLateProp', 'bindLatePropTo', 'bindProp', 'bindPropTo', 'bindTo', 'bindTry', 'bindTryProp', 'bindTryPropTo', 'bindTryTo', 'callOn', 'callOn1', 'callOn2', 'callOn3', 'callOn4', 'callOn5', 'callOnN', 'compose', 'composeAsMethods', 'composeAsMethodsRight', 'composeRight', 'concat', 'concatM', 'concatTo', 'concatToM', 'cond', 'condPredicate', 'condS', 'contains', 'containsV', 'deconstruct', 'deconstruct2', 'deconstructN', 'defaultTo', 'die', 'divideBy', 'divideInto', 'dot', 'dot1', 'dot2', 'dot3', 'dot4', 'dot5', 'dotN', 'drop', 'each', 'eachObj', 'eachObjIn', 'eq', 'exception', 'F', 'factoryInit', 'factoryProps', 'filter', 'find', 'findIndex', 'findWithIndex', 'flip', 'flip3', 'flip4', 'flip5', 'getType', 'gt', 'gte', 'has', 'hasIn', 'ifAlways', 'ifBind', 'ifHas', 'ifHasIn', 'ifPredicate', 'ifPredicateResults', 'ifPredicateWithResults', 'ifPredicateV', 'ifReplace', 'ifXReplace', 'ifXReplaceStr', 'ifXReplaceStrFlags', 'invoke', 'isArray', 'isBoolean', 'isFalse', 'isFunction', 'isNo', 'isNumber', 'isObject', 'isRegExp', 'isString', 'isSymbol', 'isTrue', 'isType', 'isYes', 'join', 'letN', 'letNV', 'lets', 'letS', 'lets1', 'lets2', 'lets3', 'lets4', 'lets5', 'lets6', 'letV', 'lt', 'lte', 'map', 'match', 'merge', 'mergeAllIn', 'mergeIn', 'mergeInM', 'mergeInMSym', 'mergeInSym', 'mergeInTo', 'mergeInToM', 'mergeInToMSym', 'mergeInToSym', 'mergeM', 'mergeMSym', 'mergeSym', 'mergeTo', 'mergeToM', 'mergeToMSym', 'mergeToSym', 'mergeWhen', 'mergeWith', 'mixinM', 'mixinNM', 'mixinPreM', 'mixinPreNM', 'modulo', 'moduloWholePart', 'multiply', 'ne', 'neu1', 'neu2', 'neu3', 'neu4', 'neu5', 'neuN', 'noop', 'not', 'notOk', 'ok', 'or', 'orNot', 'passTo', 'passToN', 'path', 'pathOf', 'pipe', 'prepend', 'prependM', 'prependTo', 'prependToM', 'prop', 'propOf', 'provideTo', 'provideTo1', 'provideTo2', 'provideTo3', 'provideTo4', 'provideTo5', 'provideToN', 'raise', 'rangeFromBy', 'rangeFromByAsc', 'rangeFromByDesc', 'rangeToBy', 'reduce', 'reduceAbort', 'reduceObj', 'reduceObjIn', 'reduceRight', 'reduceRightC', 'reject', 'repeatF', 'repeatSide', 'repeatV', 'side', 'side1', 'side2', 'side3', 'side4', 'side5', 'sideN', 'split', 'sprintf1', 'sprintfN', 'subtract', 'subtractFrom', 'T', 'take', 'tap', 'timesF', 'timesSide', 'timesV', 'toThe', 'tryCatch', 'update', 'updateM', 'updatePath', 'updatePathM', 'whenAlways', 'whenBind', 'whenHas', 'whenHasIn', 'whenPredicate', 'whenPredicateResults', 'whenPredicateWithResults', 'whenPredicateV', 'xMatch', 'xMatchGlobal', 'xMatchStr', 'xMatchStrFlags', 'xRegExp', 'xRegExpFlags', 'xRegExpStr', 'xReplace', 'xReplaceStr', 'xReplaceStrFlags']
}, {
  path: '../cjs/map',
  tag: 'map',
  symbols: ['mapKeys', 'mapTuples', 'mapValues', 'remapKeys', 'remapTuples', 'remapValues', 'withFilter']
}, {
  path: '../cjs/operator',
  tag: 'operator',
  symbols: ['bitwiseAnd', 'bitwiseLeft', 'bitwiseLeftBy', 'bitwiseNot', 'bitwiseOr', 'bitwiseRight', 'bitwiseRightBy', 'bitwiseRightZeroFill', 'bitwiseRightZeroFillBy', 'bitwiseXor']
}, {
  path: null,
  tag: 'only-in-index',
  symbols: ['ifFalse', 'ifNo', 'ifNotOk', 'ifOk', 'ifTrue', 'ifYes', 'whenFalse', 'whenNo', 'whenNotOk', 'whenOk', 'whenTrue', 'whenYes']
});
index = require('../cjs/index');
map(function(arg$){
  var path, tag, symbols;
  path = arg$.path, tag = arg$.tag, symbols = arg$.symbols;
  return describe(tag, function(){
    map(function(fn){
      describe(tag + "/" + fn, function(){
        var mod, named;
        if (path !== null) {
          mod = require(path);
          named = mod[fn];
          test('named exported', function(){
            return expectNotToBe(void 8)(
            named);
          });
        }
      });
      return describe("index/" + fn, function(){
        var named;
        named = index[fn];
        test('named exported from index', function(){
          return expectNotToBe(void 8)(
          named);
        });
      });
    })(
    symbols);
  });
})(
api);