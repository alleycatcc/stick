var ref$, test, xtest, expectToEqual, expectToBe, expectNotToBe, array, compact, compactOk, list, last, reverse, reverseM, head, tail;
ref$ = require('./common'), test = ref$.test, xtest = ref$.xtest, expectToEqual = ref$.expectToEqual, expectToBe = ref$.expectToBe, expectNotToBe = ref$.expectNotToBe;
ref$ = require('../cjs/index'), array = ref$.array, compact = ref$.compact, compactOk = ref$.compactOk, list = ref$.list, last = ref$.last, reverse = ref$.reverse, reverseM = ref$.reverseM, head = ref$.head, tail = ref$.tail;
describe('list', function(){
  test(1, function(){
    return expectToEqual([3, 4, 5])(
    list(3, 4, 5));
  });
  test('empty', function(){
    return expectToEqual([])(
    list());
  });
});
describe('compact*', function(){
  var mixed, falsey, truthy;
  mixed = [1, '', 0, '0', void 8, false, true, 2];
  falsey = [false, void 8, null, '', 0, NaN];
  truthy = [true, '0', [], {}, -1, Infinity];
  describe('compact', function(){
    test(1, function(){
      return expectToEqual([1, '0', true, 2])(
      compact(
      mixed));
    });
    test('all falsey', function(){
      return expectToEqual([])(
      compact(
      falsey));
    });
    test('all truthy', function(){
      return expectToEqual(truthy)(
      compact(
      truthy));
    });
  });
  describe('compactOk', function(){
    test(1, function(){
      return expectToEqual([1, '', 0, '0', false, true, 2])(
      compactOk(
      mixed));
    });
    test('all falsey', function(){
      return expectToEqual([false, '', 0, NaN])(
      compactOk(
      falsey));
    });
    test('all truthy', function(){
      return expectToEqual(truthy)(
      compactOk(
      truthy));
    });
  });
});
describe('reverse(M)', function(){
  describe('reverse', function(){
    test(1, function(){
      var x, y;
      x = [1, 2, 3];
      y = reverse(x);
      return expectToEqual([3, 2, 1])(
      y);
    });
    test(2, function(){
      var x, y;
      x = [1, 2, 3];
      y = reverse(x);
      return expectToEqual([3, 2, 1])(
      y);
    });
  });
  describe('reverseM', function(){
    test(1, function(){
      var x, y;
      x = [1, 2, 3];
      y = reverseM(x);
      return expectToEqual([3, 2, 1])(
      y);
    });
    test(2, function(){
      var x, y;
      x = [1, 2, 3];
      y = reverseM(x);
      return expectToBe(x)(
      y);
    });
  });
});
describe('last', function(){
  var x, y;
  x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  y = [];
  test(1, function(){
    return expectToEqual(10)(
    last(
    x));
  });
  test('undef on empty', function(){
    return expectToEqual(void 8)(
    last(
    y));
  });
});
describe('head, tail', function(){
  var x, y;
  x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  y = [];
  test('head', function(){
    expectToEqual(1)(
    head(
    x));
    return expectToEqual(void 8)(
    head(
    y));
  });
  test('tail', function(){
    expectToEqual([2, 3, 4, 5, 6, 7, 8, 9, 10])(
    tail(
    x));
    return expectToEqual([])(
    tail(
    y));
  });
});