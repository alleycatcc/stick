{
    join: r-join,
    tap: r-tap,
} = require 'ramda'

{
    odd, even,
} = require 'prelude-ls'

{
    list,
    test, xtest,
    expect-to-equal, expect-to-be,
    expect-predicate,
    expect-to-be-truthy, expect-to-be-falsy,
    expect-to-have-typeof, expect-to-have-type-of,
    expect-to-contain-object,
    expect-not-to-equal,
    expect-not-to-be,
} = require './common'

{
    map, filter, reject,
    reduce, reduce-right, reduce-right-c,
    find, contains, contains-v,
    find-index, find-with-index,
    each, each-obj, each-obj-in,
    keys, keys-in,
    values, values-in,

    map-keys, map-values, map-tuples,
    remap-keys, remap-values, remap-tuples,
    with-filter, from-pairs, to-pairs,

    add-index, add-index2,
    add-collection, add-collection2,
    reduce-obj, reduce-obj-in,

    default-to, default-to-v,

    join, split,

    prop, prop-of, path, path-of,
    has, has-in,

    assoc, assoc-m,
    assoc-path, assoc-path-m,
    update-m, update, update-path-m, update-path,
    append-to, append-to-m, append, append-m,
    prepend, prepend-m, prepend-to, prepend-to-m,
    concat-to, concat-to-m, concat, concat-m,
    precat-to, precat,

    merge-to, merge, merge-to-m, merge-m,
    merge-in-to, merge-in, merge-in-to-m, merge-in-m,

    merge-to-sym, merge-sym, merge-to-m-sym, merge-m-sym,
    merge-in-to-sym, merge-in-sym, merge-in-to-m-sym, merge-in-m-sym,

    merge-all-in,
    merge-with,
    merge-when,

    discard-prototype, flatten-prototype,

    asterisk1,
    ampersand-n, asterisk-n,

    drop, take,

    arg0, arg1, arg2, arg3, arg4, arg5, arg6,

    deconstruct, deconstruct2, deconstruct-n,

} = require '../cjs/index'

{
    remap-keys: manual-remap-keys,
    remap-values: manual-remap-values,
    remap-tuples: manual-remap-tuples,
    map-keys: manual-map-keys,
    map-values: manual-map-values,
    map-tuples: manual-map-tuples,
} = require '../cjs/map-manual'

sort-alpha = (.sort ())
sort-num = (.sort ((a, b) -> a - b))

noop = ->
choose-src = (a, b) -> a
choose-tgt = (a, b) -> b

# --- the -null versions are to prove that the collision function is not called in certain cases.

merge-to-choose-tgt-m = merge-to-m-sym |> merge-with choose-tgt
merge-to-choose-tgt   = merge-to-sym   |> merge-with choose-tgt
merge-to-choose-src-m = merge-to-m-sym |> merge-with choose-src
merge-to-choose-src   = merge-to-sym   |> merge-with choose-src
merge-to-with-null-m  = merge-to-m-sym |> merge-with null
merge-to-with-null    = merge-to-sym   |> merge-with null
merge-to-with-noop-m  = merge-to-m-sym |> merge-with noop
merge-to-with-noop    = merge-to-sym   |> merge-with noop

merge-in-to-choose-tgt-m = merge-in-to-m-sym |> merge-with choose-tgt
merge-in-to-choose-tgt   = merge-in-to-sym   |> merge-with choose-tgt
merge-in-to-choose-src-m = merge-in-to-m-sym |> merge-with choose-src
merge-in-to-choose-src   = merge-in-to-sym   |> merge-with choose-src
merge-in-to-with-noop-m  = merge-in-to-m-sym |> merge-with noop
merge-in-to-with-noop    = merge-in-to-sym   |> merge-with noop

merge-choose-tgt-m    = merge-m-sym    |> merge-with choose-tgt
merge-choose-tgt      = merge-sym      |> merge-with choose-tgt
merge-choose-src-m    = merge-m-sym    |> merge-with choose-src
merge-choose-src      = merge-sym      |> merge-with choose-src
merge-with-null-m     = merge-m-sym    |> merge-with null
merge-with-null       = merge-sym      |> merge-with null
merge-with-noop-m     = merge-m-sym    |> merge-with noop
merge-with-noop       = merge-sym      |> merge-with noop

merge-in-choose-tgt-m    = merge-in-m-sym    |> merge-with choose-tgt
merge-in-choose-tgt      = merge-in-sym      |> merge-with choose-tgt
merge-in-choose-src-m    = merge-in-m-sym    |> merge-with choose-src
merge-in-choose-src      = merge-in-sym      |> merge-with choose-src
merge-in-with-null-m     = merge-in-m-sym    |> merge-with null
merge-in-with-null       = merge-in-sym      |> merge-with null
merge-in-with-noop-m     = merge-in-m-sym    |> merge-with noop
merge-in-with-noop       = merge-in-sym      |> merge-with noop

src-ok = (s, _) -> s?
tgt-ok = (_, t) -> t?

merge-to-when-src-ok-m = merge-to-m-sym |> merge-when src-ok
merge-to-when-src-ok   = merge-to-sym   |> merge-when src-ok
merge-to-when-tgt-ok-m = merge-to-m-sym |> merge-when tgt-ok
merge-to-when-tgt-ok   = merge-to-sym   |> merge-when tgt-ok
merge-when-src-ok-m = merge-m-sym |> merge-when src-ok
merge-when-src-ok   = merge-sym   |> merge-when src-ok
merge-when-tgt-ok-m = merge-m-sym |> merge-when tgt-ok
merge-when-tgt-ok   = merge-sym   |> merge-when tgt-ok

describe 'map, filter, reject, each' !->
    describe 'map' !->
        map-x = map |> add-index
        map-x-c = map |> add-index |> add-collection
        map-c-x = map |> add-collection |> add-index

        test 1 ->
            [1 2 3] |> map (* 2) |> expect-to-equal [2 4 6]
        test 'capped' ->
            [1 2 3]
            |> map (x, arg2) -> arg2
            |> expect-to-equal [void void void]
        test 'indexed' ->
            [1 2 3]
            |> map-x (x, idx) -> idx
            |> expect-to-equal [0 1 2]
        test 'mapXC' ->
            [1 2 3]
            |> map-x-c (x, i, c) -> i * c.length
            |> expect-to-equal [0 3 6]
        test 'mapCX' ->
            [1 2 3]
            |> map-c-x (x, c, i) -> i * c.length
            |> expect-to-equal [0 3 6]
    describe 'filter' !->
        filter-x = filter |> add-index
        filter-x-c = filter |> add-index |> add-collection
        filter-c-x = filter |> add-collection |> add-index

        test 1 ->
            [1 2 3] |> filter (odd) |> expect-to-equal [1 3]
        test 'capped' ->
            [1 2 3]
            |> filter (x, arg2) -> arg2
            |> expect-to-equal []
        test 'indexed' ->
            [1 2 3]
            |> filter-x (x, idx) -> idx == 2
            |> expect-to-equal [3]
        test 'filterXC' ->
            [1 2 3]
            |> filter-x-c (x, i, c) -> i * c.length == 3
            |> expect-to-equal [2]
        test 'filterCX' ->
            [1 2 3]
            |> filter-c-x (x, c, i) -> i * c.length == 3
            |> expect-to-equal [2]

    describe 'reject' !->
        reject-x = reject |> add-index
        reject-x-c = reject |> add-index |> add-collection
        reject-c-x = reject |> add-collection |> add-index

        test 1 ->
            [1 2 3] |> reject (even) |> expect-to-equal [1 3]
        test 'capped' ->
            [1 2 3]
            |> reject (x, arg2) -> arg2 == void
            |> expect-to-equal []
        test 'indexed' ->
            [1 2 3]
            |> reject-x (x, idx) -> idx == 2
            |> expect-to-equal [1 2]
        test 'rejectXC' ->
            [1 2 3]
            |> reject-x-c (x, i, c) -> i * c.length == 3
            |> expect-to-equal [1 3]
        test 'rejectCX' ->
            [1 2 3]
            |> reject-c-x (x, c, i) -> i * c.length == 3
            |> expect-to-equal [1 3]

    describe 'each' !->
        each-x = each |> add-index
        each-x-c = each |> add-index |> add-collection
        each-c-x = each |> add-collection |> add-index
        y = y: []
        ping = (x) -> y.y.push x

        before-each -> y.y = []
        test 1 ->
            [1 2 3] |> each (x) -> ping x
            y.y |> expect-to-equal [1 2 3]
        test 'capped' ->
            [1 2 3] |> each (x, arg2) -> ping arg2
            y.y |> expect-to-equal [void void void]
        test 'indexed' ->
            [1 2 3] |> each-x (x, idx) -> ping idx
            y.y |> expect-to-equal [0 1 2]
        test 'eachXC' ->
            [1 2 3] |> each-x-c (x, i, c) -> ping i * c.length
            y.y |> expect-to-equal [0 3 6]
        test 'eachCX' ->
            [1 2 3] |> each-c-x (x, c, i) -> ping i * c.length
            y.y |> expect-to-equal [0 3 6]

    describe 'eachObj' !->
        each-obj-x = each-obj |> add-index
        each-obj-x-c = each-obj |> add-index |> add-collection
        each-obj-c-x = each-obj |> add-collection |> add-index
        base = base-val: 15
        o = (Object.create base) <<< a: 1 b: 2
        y =
            y: void
            z: []

        before-each -> y.y = {}; y.z = []
        test 1 ->
            o |> each-obj (v, k) -> y.y[k] = v
            y.y |> expect-to-equal a: 1 b: 2
        test 'capped' ->
            o |> each-obj (v, k, arg3) -> y.z.push arg3
            y.z |> expect-to-equal [void void]
        test 'indexed' ->
            o |> each-obj-x (v, k, idx) -> y.y[k] = v; y.z.push idx
            y.y |> expect-to-equal a: 1 b: 2
            y.z |> expect-to-equal [0 1]
        test 'eachObjXC' ->
            o |> each-obj-x-c (v, k, idx, c) -> y.y[k] = v; y.z.push idx; y.z.push c
            y.y |> expect-to-equal a: 1 b: 2
            y.z |> expect-to-equal [0, o, 1, o]
        test 'eachObjCX' ->
            o |> each-obj-c-x (v, k, c, idx) -> y.y[k] = v; y.z.push idx; y.z.push c
            y.y |> expect-to-equal a: 1 b: 2
            y.z |> expect-to-equal [0, o, 1, o]

describe 'reduce' !->
    describe 'reduce' !->
        reduce-x = reduce |> add-index2
        reduce-c = reduce |> add-collection2
        reduce-x-c = reduce |> add-index2 |> add-collection2
        reduce-c-x = reduce |> add-collection2 |> add-index2

        test 'right order (acc, x)' ->
            [1 2 3]
            |> reduce (-), 15
            |> expect-to-equal 9
        test 'capped at 2' ->
            ret = []
            [1 2 3]
            |> reduce do
                (x, y, arg3) -> ret.push arg3
                42
            ret |> expect-to-equal [void void void]
        test 'capped at 2' ->
            join-all = (...args) -> args.join ''
            ['x' 'y' 'z']
            |> reduce do
                join-all
                'w'
            |> expect-to-equal 'wxyz'
        test 'indexed' ->
            ret = []
            [1 2 3]
            |> reduce-x do
                (x, y, idx) -> ret.push idx
                42
            ret |> expect-to-equal [0 1 2]
        test 'indexed curry' ->
            ret = []
            reducer = reduce-x do
                (x, y, idx) -> ret.push idx
            [1 2 3]
            |> reducer 42
            ret |> expect-to-equal [0 1 2]
        test 'with-collection' ->
            ret = []
            xs = [1 2 3]
            xs |> reduce-c do
                (x, y, coll) -> ret.push coll
                42
            ret |> expect-to-equal [xs, xs, xs]
        test 'reduceXC' ->
            [1 2 3]
            |> reduce-x-c do
                (acc, x, i, c) -> acc + i * c.length
                10
            |> expect-to-equal 19
        test 'reduceCX' ->
            [1 2 3]
            |> reduce-c-x do
                (acc, x, c, i) -> acc + i * c.length
                10
            |> expect-to-equal 19

    describe 'reduceRight' !->
        reduce-right-x = reduce-right |> add-index2
        reduce-right-coll = reduce-right |> add-collection2
        reduce-right-x-c = reduce-right |> add-index2 |> add-collection2
        reduce-right-c-x = reduce-right |> add-collection2 |> add-index2

        test 'right order (x, acc)' ->
            [14, 15, 16]
            |> reduce-right (-), 1
            |> expect-to-equal 14
        test 'capped at 2' ->
            ret = []
            [1 2 3]
            |> reduce-right do
                (x, y, arg3) -> ret.push arg3
                42
            ret |> expect-to-equal [void void void]
        test 'capped at 2' ->
            join-all = (...args) -> args.join ''
            ['x' 'y' 'z']
            |> reduce-right do
                join-all
                'w'
            |> expect-to-equal 'xyzw'
        test 'indexed' ->
            ret = []
            [1 2 3]
            |> reduce-right-x do
                (x, y, idx) -> ret.push idx
                42
            ret |> expect-to-equal [0 1 2]
        test 'indexed curry' ->
            ret = []
            reduce-rightr = reduce-right-x do
                (x, y, idx) -> ret.push idx
            [1 2 3]
            |> reduce-rightr 42
            ret |> expect-to-equal [0 1 2]
        test 'with-collection' ->
            ret = []
            xs = [1 2 3]
            xs |> reduce-right-coll do
                (x, y, coll) -> ret.push coll
                42
            ret |> expect-to-equal [xs, xs, xs]
        test 'reduce-rightXC' ->
            [1 2 3]
            |> reduce-right-x-c do
                (x, acc, i, c) -> acc + i * c.length
                10
            |> expect-to-equal 19
        test 'reduce-rightCX' ->
            [1 2 3]
            |> reduce-right-c-x do
                (x, acc, c, i) -> acc + i * c.length
                10
            |> expect-to-equal 19
    describe 'reduceRightC' !->
        test 1 ->
            to-obj = reduce-right-c do
                (x) ->
                    y = {}
                    y[x] = true
                    merge y
                {}
            ['a' 'b' 'c']
            |> to-obj
            |> expect-to-equal a: true, b: true, c: true
        # --- can not be used with add-index/add-collection -- will require a separate (curried)
        # version of those.
        xtest 'indexed' ->
            reduce-right-c-x = reduce-right-c |> add-index2-c
            ret = []
            ['a' 'b' 'c']
            |> reduce-right-c-x do
                (x) -> (y) -> (idx) -> ret.push idx
                42
            ret |> expect-to-equal [0 1 2]

describe 'find, contains' !->
    describe 'find' !->
        find-x = find |> add-index
        find-c = find |> add-collection
        find-x-c = find |> add-index |> add-collection
        find-c-x = find |> add-collection |> add-index

        test '1' ->
            [1 2 3 4]
            |> find (> 2)
            |> expect-to-equal 3
        test 'capped at 1' ->
            ret = []
            [1 2 3 4]
            |> find (_, arg2) ->
                ret.push arg2
                false
            ret |> expect-to-equal [void void void void]
        test 'indexed' ->
            ret = []
            [1 2 3 4]
            |> find-x (_, i) ->
                ret.push i
                false
            ret |> expect-to-equal [0 1 2 3]
        test 'with-collection' ->
            ret = []
            xs = [1 2 3]
            xs |> find-c (_, c) ->
                ret.push c
                false
            ret |> expect-to-equal [xs, xs, xs]
        test 'findXC' ->
            ret = []
            [1 2 3]
            |> find-x-c (_, i, c) ->
                ret.push(i * c.length)
                false
            ret |> expect-to-equal [0 3 6]
        test 'findCX' ->
            ret = []
            [1 2 3]
            |> find-c-x (_, c, i) ->
                ret.push(i * c.length)
                false
            ret |> expect-to-equal [0 3 6]
    describe 'findIndex' !->
        find-index-x = find-index |> add-index
        find-index-c = find-index |> add-collection
        find-index-x-c = find-index |> add-index |> add-collection
        find-index-c-x = find-index |> add-collection |> add-index

        test '1' ->
            [1 2 3 4]
            |> find-index (> 2)
            |> expect-to-equal 2
        test 'capped at 1' ->
            ret = []
            [1 2 3 4]
            |> find-index (_, arg2) ->
                ret.push arg2
                false
            ret |> expect-to-equal [void void void void]
        test 'indexed' ->
            ret = []
            [1 2 3 4]
            |> find-index-x (_, i) ->
                ret.push i
                false
            ret |> expect-to-equal [0 1 2 3]
        test 'with-collection' ->
            ret = []
            xs = [1 2 3]
            xs |> find-index-c (_, c) ->
                ret.push c
                false
            ret |> expect-to-equal [xs, xs, xs]
        test 'findIndexXC' ->
            ret = []
            [1 2 3]
            |> find-index-x-c (_, i, c) ->
                ret.push(i * c.length)
                false
            ret |> expect-to-equal [0 3 6]
        test 'findIndexCX' ->
            ret = []
            [1 2 3]
            |> find-index-c-x (_, c, i) ->
                ret.push(i * c.length)
                false
            ret |> expect-to-equal [0 3 6]
    describe 'findWithIndex' !->
        find-with-index-x = find-with-index |> add-index
        find-with-index-c = find-with-index |> add-collection
        find-with-index-x-c = find-with-index |> add-index |> add-collection
        find-with-index-c-x = find-with-index |> add-collection |> add-index

        test '1' ->
            [1 2 3 4]
            |> find-with-index (> 2)
            |> expect-to-equal [3, 2]
        test 'not found' ->
            [1 2 3 4]
            |> find-with-index (> 4)
            |> expect-to-equal [void, void]
        test 'capped at 1' ->
            ret = []
            [1 2 3 4]
            |> find-with-index (_, arg2) ->
                ret.push arg2
                false
            ret |> expect-to-equal [void void void void]
        test 'indexed' ->
            ret = []
            [1 2 3 4]
            |> find-with-index-x (_, i) ->
                ret.push i
                false
            ret |> expect-to-equal [0 1 2 3]
        test 'with-collection' ->
            ret = []
            xs = [1 2 3]
            xs |> find-with-index-c (_, c) ->
                ret.push c
                false
            ret |> expect-to-equal [xs, xs, xs]
        test 'findWithIndexXC' ->
            ret = []
            [1 2 3]
            |> find-with-index-x-c (_, i, c) ->
                ret.push(i * c.length)
                false
            ret |> expect-to-equal [0 3 6]
        test 'findWithIndexCX' ->
            ret = []
            [1 2 3]
            |> find-with-index-c-x (_, c, i) ->
                ret.push(i * c.length)
                false
            ret |> expect-to-equal [0 3 6]
    describe 'containsV' !->
        test '1' ->
            [1 2 3 4]
            |> contains-v 3
            |> expect-to-equal true
        test '1' ->
            [1 2 3 4]
            |> contains-v 5
            |> expect-to-equal false
    describe 'contains' !->
        test '1' ->
            [1 2 3 4]
            |> contains odd
            |> expect-to-equal true
        test '1' ->
            [1 3 5]
            |> contains even
            |> expect-to-equal false

describe 'reduceObj' !->
    base = base-val: 15
    o = (Object.create base) <<< a: 1 b: 2
    # --- remember not to depend on order.
    describe 'reduceObj' !->
        test 1 ->
            f = (acc, [k, v]) -> [...acc, "\"#k\": #v"]
            reduced = o |> reduce-obj f, []
            json = reduced
                |> r-join ', '
                |> (x) -> "{#x}"
            (JSON.parse json).a
            |> expect-to-equal 1
            (JSON.parse json).base-val
            |> expect-to-equal void
    describe 'reduceObjIn' !->
        test 1 ->
            f = (acc, [k, v]) -> [...acc, "\"#k\": #v"]
            reduced = o |> reduce-obj-in f, []
            json = reduced
                |> r-join ', '
                |> (x) -> "{#x}"
            (JSON.parse json).a
            |> expect-to-equal 1
            (JSON.parse json).base-val
            |> expect-to-equal 15

describe 'keys, values' !->
    base = base-val: 10
    o = (Object.create base) <<< one: 1 two: 2
    sort-alpha = (.sort ())
    sort-num = (.sort ((a, b) -> a - b))
    test 'keys' ->
        o |> keys
          |> sort-alpha
          |> expect-to-equal <[ one two ]>
    xtest 'keysIn' ->
        o |> keys-in
          |> sort-alpha
          |> expect-to-equal <[ baseVal one two ]>
    test 'values' ->
        o |> values
          |> sort-num
          |> expect-to-equal [1 2]
    xtest 'valuesIn' ->
        o |> values-in
          |> sort-num
          |> expect-to-equal [1 2 10]

describe 'remap' !->
    o = one: 1 two: 2
    # --- we assume that Node.js will return consistent orders.
    describe 'remapKeys' !->
        test 1 ->
            o |> remap-keys (.to-upper-case ())
              |> expect-to-equal ['ONE' 'TWO']
        test 'non-curried' ->
            remap-keys (.to-upper-case ()), o
            |> expect-to-equal ['ONE' 'TWO']
    describe 'remapValues' !->
        test 1 ->
            o |> remap-values (* 10)
              |> expect-to-equal [10 20]
        test 'non-curried' ->
            remap-values (* 10), o
            |> expect-to-equal [10 20]
    describe 'remapTuples' !->
        test 1 ->
            o |> remap-tuples ((x, y) -> [x, [x] * y])
              |> expect-to-equal [['one', ['one']], ['two', ['two' 'two']]]
        test 'non-curried' ->
            remap-tuples ((x, y) -> [x, [x] * y]), o
            |> expect-to-equal [['one', ['one']], ['two', ['two' 'two']]]

describe 'remap + with filter' !->
    o = one: 1 two: 2
    map-k-reject-starts-with-O = remap-keys |> with-filter (.0 != 'O')
    manual-map-k-reject-starts-with-O = manual-remap-keys |> with-filter (.0 != 'O')
    map-v-reject-even = remap-values |> with-filter odd
    manual-map-v-reject-even = manual-remap-values |> with-filter odd
    map-t-reject-k-O-and-v-even = remap-tuples
        |> with-filter (k, v) -> k.0 != 'O' or odd v
    manual-map-t-reject-k-O-and-v-even = manual-remap-tuples
        |> with-filter (k, v) -> k.0 != 'O' or odd v
    describe 'remapKeysWithFilter' !->
        test 1 ->
            o |> map-k-reject-starts-with-O (.to-upper-case ())
              |> expect-to-equal <[ TWO ]>
        test 'non-curried' ->
            map-k-reject-starts-with-O (.to-upper-case ()), o
            |> expect-to-equal <[ TWO ]>
        test 'manual' ->
            o |> manual-map-k-reject-starts-with-O (.to-upper-case ())
              |> expect-to-equal <[ TWO ]>
    describe 'remapValuesWithFilter' !->
        test 1 ->
            o |> map-v-reject-even (+ 1)
              |> expect-to-equal [3]
        test 'non-curried' ->
            map-v-reject-even (+ 1), o
            |> expect-to-equal [3]
        test 'manual' ->
            o |> manual-map-v-reject-even (+ 1)
              |> expect-to-equal [3]
    describe 'remapTuplesWithFilter' !->
        test 1 ->
            o |> map-t-reject-k-O-and-v-even (k, v) -> [k.to-upper-case (), v + 1]
              |> expect-to-equal [['TWO', 3]]
        test 'non-curried' ->
            map-t-reject-k-O-and-v-even do
                (k, v) -> [k.to-upper-case (), v + 1]
                o
            |> expect-to-equal [['TWO', 3]]
        test 'manual' ->
            o |> manual-map-t-reject-k-O-and-v-even (k, v) -> [k.to-upper-case (), v + 1]
              |> expect-to-equal [['TWO', 3]]

describe 'map keys/values/tuples' !->
    o = one: 1 two: 2
    test 'mapKeys' ->
        o |> map-keys (.to-upper-case ())
          |> expect-to-equal do
              TWO: 2
              ONE: 1
    test 'mapValues' ->
        o |> map-values (* 10)
          |> expect-to-equal do
              one: 10
              two: 20
    test 'mapTuples' ->
        o |> map-tuples (k, v) -> [k.to-upper-case (), v + 1]
          |> expect-to-equal do
              TWO: 3
              ONE: 2

describe 'map + with filter' !->
    o = one: 1 two: 2
    map-k-reject-starts-with-O = map-keys |> with-filter (.0 != 'O')
    map-v-odd = map-values |> with-filter odd
    map-tuples-v-odd = map-tuples |> with-filter (k, v) -> odd v
    manual-map-k-reject-starts-with-O = manual-map-keys |> with-filter (.0 != 'O')
    manual-map-v-odd = manual-map-values |> with-filter odd
    manual-map-tuples-v-odd = manual-map-tuples |> with-filter (k, v) -> odd v
    describe 'mapKeysWithFilter' !->
        test 1 ->
            o |> map-k-reject-starts-with-O (.to-upper-case ())
              |> expect-to-equal TWO: 2
        test 'non-curried' ->
            map-k-reject-starts-with-O (.to-upper-case ()), o
            |> expect-to-equal TWO: 2
        test 'manual' ->
            o |> manual-map-k-reject-starts-with-O (.to-upper-case ())
              |> expect-to-equal TWO: 2
    describe 'mapValuesWithFilter' !->
        test 1 ->
            o |> map-v-odd (+ 1)
              |> expect-to-equal two: 3
        test 'non-curried' ->
            map-v-odd (+ 1), o
            |> expect-to-equal two: 3
        test 'manual' ->
            o |> manual-map-v-odd (+ 1)
              |> expect-to-equal two: 3
    describe 'mapTuplesWithFilter' !->
        test 1 ->
            o |> map-tuples-v-odd (k, v) -> [k.to-upper-case (), v + 1]
              |> expect-to-equal TWO: 3
        test 'non-curried' ->
            map-tuples-v-odd do
                (k, v) -> [k.to-upper-case (), v + 1]
                o
            |> expect-to-equal TWO: 3
        test 'manual' ->
            o |> manual-map-tuples-v-odd (k, v) -> [k.to-upper-case (), v + 1]
              |> expect-to-equal TWO: 3

describe 'fromPairs, toPairs' !->
    describe 'fromPairs' !->
        test 1 ->
            [['a' 1], ['b' 2]]
            |> from-pairs
            |> expect-to-equal a: 1, b: 2
    describe 'toPairs' !->
        test 1 ->
            (a: 1 b: 2)
            |> to-pairs
            |> expect-to-equal [['a' 1], ['b' 2]]

describe 'join, split' !->
    describe 'join' !->
        test 1 ->
            [1 to 4] |> join ','
            |> expect-to-equal '1,2,3,4'
    describe 'split' !->
        test 1 ->
            '1,2,3,4' |> split ',' |> map Number
            |> expect-to-equal [1 to 4]

describe 'default to' !->
    test 1 ->
        false
        |> default-to -> 42
        |> expect-to-equal false
    test 2 ->
        null
        |> default-to -> 42
        |> expect-to-equal 42
    test 3 ->
        void
        |> default-to -> 42
        |> expect-to-equal 42

describe 'default to value' !->
    test 1 ->
        false
        |> default-to-v 43
        |> expect-to-equal false
    test 2 ->
        null
        |> default-to-v 42
        |> expect-to-equal 42
    test 3 ->
        void
        |> default-to-v 42
        |> expect-to-equal 42

describe 'default to __' !->
    return
    test 1 ->
        default-to__ false -> 42
        |> expect-to-equal false
    test 2 ->
        default-to__ null -> 42
        |> default-to -> 42
        |> expect-to-equal 42
    test 3 ->
        default-to__ void -> 42
        |> default-to -> 42
        |> expect-to-equal 42

describe 'object stuff' !->
    # --- vals always get loaded from src into target (direction of pipe is
    # changed).

    run = (args) ->
        { fn, src, tgt, dir, } = args
        if dir == 'to' then src |> fn tgt
        else                tgt |> fn src

    test-m = (args) ->
        { res, mut, tgt, } = args
        if mut then (expect res).to-be tgt
        else        (expect res).not.to-be tgt

    describe 'assoc' !->
        base = base-val: 10
        orig = (Object.create base) <<< a: 1 b:2
        nieuw = orig |> assoc 'b' 3
        test 1 ->
            (expect nieuw).not.to-be orig
            nieuw.a |> expect-to-equal 1
            nieuw.b |> expect-to-equal 3
        test 'flattens proto' ->
            nieuw.base-val |> expect-to-equal 10

    describe 'assocM' !->
        test 1 ->
            orig = a: 1 b:2
            nieuw = orig
                |> assoc-m 'b' 3
            (expect nieuw).to-be orig
            (expect nieuw).to-equal a: 1 b: 3

    describe 'assocPath' !->
        o = o: void
        before-each ->
            base = base-val: 10
            o.o = (Object.create base) <<<
                a: 1 b: 2
                c:
                   w: null
                   x: void
                   y: 3
                   z:
                       zz: 10
                       yy: 11
            o.p =
                a: 1 b: 2
                c: [10 11 [a: 1]]
            o.q =
                a: 1 b: 2
                c: new Date
                d: new Error

        # --- spot-check non-mutable version.
        test 1 ->
            o.o |> assoc-path-m <[ a ]> 2
            o.o |> expect-to-contain-object a: 2 b: 2

            o.o |> assoc-path <[ a ]> 2
                |> r-tap expect-to-contain-object a: 2 b: 2
                |> expect-not-to-be o.o
        test 2 ->
            o.o |> assoc-path-m <[ d ]> 2
            o.o |> expect-to-contain-object a: 1 b: 2 d: 2
        test 3 ->
            o.o |> assoc-path-m <[ a c ]> 3
            o.o |> expect-to-contain-object b: 2 a: c: 3
        test 4 ->
            o.o |> assoc-path-m <[ d a ]> 3
            o.o |> expect-to-contain-object a:1 d: a: 3
        test 5 ->
            o.o |> assoc-path-m <[ a d ]> 3
            o.o |> expect-to-contain-object do
                b: 2 a: d: 3
        test 6 ->
            o.o |> assoc-path-m <[ c w ]> void
            o.o.c.w |> expect-to-equal void
        test 7 ->
            o.o |> assoc-path-m <[ c w ]> 10
            o.o.c.w |> expect-to-equal 10
        test 8 ->
            o.o |> assoc-path-m <[ c w ]> null
            o.o.c.w |> expect-to-equal null
        test 9 ->
            o.p |> assoc-path-m <[ c 1 ]> 1
            o.p |> expect-to-equal do
                a: 1 b: 2 c: [10 1 [a: 1]]

            o.p |> assoc-path <[ c 1 ]> 1
                |> r-tap expect-to-equal do
                    a: 1 b: 2 c: [10 1 [a: 1]]
                |> expect-not-to-be o.p
        test 10 ->
            # --- number will work, though it relies on coercion.
            o.p |> assoc-path-m ['c' 1] 1
            o.p |> expect-to-equal do
                a: 1 b: 2 c: [10 1 [a: 1]]
        test 11 ->
            o.p |> assoc-path-m ['c' '2' '0' 'a'] 2
            o.p |> expect-to-equal do
                a: 1 b: 2 c: [10 11 [a: 2]]
        test 11 ->
            o.p |> assoc-path-m ['c' '2' '0' 'a'] 2
            o.p |> expect-to-equal do
                a: 1 b: 2 c: [10 11 [a: 2]]
        test 12 ->
            o.q |> assoc-path-m <[ c d ]> 2
            o.q |> expect-to-contain-object a: 1 c: d: 2
        test 'non-m flattens proto' ->
            o.o |> assoc-path <[ c ]> 1
                |> expect-to-contain-object base-val: 10 c: 1

    describe 'update' !->
        o = {}
        before-each ->
            base = base-val: 10
            o.o = (Object.create base) <<<
                a: 1 b: 2
                c:
                   w: null
                   x: void
                   y: 3
                   z:
                       zz: 10
                       yy: 11
            o.p =
                a: 1 b: 2
                c: [10 11 [a: 1]]
            o.q =
                a: 1 b: 2
                c: new Date
                d: new Error

        describe 'update' !->
            test 1 ->
                o.o |> update-m 'a' (* 51)
                o.o |> expect-to-contain-object a: 51 b:2
            test 'vivify m' ->
                o.o |> update-m 'nonexistent' (* 51)
                o.o |> expect-to-contain-object b: 2 nonexistent: NaN
            test 2 ->
                o.o |> update   'a' (* 51)
                    |> r-tap expect-to-contain-object a: 51 b:2
                    |> expect-not-to-be o.o
            test 'vivify' ->
                o.o |> update   'nonexistent' (* 51)
                    |> r-tap expect-to-contain-object b: 2 nonexistent: NaN
                    |> expect-not-to-be o.o

        # --- assume routes through path and assocPath
        describe 'updatePath' !->
            test 1 ->
                o.o |> update-path-m <[ c y ]> (* 10)
                o.o.c.y |> expect-to-equal 30
            test 'vivify M' ->
                o.o |> update-path-m <[ non existent ]> (* 10)
                o.o.non.existent |> expect-to-equal NaN
            test 2 ->
                o.o |> update-path   <[ c y ]> (* 10)
                    |> r-tap expect-predicate ({ c: { y } }) -> y == 30
                    |> expect-not-to-be o.o
            test 'vivify' ->
                o.o |> update-path   <[ non existent ]> (* 10)
                    |> r-tap expect-predicate do
                        ({ non: { existent } }) -> isNaN existent
                    |> expect-not-to-be o.o

    describe 'appendTo' !->
        fn = append-to
        dir = 'to'
        mut = false
        test 1 ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 2 3 [4 5 6]]
        test 2 ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 4]
        test 'eg' ->
            res = 3
            |> append-to [4]
            (expect res).to-equal [4 3]

    describe 'appendToM' !->
        fn = append-to-m
        dir = 'to'
        mut = true
        test 'array to array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 2 3 [4 5 6]]
        test 'elem to array' ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 4]
        test 'eg' ->
            res = 3
            |> append-to-m [4]
            (expect res).to-equal [4 3]

    describe 'append' !->
        fn = append
        dir = 'from'
        mut = false
        test 1 ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 2 3 [4 5 6]]
        test 2 ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 4]
        test 'eg' ->
            res = [3]
            |> append 4
            (expect res).to-equal [3 4]

    describe 'appendM' !->
        fn = append-m
        dir = 'from'
        mut = true
        test 'array -> array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 2 3 [4 5 6]]
        test 'elem -> array' ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 4]
        test 'eg' ->
            res = [4]
            |> append-m 3
            (expect res).to-equal [4 3]

    describe 'prependTo' !->
        fn = prepend-to
        dir = 'to'
        mut = false
        test 'array -> array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [[4 5 6] 1 2 3]
        test 'number -> array' ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [4 1 2 3]
        test 'eg' ->
            res = 3
            |> prepend-to [4]
            (expect res).to-equal [3 4]

    describe 'prepend' !->
        fn = prepend
        dir = 'from'
        mut = false
        test 'array -> array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [[4 5 6] 1 2 3]
        test 'element -> array' ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [4 1 2 3]
        test 'eg' ->
            res = [4]
            |> prepend 3
            (expect res).to-equal [3 4]

    describe 'prependM' !->
        fn = prepend-m
        dir = 'from'
        mut = true
        test 'array to array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [[4 5 6] 1 2 3]
        test 'elem to array' ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [4 1 2 3]
        test 'eg' ->
            res = [3]
            |> prepend-m 4
            (expect res).to-equal [4 3]

    describe 'prependToM' !->
        fn = prepend-to-m
        dir = 'to'
        mut = true
        test 'array to array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [[4 5 6] 1 2 3]
        test 'elem to array' ->
            tgt = [1 2 3]
            src = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [4 1 2 3]
        test 'eg' ->
            res = 4
            |> prepend-to-m [3]
            (expect res).to-equal [4 3]

    describe 'concatTo' !->
        fn = concat-to
        dir = 'to'
        mut = false
        test 'array -> array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 6]
        test 'string -> string' ->
            tgt = "don't give me no "
            src = 'jibber jabber'
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal tgt + src
        test 'unequal types => dont throw, unlike ramda' ->
            tgt = [1 2 3]
            src = 4
            (expect -> src |> concat-to tgt).not.to-throw()

    describe 'concatToM' !->
        fn = concat-to-m
        dir = 'to'
        mut = true
        test 1 ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 6]
        test 'strings -> throw' ->
            tgt = "don't give me no "
            src = 'jibber jabber'
            (expect -> src |> concat-to-m tgt).to-throw()

    describe 'concat' !->
        fn = concat
        dir = 'from'
        mut = false
        test 'array -> array' ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 6]
        test 'elem -> array' ->
            tgt = [1 2 3]
            src = 4
            expect(-> src |> concat tgt).to-throw()
        test 'alias' ->
            precat-to   |> expect-to-equal concat
            precat |> expect-to-equal concat-to

    describe 'concatM' !->
        fn = concat-m
        dir = 'from'
        mut = true
        test 1 ->
            tgt = [1 2 3]
            src = [4 5 6]
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal [1 to 6]

    describe 'mergeTo' !->
        fn = merge-to
        dir = 'to'
        mut = false
        test 1 ->
            tgt = a: 1 b: 2
            src =      b: 3 c: 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
        test 'also takes null/undef' ->
            tgt = a: 1 b: 2
            src =      b: 3 c: 4 d: void
            res = run do
                { fn, src, tgt, dir, }
            # --- = JS `in`
            ('d' of res) |> expect-to-be true
        test 'discards non-own vals on src' ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void
        test 'discards non-own vals on tgt' ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void
        test 'discards non-own vals 3' ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void

        # --- note that we can't use test-m (which uses expect.to-be) and doesn't catch this.
        test 'preserves symbol keys on src and tgt' ->
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            tgt = a:1 b:2
            src = c:3 d:4
            src[sym1] = 10
            tgt[sym2] = 11
            res = run do
                { fn, src, tgt, dir, }
            (expect res[sym1]).to-equal 10
            (expect res[sym2]).to-equal 11
            (expect get-syms res).to-equal do
                # --- insertion order
                (get-syms tgt) ++ get-syms src

    describe 'merge' !->
        fn = merge
        dir = 'from'
        mut = false
        test 1 ->
            tgt = a: 1 b: 2
            src =      b: 3 c: 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res).to-equal a: 1 b: 3 c: 4
        test 'discards non-own vals on src' ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void
        test 'discards non-own vals on tgt' ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void
        test 'discards non-own vals 3' ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void
        # --- note that we can't use test-m (which uses expect.to-be) and doesn't catch this.
        test 'preserves symbol keys on src and tgt' ->
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            tgt = a:1 b:2
            src = c:3 d:4
            src[sym1] = 10
            tgt[sym2] = 11
            res = run do
                { fn, src, tgt, dir, }
            (expect res[sym1]).to-equal 10
            (expect res[sym2]).to-equal 11
            # --- insertion order
            (expect get-syms res).to-equal [sym2, sym1]

    describe 'mergeToM' !->
        fn = merge-to-m
        dir = 'to'
        mut = true
        test 1 ->
            tgt = a: 1 b: 2
            src =      b: 3 c: 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
        test 'discards non-own on src' ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void
        test 'discards non-own on src, retains on tgt' ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal 42
        test 'discards non-own vals 3' ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
        # --- note that we can't use test-m (which uses expect.to-be) and doesn't catch this.
        test 'preserves symbol keys on src and tgt' ->
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            tgt = a:1 b:2
            src = c:3 d:4
            src[sym1] = 10
            tgt[sym2] = 11
            res = run do
                { fn, src, tgt, dir, }
            (expect res[sym1]).to-equal 10
            (expect res[sym2]).to-equal 11
            # --- insertion order
            (expect get-syms res).to-equal [sym2, sym1]

    describe 'mergeM' !->
        fn = merge-m
        dir = 'from'
        mut = true
        test 1 ->
            tgt = a: 1 b: 2
            src =      b: 3 c: 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
        test 'discards non-own on src' ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal void
        test 'discards non-own on src, retains on tgt' ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal 42
        test 'discards non-own vals 3' ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }
            (expect res).to-equal a: 1 b: 3 c: 4
        # --- note that we can't use test-m (which uses expect.to-be) and doesn't catch this.
        test 'preserves symbol keys on src and tgt' ->
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            tgt = a:1 b:2
            src = c:3 d:4
            src[sym1] = 10
            tgt[sym2] = 11
            res = run do
                { fn, src, tgt, dir, }
            (expect res[sym1]).to-equal 10
            (expect res[sym2]).to-equal 11
            # --- insertion order
            (expect get-syms res).to-equal [sym2, sym1]

    describe 'mergeToWith' !->
        describe 'main' !->
            var src, tgt, tgt-copy
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            before-each ->
                tgt := Object.create hidden1: 42
                    ..a = 1
                    ..b = 2
                tgt-copy := Object.create hidden1: 42
                    ..a = 1
                    ..b = 2
                src := Object.create hidden2: 42
                    ..c = 3
                    ..d = 4
                src[sym1] = 10
                tgt[sym2] = 11
                tgt-copy[sym2] = 11
            test 'when no collisions, acts like mergeTo M' ->
                src |> merge-to-with-noop-m tgt
                    |> expect-to-equal (src |> merge-to-m tgt-copy)
                get-syms tgt |> expect-to-equal get-syms tgt-copy
            test 'when no collisions, acts like mergeTo M in' ->
                src |> merge-in-to-with-noop-m tgt
                    |> expect-to-equal (src |> merge-in-to-m tgt-copy)
                get-syms tgt |> expect-to-equal get-syms tgt-copy
            test 'when no collisions, acts like mergeTo' ->
                ctrl = src |> merge-to tgt
                res = src |> merge-to-with-noop tgt
                res |> expect-to-equal ctrl
                get-syms res |> expect-to-equal get-syms ctrl
            test 'when no collisions, acts like mergeTo in' ->
                ctrl = src |> merge-in-to tgt
                res = src |> merge-in-to-with-noop tgt
                res |> expect-to-equal ctrl
                get-syms res |> expect-to-equal get-syms ctrl

        # --- testing all possible combinations is overkill.
        # suffice it to show that they can be composed with an arbitrary merge function and in an
        # various orders.
        describe 'with + when' !->
            var tgt, src
            src-odd = (a, b) -> odd a
            src-even = (a, b) -> even a
            tgt-odd = (a, b) -> odd b
            tgt-even = (a, b) -> even b
            # --- with then when needs specification
            describe 'with then when' !->
                return
                before-each ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 20
                    src := Object.create hidden: 42
                        ..b = 11
                        ..c = 12
                test 'with then when' ->
                    merger = merge-m |> merge-with choose-src |> merge-when src-odd
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 11
                test 'when test is applied before with test' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 21
                    src := Object.create hidden: 42
                        ..b = 10
                        ..c = 12
                    merger = merge-m |> merge-with choose-src |> merge-when src-odd
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 21
                test 'when test is applied before with test, 2' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 20
                    src := Object.create hidden: 42
                        ..b = 10
                        ..c = 12
                    merger = merge-m |> merge-with choose-src |> merge-when src-even
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 10
                        c: 12
                test 'when test is applied before with test, 3' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 21
                    src := Object.create hidden: 42
                        ..b = 10
                        ..c = 12
                    merger = merge-m |> merge-with choose-src |> merge-when src-odd
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 21
                test 'when test is applied before with test, 4' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 20
                    src := Object.create hidden: 42
                        ..b = 11
                        ..c = 12
                    merger = merge-m |> merge-with choose-src |> merge-when src-even
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 20
                        c: 12

            # --- with then when / when then with needs specification
            describe 'when then with' !->
                return
                test 'with then when' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 20
                    src := Object.create hidden: 42
                        ..b = 11
                        ..c = 12
                    merger = merge-m |> merge-when src-odd |> merge-with choose-src
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 11
                test 'when test is applied before with test' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 21
                    src := Object.create hidden: 42
                        ..b = 10
                        ..c = 12
                    merger = merge-m |> merge-when src-odd |> merge-with choose-src
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 21
                test 'when test is applied before with test, 2' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 20
                    src := Object.create hidden: 42
                        ..b = 10
                        ..c = 12
                    merger = merge-m |> merge-when src-even |> merge-with choose-src
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 10
                        c: 12
                test 'when test is applied before with test, 3' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 21
                    src := Object.create hidden: 42
                        ..b = 10
                        ..c = 12
                    merger = merge-m |> merge-when src-odd |> merge-with choose-src
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 21
                test 'when test is applied before with test, 4' ->
                    tgt := Object.create hidden: 43
                        ..a = 20
                        ..b = 20
                    src := Object.create hidden: 42
                        ..b = 11
                        ..c = 12
                    merger = merge-m |> merge-when tgt-even |> merge-with choose-src
                    tgt |> merger src
                    tgt |> expect-to-equal do
                        a: 20
                        b: 20
                        c: 12

        describe 'collisions' !->
            var tgt, src
            before-each ->
                tgt := Object.create hidden: 43
                    ..a = 'target a'
                    ..b = 'target b'
                src := Object.create hidden: 42
                    ..b = 'source b'
                    ..c = 'source c'
            test 'choose target M' ->
                src |> merge-to-choose-tgt-m tgt
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'target b'
                    c: 'source c'
            test 'choose source M' ->
                src |> merge-to-choose-src-m tgt
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'source b'
                    c: 'source c'
            test 'choose target' ->
                src |> merge-to-choose-tgt tgt
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'target b'
                        c: 'source c'
            test 'choose source' ->
                src |> merge-to-choose-src tgt
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'source b'
                        c: 'source c'


            test 'choose target M in' ->
                src |> merge-in-to-choose-tgt-m tgt
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'target b'
                    c: 'source c'
                    hidden: 43
            test 'choose source M in' ->
                src |> merge-in-to-choose-src-m tgt
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'source b'
                    c: 'source c'
                    hidden: 42
            test 'choose target in' ->
                src |> merge-in-to-choose-tgt tgt
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'target b'
                        c: 'source c'
                        hidden: 43
            test 'choose source in' ->
                src |> merge-in-to-choose-src tgt
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'source b'
                        c: 'source c'
                        hidden: 42

        # --- not sure this is necessary.
        describe 'no collision' !->
            var tgt, src
            before-each ->
                tgt := Object.create hidden: 43
                    ..a = 'target a'
                    ..b = 'target b'
                src := Object.create hidden: 42
                    ..c = 'source c'

            test 'collision f is not called M' ->
                src |> merge-to-with-null-m tgt
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'target b'
                    c: 'source c'

            test 'collision f is not called' ->
                src |> merge-to-with-null tgt
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'target b'
                        c: 'source c'

    describe 'mergeWith' !->
        describe 'main' !->
            var tgt, src, tgt-copy
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            before-each ->
                tgt := Object.create hidden1: 42
                    ..a = 1
                    ..b = 2
                tgt-copy := Object.create hidden1: 42
                    ..a = 1
                    ..b = 2
                src := Object.create hidden2: 42
                    ..c = 3
                    ..d = 4
                src[sym1] = 10
                tgt[sym2] = 11
                tgt-copy[sym2] = 11
            test 'when no collisions, acts like merge M' ->
                tgt |> merge-with-noop-m src
                    |> expect-to-equal (tgt-copy |> merge-m src)
                get-syms tgt |> expect-to-equal get-syms tgt-copy
            test 'when no collisions, acts like merge M in' ->
                tgt |> merge-in-with-noop-m src
                    |> expect-to-equal (tgt-copy |> merge-in-m src)
                get-syms tgt |> expect-to-equal get-syms tgt-copy
            test 'when no collisions, acts like merge' ->
                ctrl = tgt |> merge src
                res = tgt |> merge-with-noop src
                res |> expect-to-equal ctrl
                get-syms res |> expect-to-equal get-syms ctrl
            test 'when no collisions, acts like merge in' ->
                ctrl = tgt |> merge-in src
                res = tgt |> merge-in-with-noop src
                res |> expect-to-equal ctrl
                get-syms res |> expect-to-equal get-syms ctrl

        describe 'collisions' !->
            var tgt, src
            before-each ->
                tgt := Object.create hidden: 43
                    ..a = 'target a'
                    ..b = 'target b'
                src := Object.create hidden: 42
                    ..b = 'source b'
                    ..c = 'source c'
            test 'choose target M' ->
                tgt |> merge-choose-tgt-m src
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'target b'
                    c: 'source c'
            test 'choose source M' ->
                tgt |> merge-choose-src-m src
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'source b'
                    c: 'source c'
            test 'choose target' ->
                tgt |> merge-choose-tgt src
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'target b'
                        c: 'source c'
            test 'choose source' ->
                tgt |> merge-choose-src src
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'source b'
                        c: 'source c'

            test 'choose target M in' ->
                tgt |> merge-in-choose-tgt-m src
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'target b'
                    c: 'source c'
                    hidden: 43
            test 'choose source M in' ->
                tgt |> merge-in-choose-src-m src
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'source b'
                    c: 'source c'
                    hidden: 42
            test 'choose target in' ->
                tgt |> merge-in-choose-tgt src
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'target b'
                        c: 'source c'
                        hidden: 43
            test 'choose source' ->
                tgt |> merge-in-choose-src src
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'source b'
                        c: 'source c'
                        hidden: 42

        # --- not sure this is necessary.
        describe 'no collision' !->
            var tgt, src
            before-each ->
                tgt := Object.create hidden: 'target hidden'
                    ..a = 'target a'
                # --- src prototype is discarded anyway.
                src :=
                    b: 'source b'
                    c: 'source c'
                    hidden: 'source hidden'
            test 'proto chain of target is not checked M' ->
                tgt |> merge-with-null-m src
                tgt |> expect-to-equal do
                    a: 'target a'
                    b: 'source b'
                    c: 'source c'
                    hidden: 'source hidden'
            test 'proto chain of target is not checked' ->
                tgt |> merge-with-null src
                    |> expect-to-equal do
                        a: 'target a'
                        b: 'source b'
                        c: 'source c'
                        hidden: 'source hidden'

    describe 'mergeToIn' !->
        fn = merge-in-to
        dir = 'to'
        mut = false
        test 1 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 42
        test 2 ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43
        test 3 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43

    describe 'mergeIn' !->
        fn = merge-in
        dir = 'from'
        mut = false
        test 1 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 42
        test 2 ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43
        test 3 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43

    describe 'merge*WhenM' !->
        base = base-val: 10
        var tgt
        var src
        src-ok = (s, _) -> s?
        tgt-ok = (_, t) -> t?
        before-each ->
            tgt := (Object.create base) <<< a: 1 b: 2 c: null
            src := (Object.create base) <<< a: 3 b: null c: 4
        test 1 ->
            src |> merge-to-when-src-ok-m tgt
            tgt |> expect-to-equal a: 3 b: 2 c: 4
        test 2 ->
            src |> merge-to-when-tgt-ok-m tgt
            tgt |> expect-to-equal a: 3 b: null c: null
        test 3 ->
            tgt |> merge-when-src-ok-m src
            tgt |> expect-to-equal a: 3 b: 2 c: 4
        test 4 ->
            tgt |> merge-when-tgt-ok-m src
            tgt |> expect-to-equal a: 3 b: null c: null
        describe 'symbols' ->
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            merge-always = merge-when -> true
            test 'to M' ->
                src = a:1 b:2
                src[sym1] = 10
                tgt = b:3 c:4
                tgt[sym2] = 11
                src |> merge-always merge-to-m-sym, tgt
                (get-syms tgt) |> expect-to-equal [sym2, sym1]
            test 'M' ->
                src = a:1 b:2
                src[sym1] = 10
                tgt = b:3 c:4
                tgt[sym2] = 11
                src |> merge-always merge-m-sym, tgt
                (get-syms src) |> expect-to-equal [sym1, sym2]

    describe 'merge*When' !->
        base = base-val: 10
        tgt = (Object.create base) <<< a: 1 b: 2 c: null
        src = (Object.create base) <<< a: 3 b: null c: 4
        test 1 ->
            src |> merge-to-when-src-ok tgt
                |> expect-to-equal a: 3 b: 2 c: 4
        test 2 ->
            src |> merge-to-when-tgt-ok tgt
                |> expect-to-equal a: 3 b: null c: null
        test 3 ->
            tgt |> merge-when-src-ok src
                |> expect-to-equal a: 3 b: 2 c: 4
        test 4 ->
            tgt |> merge-when-tgt-ok src
                |> expect-to-equal a: 3 b: null c: null
        describe 'symbols' ->
            get-syms = Object.get-own-property-symbols
            sym1 = Symbol 5
            sym2 = Symbol 6
            merge-always = merge-when -> true
            test 'to' ->
                src = a:1 b:2
                src[sym1] = 10
                tgt = b:3 c:4
                tgt[sym2] = 11
                res = src |> merge-always merge-to-sym, tgt
                (get-syms res) |> expect-to-equal [sym2, sym1]
            test 'from' ->
                src = a:1 b:2
                src[sym1] = 10
                tgt = b:3 c:4
                tgt[sym2] = 11
                res = src |> merge-always merge-sym, tgt
                (get-syms res) |> expect-to-equal [sym1, sym2]


    describe 'mergeToInM' !->
        fn = merge-in-to-m
        dir = 'to'
        mut = true
        test 1 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal 42
        test 2 ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43
        test 3 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43

    describe 'mergeInM' !->
        fn = merge-in-m
        dir = 'from'
        mut = true
        test 1 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create {}
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4
            (expect res.hidden).to-equal 42
        test 2 ->
            tgt = Object.create {}
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43
        test 3 ->
            tgt = Object.create hidden: 42
                ..a = 1
                ..b = 2
            src = Object.create hidden: 43
                ..b = 3
                ..c = 4
            res = run do
                { fn, src, tgt, dir, }
            test-m do
                { res, mut, tgt, }

            (expect res).to-equal a: 1 b: 3 c: 4 hidden: 43

    describe 'mergeAllIn' !->
        test 'no prototypes' ->
            merge-all-in list do
                *   a: 1
                *   b: 2
                *   c: 3
            |> expect-to-equal do
                a: 1 b: 2 c: 3
        test 'with prototypes' ->
            merge-all-in list do
                { a: 1 } |> Object.create
                { b: 2 } |> Object.create
                { c: 3 } |> Object.create
            |> expect-to-equal do
                a: 1 b: 2 c: 3

describe 'prop, path, has' !->
    base = base-val: 10
    o = base <<<
        n: null
        a: 1 b: 2
        c:
           w: null
           x: void
           y: 3
           z:
               zz: 10
               yy: 11
    p =
        a: 1 b: 2
        c: [10 11 [a: 1]]
    q =
        a: 1 b: 2
        c: new Date
        d: new Error

    describe 'prop' !->
        test 1 ->
            o |> prop 'b'
              |> expect-to-equal 2

        test 2 ->
            o |> prop 'z'
              |> expect-to-equal void

        test 3 ->
            o |> prop 'n'
              |> expect-to-equal null

    describe 'propOf' !->
        test 1 ->
            'b' |> prop-of o
                |> expect-to-equal 2

        test 2 ->
            'z' |> prop-of o
                |> expect-to-equal void

        test 3 ->
            'n' |> prop-of o
                |> expect-to-equal null

    describe 'path' !->
        test 'path 1' ->
            o |> path <[ a ]>
              |> expect-to-equal 1
        test 'path 2' ->
            o |> path <[ c ]>
              |> expect-to-contain-object w: null x: void
        test 'path 3' ->
            o |> path <[ c w ]>
              |> expect-to-be null
        test 'path 4' ->
            o |> path <[ c x ]>
              |> expect-to-be void
        test 'path 5' ->
            o |> path <[ c a ]>
              |> expect-to-be void
        test 'path 6' ->
            o |> path <[ c y ]>
              |> expect-to-be 3
        test 'path 6.1' ->
            o |> path <[ c y y ]>
              |> expect-to-be void
        test 'path 6.2' ->
            o |> path <[ c y y y y y y ]>
              |> expect-to-be void
        test 'path 7' ->
            o |> path <[ c z zz ]>
              |> expect-to-be 10
        test 'path 8' ->
            o |> path <[ c z zz zz zz ]>
              |> expect-to-be void
        test 'path 9' ->
            p |> path <[ c 1 ]>
              |> expect-to-equal 11
        test 'path 10' ->
            # --- number will work, though it relies on coercion.
            p |> path ['c' 1]
              |> expect-to-equal 11
        test 'path 11' ->
            p |> path ['c' '2' '0' 'a']
              |> expect-to-equal 1
        # --- undefined, like ramda.
        test 'path 12' ->
            p |> path <[ c date ]>
              |> expect-to-equal void

    describe 'pathOf' !->
        test 'pathOf 1' ->
            <[ a ]> |> path-of o
                    |> expect-to-equal 1
        test 'pathOf 2' ->
            <[ c ]> |> path-of o
                    |> expect-to-contain-object w: null x: void

    describe 'has' !->
        test 1 ->
            o |> has 'c'
              |> expect-to-be true
            o |> has 'z'
              |> expect-to-be false
            o |> has 'base-val'
              |> expect-to-be false
        test 2 ->
            o |> has-in 'c'
              |> expect-to-be true
            o |> has-in 'z'
              |> expect-to-be false
            o |> has-in 'baseVal'
              |> expect-to-be true

describe 'discard / flatten prototype' !->
    describe 'discardPrototype' !->
        base = base-val: 10
        base2 = Object.create base
        obj = Object.create base2
        obj.base-val |> expect-to-equal 10
        after = obj |> discard-prototype
        after |> expect-not-to-be obj

        # --- of (LS) = in (JS)
        ('baseVal' of after) |> expect-to-equal false
        after.base-val |> expect-to-equal void

    describe 'flattenPrototype' !->
        base = Object.create base-val: 10
            ..feets = 'sometimes'
        base2 = Object.create base
            ..hands = 'mostways'
        obj = Object.create base2
            ..legs = 'noo'
        obj.base-val |> expect-to-equal 10
        after = obj |> flatten-prototype
        # --- of (LS) = in (JS)
        ('baseVal' of after) |> expect-to-equal true
        after |> expect-to-equal do
            base-val: 10 feets: 'sometimes' hands: 'mostways' legs: 'noo'

describe 'eachObjIn' !->
    test 'also enumerates prototype vals' ->
        ret = []
        do ->
            how: 'fine'
            are: 'thanks'
        |> Object.create
        |> each-obj-in (v, k) ->
            ret.push k
            ret.push v

        ret |> expect-to-equal <[ how fine are thanks ]>

            # --- apply function to arguments has more hits on google.
            # --- maybe pass -> prams
            #
            # --- apply -> func

describe 'asterisk' !->
    test 1 ->
        1 |> asterisk1 (*2)
          |> expect-to-equal [2]
    test 'n' ->
        [1 2 3]
        |> asterisk-n [(* 2), ( + 1), (/ 2)]
        |> expect-to-equal [2 3 1.5]

describe 'ampersand' !->
    test 1 ->
        10
        |> ampersand-n [(* 2), ( + 1), (/ 2)]
        |> expect-to-equal [20 11 5]

describe 'argx' !->
    test 'arg0' ->
        arg0 1
        |> expect-to-equal 1
    test 'arg1' ->
        arg1 1 2
        |> expect-to-equal 2
    test 'arg2' ->
        arg2 1 2 3
        |> expect-to-equal 3
    test 'arg3' ->
        arg3 1 2 3 4
        |> expect-to-equal 4
    test 'arg4' ->
        arg4 1 2 3 4 5
        |> expect-to-equal 5
    test 'arg5' ->
        arg5 1 2 3 4 5 6
        |> expect-to-equal 6
    test 'arg6' ->
        arg6 1 2 3 4 5 6 7
        |> expect-to-equal 7

describe 'drop, take' !->
    describe 'drop' !->
        test 1 ->
            [1 to 5]
            |> drop 3
            |> expect-to-equal [4 5]
        test 2 ->
            [1 to 5]
            |> drop 6
            |> expect-to-equal []
        test 3 ->
            []
            |> drop 3
            |> expect-to-equal []
    describe 'take' !->
        test 1 ->
            [1 to 5]
            |> take 3
            |> expect-to-equal [1 2 3]
        test 2 ->
            [1 to 5]
            |> take 6
            |> expect-to-equal [1 to 5]
        test 3 ->
            []
            |> take 3
            |> expect-to-equal []

describe 'deconstruct' !->
    o = some: 2 vals: 10
    describe 'deconstruct' !->
        test 1 ->
            o |> deconstruct ({ some, vals, }) ->
                [some, vals] |> expect-to-equal [2, 10]
    describe 'deconstruct2' !->
        test 1 ->
            o |> deconstruct2 ({ some, vals, }) -> (o) ->
                [some, vals] |> expect-to-equal [2, 10]
                [o.some, o.vals] |> expect-to-equal [2, 10]
        test 2 ->
            o
                |> deconstruct2 ({ some, vals, }) -> merge {
                    new: some + 2 extra: vals + 3
                }
                |> expect-to-equal do
                    some: 2 vals: 10 new: 4 extra: 13
    describe 'deconstructN' !->
        self =
            state:
                state1: 10
                state2: 20
            props:
                prop1: 15
                prop2: 25
        test 1 ->
            self
            |> deconstruct ({ props, state, }) ->
                [props, state]
                |> deconstruct-n do
                    ({ prop1, prop2, }, { state1, state2, }) ->
                        prop1 + prop2 + state1 + state2
                        |> expect-to-equal 70
