var ref$, list, test, xtest, expectToEqual, expectToBe, tryCatch, tryCatch__, exception, raise, die;
ref$ = require('./common'), list = ref$.list, test = ref$.test, xtest = ref$.xtest, expectToEqual = ref$.expectToEqual, expectToBe = ref$.expectToBe;
ref$ = require('../cjs/index'), tryCatch = ref$.tryCatch, tryCatch__ = ref$.tryCatch__, exception = ref$.exception, raise = ref$.raise, die = ref$.die;
describe('try/catch__', function(){
  var fails, passes, x$, howToFail;
  fails = function(){
    throw new Error;
  };
  passes = function(){
    return 99;
  };
  x$ = howToFail = jest.fn();
  x$.mockReturnValue('failed');
  xtest('should fail', function(){
    return expectToEqual('failed')(
    tryCatch__(fails, howToFail));
  });
  xtest('should succeed', function(){
    return expectToEqual(99)(
    tryCatch__(passes, howToFail));
  });
});
describe('try/catch', function(){
  var fails, passes, x$, howToPass, y$, howToFail, tryIt;
  fails = function(){
    throw new TypeError('a thing is not a thang');
  };
  passes = function(){
    return 99;
  };
  x$ = howToPass = jest.fn();
  x$.mockImplementation(function(x){
    return [x, x, x];
  });
  y$ = howToFail = jest.fn();
  y$.mockImplementation(function(e){
    return 'failed: ' + e.message;
  });
  tryIt = tryCatch(howToPass, howToFail);
  test('should fail', function(){
    return expectToEqual('failed: a thing is not a thang')(
    tryIt(
    fails));
  });
  test('should succeed, and pass params', function(){
    return expectToEqual([99, 99, 99])(
    tryIt(
    passes));
  });
});
describe('exceptions', function(){
  test('exception', function(){
    return expectToEqual(new Error('a b c'))(
    exception('a', 'b', 'c'));
  });
  test('raise', function(){
    return expect(function(){
      return raise(
      new Error('bad news'));
    }).toThrow('bad news');
  });
  test('die', function(){
    return expect(function(){
      return die('really', 'bad', 'news');
    }).toThrow('really bad news');
  });
});