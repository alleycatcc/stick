var ref$, id, rJoin, each, odd, even, list, test, xtest, expectToEqual, expectToBe, expectToThrow, ok, notOk, isTrue, isFalse, isYes, isNo, isTruthy, isFalsy, ifPredicate, whenPredicate, ifPredicateResults, whenPredicateResults, ifPredicateWithResults, whenPredicateWithResults, ifPredicateV, whenPredicateV, ifAlways, whenAlways, ifOk, whenOk, ifNotOk, whenNotOk, ifTrue, whenTrue, ifFalse, whenFalse, ifYes, whenYes, ifTruthy, whenTruthy, ifNo, whenNo, ifFalsy, whenFalsy, bindTryPropTo, bindTryProp, bindTryTo, bindTry, ifHas, whenHas, ifHasIn, whenHasIn, ifBind, whenBind, cond, condN, condS, guard, guardV, otherwise, againstAll, againstAny, allAgainst, anyAgainst, againstBoth, againstEither, doTests, doTestDoubleArm, doTestSingleArm;
ref$ = require('ramda'), id = ref$.identity, rJoin = ref$.join, each = ref$.forEach;
ref$ = require('prelude-ls'), odd = ref$.odd, even = ref$.even;
ref$ = require('./common'), list = ref$.list, test = ref$.test, xtest = ref$.xtest, expectToEqual = ref$.expectToEqual, expectToBe = ref$.expectToBe, expectToThrow = ref$.expectToThrow;
ref$ = require('../cjs/index'), ok = ref$.ok, notOk = ref$.notOk, isTrue = ref$.isTrue, isFalse = ref$.isFalse, isYes = ref$.isYes, isNo = ref$.isNo, isTruthy = ref$.isTruthy, isFalsy = ref$.isFalsy, ifPredicate = ref$.ifPredicate, whenPredicate = ref$.whenPredicate, ifPredicateResults = ref$.ifPredicateResults, whenPredicateResults = ref$.whenPredicateResults, ifPredicateWithResults = ref$.ifPredicateWithResults, whenPredicateWithResults = ref$.whenPredicateWithResults, ifPredicateV = ref$.ifPredicateV, whenPredicateV = ref$.whenPredicateV, ifAlways = ref$.ifAlways, whenAlways = ref$.whenAlways, ifOk = ref$.ifOk, whenOk = ref$.whenOk, ifNotOk = ref$.ifNotOk, whenNotOk = ref$.whenNotOk, ifTrue = ref$.ifTrue, whenTrue = ref$.whenTrue, ifFalse = ref$.ifFalse, whenFalse = ref$.whenFalse, ifYes = ref$.ifYes, whenYes = ref$.whenYes, ifTruthy = ref$.ifTruthy, whenTruthy = ref$.whenTruthy, ifNo = ref$.ifNo, whenNo = ref$.whenNo, ifFalsy = ref$.ifFalsy, whenFalsy = ref$.whenFalsy, bindTryPropTo = ref$.bindTryPropTo, bindTryProp = ref$.bindTryProp, bindTryTo = ref$.bindTryTo, bindTry = ref$.bindTry, ifHas = ref$.ifHas, whenHas = ref$.whenHas, ifHasIn = ref$.ifHasIn, whenHasIn = ref$.whenHasIn, ifBind = ref$.ifBind, whenBind = ref$.whenBind, cond = ref$.cond, condN = ref$.condN, condS = ref$.condS, guard = ref$.guard, guardV = ref$.guardV, otherwise = ref$.otherwise, againstAll = ref$.againstAll, againstAny = ref$.againstAny, allAgainst = ref$.allAgainst, anyAgainst = ref$.anyAgainst, againstBoth = ref$.againstBoth, againstEither = ref$.againstEither;
doTests = curry$(function(describeSpec, tests){
  return each(function(testSpec){
    var numArms, ref$, ref1$, theTest;
    numArms = (ref$ = (ref1$ = testSpec.numArms, delete testSpec.numArms, ref1$)) != null ? ref$ : 1;
    theTest = numArms === 2 ? doTestDoubleArm : doTestSingleArm;
    return theTest(describeSpec, testSpec);
  })(
  tests);
});
doTestDoubleArm = curry$(function(arg$, arg1$){
  var fn, is__, desc, inputVal, expectBranch;
  fn = arg$.fn, is__ = arg$.is__;
  desc = arg1$.desc, inputVal = arg1$.inputVal, expectBranch = arg1$.expectBranch;
  return test(desc, function(){
    var x$, ja, y$, nee, ret, ref$, expectedRet, expectedCallsJa, expectedCallsNee;
    x$ = ja = jest.fn();
    x$.mockImplementation(function(x){
      return [x, x, x];
    });
    y$ = nee = jest.fn();
    y$.mockImplementation(function(x){
      return [x, x, x, x];
    });
    ret = !is__
      ? fn(ja, nee)(
      inputVal)
      : fn(inputVal, ja, nee);
    ref$ = expectBranch === 'ja'
      ? [[inputVal, inputVal, inputVal], 1, 0]
      : [[inputVal, inputVal, inputVal, inputVal], 0, 1], expectedRet = ref$[0], expectedCallsJa = ref$[1], expectedCallsNee = ref$[2];
    expectToEqual(expectedCallsJa)(
    ja.mock.calls.length);
    expectToEqual(expectedCallsNee)(
    nee.mock.calls.length);
    return expectToEqual(expectedRet)(
    ret);
  });
});
doTestSingleArm = curry$(function(arg$, arg1$){
  var fn, is__, desc, inputVal, expectBranch;
  fn = arg$.fn, is__ = arg$.is__;
  desc = arg1$.desc, inputVal = arg1$.inputVal, expectBranch = arg1$.expectBranch;
  return test(desc, function(){
    var x$, ja, ret, ref$, expectedRet, expectedCallsJa;
    x$ = ja = jest.fn();
    x$.mockImplementation(function(x){
      return [x, x, x];
    });
    ret = !is__
      ? fn(ja)(
      inputVal)
      : fn(inputVal, ja);
    ref$ = expectBranch === 'ja'
      ? [[inputVal, inputVal, inputVal], 1]
      : [void 8, 0], expectedRet = ref$[0], expectedCallsJa = ref$[1];
    expectToEqual(expectedCallsJa)(
    ja.mock.calls.length);
    return expectToEqual(expectedRet)(
    ret);
  });
});
describe('whenPredicate', function(){
  var describeSpec, tests, describeSpec2, tests2;
  describeSpec = {
    fn: whenPredicate((function(it){
      return it > 3;
    })),
    is__: false
  };
  tests = list({
    desc: '4',
    inputVal: 4,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'null',
    inputVal: null,
    expectBranch: 'nee',
    numArms: 1
  });
  doTests(describeSpec, tests);
  describeSpec2 = {
    fn: whenPredicate(id),
    is__: false
  };
  tests2 = list({
    desc: 'exact truth, not truthy',
    inputVal: 3,
    expectBranch: 'ja',
    numArms: 1
  });
  doTests(describeSpec2, tests2);
  test('curried', function(){
    var x;
    x = whenPredicate((function(it){
      return it > 3;
    }), function(){
      return 10;
    });
    return expectToEqual(10)(
    x(
    5));
  });
  test('non-curried', function(){
    var x;
    x = whenPredicate((function(it){
      return it > 3;
    }), function(){
      return 10;
    }, 5);
    return expectToEqual(10)(
    x);
  });
});
describe('ifPredicate', function(){
  var describeSpec, tests, describeSpec2, tests2;
  describeSpec = {
    fn: ifPredicate((function(it){
      return it > 3;
    })),
    is__: false
  };
  tests = list({
    desc: '4',
    inputVal: 4,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'null',
    inputVal: null,
    expectBranch: 'nee',
    numArms: 2
  });
  doTests(describeSpec, tests);
  describeSpec2 = {
    fn: ifPredicate(id),
    is__: false
  };
  tests2 = list({
    desc: 'exact truth, not truthy',
    inputVal: 3,
    expectBranch: 'ja',
    numArms: 2
  });
  doTests(describeSpec2, tests2);
  test('anaphoric', function(){
    expectToEqual(4)(
    ifPredicate(odd, (function(it){
      return it + 1;
    }), (function(it){
      return it - 1;
    }))(
    3));
    expectToEqual(2)(
    ifPredicate(even, (function(it){
      return it + 1;
    }), (function(it){
      return it - 1;
    }))(
    3));
    expectToEqual(4)(
    whenPredicate(odd, (function(it){
      return it + 1;
    }))(
    3));
    return expectToEqual(void 8)(
    whenPredicate(even, (function(it){
      return it + 1;
    }))(
    3));
  });
});
describe('ifPredicate', function(){
  test('predicate tested for truthy, not strict truth', function(){
    var isOdd, ifOdd, iffer;
    isOdd = function(x){
      return x % 2;
    };
    ifOdd = ifPredicate(
    isOdd);
    iffer = ifOdd((function(it){
      return it * 2;
    }), function(){
      return 'no';
    });
    expectToEqual(6)(
    iffer(
    3));
    return expectToEqual('no')(
    iffer(
    4));
  });
});
describe('ifPredicateV, whenPredicateV', function(){
  var ifTrueV, whenTrueV;
  ifTrueV = ifPredicateV(
  (function(it){
    return it === true;
  }));
  whenTrueV = whenPredicateV(
  (function(it){
    return it === true;
  }));
  test(1, function(){
    expectToEqual(10)(
    ifPredicateV(odd, 10, 11)(
    3));
    return expectToEqual(10)(
    ifTrueV(10, 11)(
    3 === 3));
  });
  test(2, function(){
    expectToEqual(11)(
    ifPredicateV(even, 10, 11)(
    3));
    return expectToEqual(11)(
    ifTrueV(10, 11)(
    3 === 4));
  });
  test(3, function(){
    expectToEqual(10)(
    whenPredicateV(odd, 10)(
    3));
    return expectToEqual(10)(
    whenTrueV(10)(
    3 === 3));
  });
  test(4, function(){
    expectToEqual(void 8)(
    whenPredicateV(even, 10)(
    3));
    return expectToEqual(void 8)(
    whenTrueV(10)(
    3 === 4));
  });
  test('ifTrueV', function(){
    expectToEqual(10)(
    ifTrueV(10, 11)(
    true));
    return expectToEqual(11)(
    ifTrueV(10, 11)(
    false));
  });
  test('whenTrueV', function(){
    expectToEqual(10)(
    whenTrueV(10)(
    true));
    return expectToEqual(void 8)(
    whenTrueV(10)(
    false));
  });
});
describe('if/whenPredicateResults', function(){
  var pred, fYes, fNo, iffer, whenner;
  pred = function(n){
    if (odd(n)) {
      return ['eg', 'bert'];
    } else {
      return false;
    }
  };
  fYes = function(p){
    return rJoin('!')(
    p);
  };
  fNo = function(p){
    return p;
  };
  iffer = ifPredicateResults(pred, fYes, fNo);
  whenner = whenPredicateResults(pred, fYes);
  test(1, function(){
    return expectToEqual('eg!bert')(
    iffer(
    1));
  });
  test(2, function(){
    return expectToEqual(false)(
    iffer(
    2));
  });
  test(3, function(){
    return expectToEqual('eg!bert')(
    whenner(
    1));
  });
  test(4, function(){
    return expectToEqual(void 8)(
    whenner(
    2));
  });
  test(5, function(){
    return expectToEqual('eg!bert')(
    whenPredicateResults(pred, fYes, 1));
  });
  test(6, function(){
    return expectToEqual('eg!bert')(
    whenPredicateResults(pred)(fYes, 1));
  });
});
describe('if/whenPredicateWithResults', function(){
  var pred, fYes, fNo, iffer, whenner;
  pred = function(n){
    if (odd(n)) {
      return ['eg', 'bert'];
    } else {
      return false;
    }
  };
  fYes = function(x, p){
    return [
      x, rJoin('!')(
      p)
    ];
  };
  fNo = function(x, p){
    return [x, p];
  };
  iffer = ifPredicateWithResults(pred, fYes, fNo);
  whenner = whenPredicateWithResults(pred, fYes);
  test(1, function(){
    return expectToEqual([1, 'eg!bert'])(
    iffer(
    1));
  });
  test(2, function(){
    return expectToEqual([2, false])(
    iffer(
    2));
  });
  test(3, function(){
    return expectToEqual([1, 'eg!bert'])(
    whenner(
    1));
  });
  test(4, function(){
    return expectToEqual(void 8)(
    whenner(
    2));
  });
  test(5, function(){
    return expectToEqual([1, 'eg!bert'])(
    whenPredicateWithResults(pred, fYes, 1));
  });
  test(6, function(){
    return expectToEqual([1, 'eg!bert'])(
    whenPredicateWithResults(pred)(fYes, 1));
  });
});
describe('if/whenAlways', function(){
  describe('ifAlways', function(){
    var x, y, z;
    x = 3;
    y = 3;
    z = 4;
    test(1, function(){
      var iffer;
      iffer = ifAlways(x === y, function(){
        return 10;
      }, function(){
        return 11;
      });
      return expectToEqual(10)(
      iffer(
      3));
    });
    test(2, function(){
      var iffer;
      iffer = ifAlways(x !== y, function(){
        return 10;
      }, function(){
        return 11;
      });
      return expectToEqual(11)(
      iffer(
      3));
    });
    test('if anaphoric', function(){
      var ifferYes, ifferNo;
      ifferYes = ifAlways(x === y, (function(it){
        return it + 1;
      }), (function(it){
        return it + 2;
      }));
      ifferNo = ifAlways(x !== y, (function(it){
        return it + 1;
      }), (function(it){
        return it + 2;
      }));
      expectToEqual(4)(
      ifferYes(
      3));
      return expectToEqual(5)(
      ifferNo(
      3));
    });
    test('when anaphoric', function(){
      var whennerYes, whennerNo;
      whennerYes = whenAlways(x === y, (function(it){
        return it + 1;
      }));
      whennerNo = whenAlways(x !== y, (function(it){
        return it + 1;
      }));
      expectToEqual(4)(
      whennerYes(
      3));
      return expectToEqual(void 8)(
      whennerNo(
      3));
    });
    test(3, function(){
      var whenner;
      whenner = whenAlways(x === y, function(){
        return 10;
      });
      return expectToEqual(10)(
      whenner(
      3));
    });
    test(4, function(){
      var whenner;
      whenner = whenAlways(x !== y, function(){
        return 10;
      });
      return expectToEqual(void 8)(
      whenner(
      3));
    });
    test('if curried', function(){
      var ja, nee, a, e, b, c, d;
      ja = function(){
        return 10;
      };
      nee = function(){
        return 11;
      };
      a = ifAlways(true)(ja, nee);
      e = ifAlways(true)(ja)(nee);
      b = ifAlways(true, ja)(nee);
      c = ifAlways(true, ja, nee);
      d = ifAlways(true, ja, nee, 3);
      expectToEqual(10)(
      a(
      3));
      expectToEqual(10)(
      b(
      3));
      expectToEqual(10)(
      e(
      3));
      return expectToEqual(10)(
      d);
    });
    test('when curried', function(){
      var ja, a, b, c;
      ja = function(){
        return 10;
      };
      a = whenAlways(true)(ja);
      b = whenAlways(true, ja);
      c = whenAlways(true, ja, 3);
      expectToEqual(10)(
      a(
      3));
      expectToEqual(10)(
      b(
      3));
      return expectToEqual(10)(
      c);
    });
  });
});
describe('whenOk', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: whenOk,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'null',
    inputVal: null,
    expectBranch: 'nee',
    numArms: 1
  });
  doTests(describeSpec, tests);
});
describe('ifOk', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: ifOk,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'null',
    inputVal: null,
    expectBranch: 'nee',
    numArms: 2
  });
  doTests(describeSpec, tests);
});
describe('whenNotOk', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: whenNotOk,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: 'null',
    inputVal: null,
    expectBranch: 'ja',
    numArms: 1
  });
  doTests(describeSpec, tests);
});
describe('ifNotOk', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: ifNotOk,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: 'null',
    inputVal: null,
    expectBranch: 'ja',
    numArms: 2
  });
  doTests(describeSpec, tests);
});
describe('whenTrue', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: whenTrue,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'nee',
    numArms: 1
  });
  doTests(describeSpec, tests);
});
describe('ifTrue', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: ifTrue,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 2
  });
  doTests(describeSpec, tests);
});
describe('whenFalse', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: whenFalse,
    is__: false
  };
  tests = list({
    desc: 'false',
    inputVal: false,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: 'true',
    inputVal: true,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'nee',
    numArms: 1
  });
  doTests(describeSpec, tests);
});
describe('ifFalse', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: ifFalse,
    is__: false
  };
  tests = list({
    desc: 'false',
    inputVal: false,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: 'true',
    inputVal: true,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 2
  });
  doTests(describeSpec, tests);
});
describe('whenYes', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: whenYes,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'nee',
    numArms: 1
  });
  doTests(describeSpec, tests);
  test('alias whenTruthy', function(){
    return expectToEqual(whenTruthy)(
    whenYes);
  });
});
describe('ifYes', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: ifYes,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'nee',
    numArms: 2
  });
  doTests(describeSpec, tests);
  test('alias ifTruthy', function(){
    return expectToEqual(ifTruthy)(
    ifYes);
  });
});
describe('whenNo', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: whenNo,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'nee',
    numArms: 1
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'ja',
    numArms: 1
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'ja',
    numArms: 1
  });
  doTests(describeSpec, tests);
  test('alias whenFalsy', function(){
    return expectToEqual(whenFalsy)(
    whenNo);
  });
});
describe('ifNo', function(){
  var describeSpec, tests;
  describeSpec = {
    fn: ifNo,
    is__: false
  };
  tests = list({
    desc: 'true',
    inputVal: true,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'false',
    inputVal: false,
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: 'empty string',
    inputVal: '',
    expectBranch: 'ja',
    numArms: 2
  }, {
    desc: '3',
    inputVal: 3,
    expectBranch: 'nee',
    numArms: 2
  }, {
    desc: 'undefined',
    inputVal: void 8,
    expectBranch: 'ja',
    numArms: 2
  });
  doTests(describeSpec, tests);
  test('alias ifFalsy', function(){
    return expectToEqual(ifFalsy)(
    ifNo);
  });
});
describe('cond', function(){
  describe('cond', function(){
    describe('raw', function(){
      test('truthy', function(){
        return expectToEqual('ok')(
        cond([
          function(){
            return 3 === 4;
          }, function(){
            return 'twilight zone';
          }
        ], [
          function(){
            return 3 === 5;
          }, function(){
            return 'even stranger';
          }
        ], [
          function(){
            return 'ok';
          }, function(str){
            return str;
          }
        ], [
          null, function(){
            return 'error';
          }
        ]));
      });
      test('fallthrough', function(){
        return expectToEqual(void 8)(
        cond([
          function(){
            return 3 === 4;
          }, function(){
            return 'twilight zone';
          }
        ], [
          function(){
            return 3 === 5;
          }, function(){
            return 'even stranger';
          }
        ]));
      });
      test('null test should throw', function(){
        return expectToThrow(
        function(){
          return cond([
            function(){
              return 3 === 4;
            }, function(){
              return 'twilight zone';
            }
          ], [
            function(){
              return 3 === 5;
            }, function(){
              return 'even stranger';
            }
          ], [null, function(){}]);
        });
      });
    });
    describe('idiomatic', function(){
      test('truthy', function(){
        return expectToEqual('ok')(
        cond(guard(function(){
          return 'twilight zone';
        })(
        function(){
          return 3 === 4;
        }), guard(function(){
          return 'even stranger';
        })(
        function(){
          return 3 === 5;
        }), guard(function(str){
          return str;
        })(
        function(){
          return 'ok';
        }), guard(function(){
          return 'error';
        })(
        otherwise)));
      });
      test('fallthrough', function(){
        return expectToEqual(void 8)(
        cond(guard(function(){
          return 'twilight zone';
        })(
        function(){
          return 3 === 4;
        }), guard(function(){
          return 'even stranger';
        })(
        function(){
          return 3 === 5;
        })));
      });
      test('otherwise', function(){
        return expectToEqual('ok')(
        cond(guard(function(){
          return 'twilight zone';
        })(
        function(){
          return 3 === 4;
        }), guard(function(){
          return 'even stranger';
        })(
        function(){
          return 3 === 5;
        }), guard(function(){
          return 'ok';
        })(
        otherwise)));
      });
    });
  });
  describe('condN', function(){
    describe('raw', function(){
      test('truthy', function(){
        return expectToEqual('ok')(
        condN([
          [
            function(){
              return 3 === 4;
            }, function(){
              return 'twilight zone';
            }
          ], [
            function(){
              return 3 === 5;
            }, function(){
              return 'even stranger';
            }
          ], [
            function(){
              return 'ok';
            }, function(str){
              return str;
            }
          ], [
            null, function(){
              return 'error';
            }
          ]
        ]));
      });
      test('fallthrough', function(){
        return expectToEqual(void 8)(
        condN([
          [
            function(){
              return 3 === 4;
            }, function(){
              return 'twilight zone';
            }
          ], [
            function(){
              return 3 === 5;
            }, function(){
              return 'even stranger';
            }
          ]
        ]));
      });
      test('null test should throw', function(){
        return expectToThrow(
        function(){
          return condN([
            [
              function(){
                return 3 === 4;
              }, function(){
                return 'twilight zone';
              }
            ], [
              function(){
                return 3 === 5;
              }, function(){
                return 'even stranger';
              }
            ], [null, function(){}]
          ]);
        });
      });
    });
    describe('idiomatic', function(){
      test('truthy', function(){
        return expectToEqual('ok')(
        condN([
          guard(function(){
            return 'twilight zone';
          })(
          function(){
            return 3 === 4;
          }), guard(function(){
            return 'even stranger';
          })(
          function(){
            return 3 === 5;
          }), guard(function(str){
            return str;
          })(
          function(){
            return 'ok';
          }), guard(function(){
            return 'error';
          })(
          otherwise)
        ]));
      });
      test('fallthrough', function(){
        return expectToEqual(void 8)(
        condN([
          guard(function(){
            return 'twilight zone';
          })(
          function(){
            return 3 === 4;
          }), guard(function(){
            return 'even stranger';
          })(
          function(){
            return 3 === 5;
          })
        ]));
      });
      test('otherwise', function(){
        return expectToEqual('ok')(
        condN([
          guard(function(){
            return 'twilight zone';
          })(
          function(){
            return 3 === 4;
          }), guard(function(){
            return 'even stranger';
          })(
          function(){
            return 3 === 5;
          }), guard(function(){
            return 'ok';
          })(
          otherwise)
        ]));
      });
    });
  });
  describe('condS', function(){
    describe('raw', function(){
      test('truthy', function(){
        return expectToEqual(5)(
        condS([
          [
            (function(it){
              return it === 5;
            }), function(){
              return 'twilight zone';
            }
          ], [
            (function(it){
              return it === 4;
            }), function(){
              return 'even stranger';
            }
          ], [
            odd, (function(it){
              return it + 2;
            })
          ], [
            null, function(){
              return 'error';
            }
          ]
        ])(
        3));
      });
      test('fallthrough', function(){
        return expectToEqual(void 8)(
        condS([
          [
            (function(it){
              return it === 5;
            }), function(){
              return 'twilight zone';
            }
          ], [
            (function(it){
              return it === 4;
            }), function(){
              return 'even stranger';
            }
          ]
        ])(
        3));
      });
      test('null test should throw', function(){
        return expectToThrow(
        function(){
          return condS([
            [
              function(){
                return 3 === 4;
              }, function(){
                return 'twilight zone';
              }
            ], [
              function(){
                return 3 === 5;
              }, function(){
                return 'even stranger';
              }
            ], [
              null, (function(it){
                return it + 5;
              })
            ]
          ])(
          3);
        });
      });
    });
    describe('idiomatic', function(){
      test('truthy', function(){
        return expectToEqual(12)(
        condS([
          guard(function(){
            return 'twilight zone';
          })(
          (function(it){
            return it === 4;
          })), guard(function(){
            return 'even stranger';
          })(
          (function(it){
            return it === 5;
          })), guard((function(it){
            return it + 9;
          }))(
          odd), guard(function(){
            return 'error';
          })(
          otherwise)
        ])(
        3));
      });
      test('fallthrough', function(){
        return expectToEqual(void 8)(
        condS([
          guard(function(){
            return 'twilight zone';
          })(
          (function(it){
            return it === 4;
          })), guard(function(){
            return 'even stranger';
          })(
          (function(it){
            return it === 5;
          })), guard((function(it){
            return it + 9;
          }))(
          even)
        ])(
        3));
      });
      test('otherwise', function(){
        return expectToEqual('ok')(
        condS([
          guard(function(){
            return 'twilight zone';
          })(
          (function(it){
            return it === 4;
          })), guard(function(){
            return 'even stranger';
          })(
          (function(it){
            return it === 5;
          })), guard(function(){
            return 'ok';
          })(
          otherwise)
        ])(
        3));
      });
    });
  });
  describe('misc', function(){
    test('guardV', function(){
      expectToEqual('big')(
      condS([
        guardV('big')(
        (function(it){
          return it > 30;
        })), guardV('medium')(
        (function(it){
          return it > 20;
        }))
      ])(
      32));
      return expectToEqual('medium')(
      cond(guardV('big')(
      function(){
        return 21 > 30;
      }), guardV('medium')(
      function(){
        return 21 > 20;
      })));
    });
  });
  test(1, function(){
    return expectToEqual('feet')(
    condS([
      [
        function(str){
          return deepEq$(str, 'lions', '===');
        }, function(){
          return 'feet';
        }
      ], [
        function(str){
          return deepEq$(str, 'tigers', '===');
        }, function(){
          return 'heads';
        }
      ], [
        void 8, function(target){
          return 'no match on ' + target;
        }
      ]
    ])(
    'lions'));
  });
  test(2, function(){
    return expectToEqual('heads')(
    condS([
      [
        function(str){
          return deepEq$(str, 'lions', '===');
        }, function(){
          return 'feet';
        }
      ], [
        function(str){
          return deepEq$(str, 'tigers', '===');
        }, function(){
          return 'heads';
        }
      ], [
        void 8, function(target){
          return 'no match on ' + target;
        }
      ]
    ])(
    'tigers'));
  });
  test('lazy, in order', function(){
    var mock;
    mock = jest.fn();
    condS([
      [
        function(str){
          return deepEq$(str, 'lions', '===');
        }, function(){
          return 'feet';
        }
      ], [
        function(str){
          return deepEq$(str, 'tigers', '===');
        }, mock
      ], [
        void 8, function(target){
          return 'no match on ' + target;
        }
      ]
    ])(
    'lions');
    return expectToEqual(0)(
    mock.mock.calls.length);
  });
});
describe('is/isNot', function(){
  test('ok', function(){
    expectToEqual(true)(
    ok(
    true));
    expectToEqual(true)(
    ok(
    false));
    expectToEqual(false)(
    ok(
    void 8));
    return expectToEqual(false)(
    ok(
    null));
  });
  test('notOk', function(){
    expectToEqual(false)(
    notOk(
    true));
    expectToEqual(false)(
    notOk(
    false));
    expectToEqual(true)(
    notOk(
    void 8));
    return expectToEqual(true)(
    notOk(
    null));
  });
  test('isTrue', function(){
    expectToEqual(true)(
    isTrue(
    true));
    expectToEqual(false)(
    isTrue(
    1));
    expectToEqual(false)(
    isTrue(
    0));
    expectToEqual(false)(
    isTrue(
    '1'));
    expectToEqual(false)(
    isTrue(
    false));
    expectToEqual(false)(
    isTrue(
    void 8));
    return expectToEqual(false)(
    isTrue(
    null));
  });
  test('isFalse', function(){
    expectToEqual(true)(
    isFalse(
    false));
    expectToEqual(false)(
    isFalse(
    true));
    expectToEqual(false)(
    isFalse(
    0));
    expectToEqual(false)(
    isFalse(
    1));
    expectToEqual(false)(
    isFalse(
    '1'));
    expectToEqual(false)(
    isFalse(
    void 8));
    return expectToEqual(false)(
    isFalse(
    null));
  });
  test('isYes', function(){
    expectToEqual(true)(
    isYes(
    true));
    expectToEqual(true)(
    isYes(
    1));
    expectToEqual(true)(
    isYes(
    '1'));
    expectToEqual(true)(
    isYes(
    '2'));
    expectToEqual(true)(
    isYes(
    '0'));
    expectToEqual(false)(
    isYes(
    ''));
    expectToEqual(false)(
    isYes(
    0));
    expectToEqual(false)(
    isYes(
    false));
    expectToEqual(false)(
    isYes(
    void 8));
    return expectToEqual(false)(
    isYes(
    null));
  });
  test('isNo', function(){
    expectToEqual(false)(
    isNo(
    true));
    expectToEqual(false)(
    isNo(
    1));
    expectToEqual(false)(
    isNo(
    '1'));
    expectToEqual(false)(
    isNo(
    '2'));
    expectToEqual(false)(
    isNo(
    '0'));
    expectToEqual(true)(
    isNo(
    ''));
    expectToEqual(true)(
    isNo(
    0));
    expectToEqual(true)(
    isNo(
    false));
    expectToEqual(true)(
    isNo(
    void 8));
    return expectToEqual(true)(
    isNo(
    null));
  });
  test('aliases', function(){
    expectToEqual(isFalsy)(
    isNo);
    return expectToEqual(isTruthy)(
    isYes);
  });
});
describe('if/when has/hasIn', function(){
  var base, extended, ref$;
  base = {
    water: 'wet',
    nothing: void 8,
    me: 'ik'
  };
  extended = (ref$ = Object.create(base), ref$.baby = 'feet', ref$);
  describe('if-has', function(){
    test('main', function(){
      return expectToEqual('wetwaterik')(
      ifHas(function(v, o, k){
        return v + k + o.me;
      }, function(){
        return 42;
      })(
      [base, 'water']));
    });
    test('has undefined (should be true)', function(){
      return expectToEqual(41)(
      ifHas(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [base, 'nothing']));
    });
    test('nonexistent', function(){
      return expectToEqual(42)(
      ifHas(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [base, 'nada']));
    });
    test('extended', function(){
      return expectToEqual(42)(
      ifHas(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [extended, 'water']));
    });
    test('extended', function(){
      return expectToEqual(41)(
      ifHas(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [extended, 'baby']));
    });
  });
  describe('when-has', function(){
    test('main', function(){
      return expectToEqual('wetwaterik')(
      whenHas(function(v, o, k){
        return v + k + o.me;
      })(
      [base, 'water']));
    });
    test('has undefined (should be true)', function(){
      return expectToEqual(41)(
      whenHas(function(v, o, k){
        return 41;
      })(
      [base, 'nothing']));
    });
    test('nonexistent', function(){
      return expectToEqual(void 8)(
      whenHas(function(v, o, k){
        return 41;
      })(
      [base, 'nada']));
    });
    test('extended', function(){
      return expectToEqual(void 8)(
      whenHas(function(v, o, k){
        return 41;
      })(
      [extended, 'water']));
    });
    test('extended', function(){
      return expectToEqual(41)(
      whenHas(function(v, o, k){
        return 41;
      })(
      [extended, 'baby']));
    });
  });
  describe('if-has-in', function(){
    test('main', function(){
      return expectToEqual('wetwaterik')(
      ifHasIn(function(v, o, k){
        return v + k + o.me;
      }, function(){
        return 42;
      })(
      [base, 'water']));
    });
    test('has-in undefined (should be true)', function(){
      return expectToEqual(41)(
      ifHasIn(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [base, 'nothing']));
    });
    test('nonexistent', function(){
      return expectToEqual(42)(
      ifHasIn(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [base, 'nada']));
    });
    test('extended', function(){
      return expectToEqual(41)(
      ifHasIn(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [extended, 'water']));
    });
    test('extended', function(){
      return expectToEqual(41)(
      ifHasIn(function(v, o, k){
        return 41;
      }, function(){
        return 42;
      })(
      [extended, 'baby']));
    });
  });
  describe('when-has-in', function(){
    test('main', function(){
      return expectToEqual('wetwaterik')(
      whenHasIn(function(v, o, k){
        return v + k + o.me;
      })(
      [base, 'water']));
    });
    test('has-in undefined (should be true)', function(){
      return expectToEqual(41)(
      whenHasIn(function(v, o, k){
        return 41;
      })(
      [base, 'nothing']));
    });
    test('nonexistent', function(){
      return expectToEqual(void 8)(
      whenHasIn(function(v, o, k){
        return 41;
      })(
      [base, 'nada']));
    });
    test('extended', function(){
      return expectToEqual(41)(
      whenHasIn(function(v, o, k){
        return 41;
      })(
      [extended, 'water']));
    });
    test('extended', function(){
      return expectToEqual(41)(
      whenHasIn(function(v, o, k){
        return 41;
      })(
      [extended, 'baby']));
    });
  });
});
describe('ifBind, whenBind', function(){
  var base, extended, ref$;
  base = {
    water: 'wet',
    nothing: void 8,
    me: 'ik',
    douse: function(level){
      return this.water + ("ness level " + level);
    }
  };
  extended = (ref$ = Object.create(base), ref$.baby = 'feet', ref$);
  describe('if bind try prop to', function(){
    describe('ifBind', function(){
      var ifBindF;
      ifBindF = ifBind(bindTryPropTo);
      test('base', function(){
        return expectToEqual('wetness level 10')(
        ifBindF(function(_, bound){
          return bound(10);
        }, function(){
          return 42;
        })(
        [base, 'douse']));
      });
      test('extended', function(){
        return expectToEqual('wetness level 10')(
        ifBindF(function(_, bound){
          return bound(10);
        }, function(){
          return 42;
        })(
        [extended, 'douse']));
      });
      test('no', function(){
        return expectToEqual(42)(
        ifBindF(function(){
          return 41;
        }, function(){
          return 42;
        })(
        [base, 'nothing']));
      });
    });
    describe('whenBind', function(){
      var whenBindF;
      whenBindF = whenBind(bindTryPropTo);
      test('base', function(){
        return expectToEqual('wetness level 10')(
        whenBindF(function(_, bound){
          return bound(10);
        })(
        [base, 'douse']));
      });
      test('extended', function(){
        return expectToEqual('wetness level 10')(
        whenBindF(function(_, bound){
          return bound(10);
        })(
        [extended, 'douse']));
      });
      test('no', function(){
        return expectToEqual(void 8)(
        whenBindF(function(){
          return 41;
        })(
        [base, 'nothing']));
      });
    });
  });
});
describe('predicate lists', function(){
  var ps1, ps2, xs;
  ps1 = [
    odd, (function(it){
      return it > 3;
    }), (function(it){
      return it < 10;
    })
  ];
  ps2 = [
    odd, (function(it){
      return it > 3;
    })
  ];
  xs = [1, 2, 3, 4, 5];
  describe('againstAll', function(){
    test(1, function(){
      expectToEqual(true)(
      againstAll(ps1)(
      7));
      expectToEqual(false)(
      againstAll(ps1)(
      8));
      expectToEqual(true)(
      againstAll(ps1)(
      9));
      return expectToEqual(false)(
      againstAll(ps1)(
      11));
    });
  });
  describe('againstAny', function(){
    test(1, function(){
      expectToEqual(true)(
      againstAny(ps2)(
      7));
      expectToEqual(true)(
      againstAny(ps2)(
      8));
      expectToEqual(true)(
      againstAny(ps2)(
      9));
      expectToEqual(true)(
      againstAny(ps2)(
      1));
      return expectToEqual(false)(
      againstAny(ps2)(
      2));
    });
  });
  describe('allAgainst', function(){
    test(1, function(){
      expectToEqual(false)(
      allAgainst(odd)(
      xs));
      return expectToEqual(true)(
      allAgainst((function(it){
        return it < 6;
      }))(
      xs));
    });
  });
  describe('anyAgainst', function(){
    test(1, function(){
      expectToEqual(true)(
      anyAgainst(odd)(
      xs));
      return expectToEqual(false)(
      anyAgainst((function(it){
        return it > 5;
      }))(
      xs));
    });
  });
  describe('againstBoth', function(){
    test(1, function(){
      expectToEqual(true)(
      againstBoth(odd, (function(it){
        return it < 5;
      }))(
      3));
      return expectToEqual(false)(
      againstBoth(odd, (function(it){
        return it < 5;
      }))(
      7));
    });
  });
  describe('againstEither', function(){
    test(1, function(){
      expectToEqual(true)(
      againstEither(odd, (function(it){
        return it < 5;
      }))(
      3));
      expectToEqual(true)(
      againstEither(odd, (function(it){
        return it < 5;
      }))(
      7));
      return expectToEqual(false)(
      againstEither(odd, (function(it){
        return it < 5;
      }))(
      8));
    });
  });
});
function curry$(f, bound){
  var context,
  _curry = function(args) {
    return f.length > 1 ? function(){
      var params = args ? args.concat() : [];
      context = bound ? context || this : this;
      return params.push.apply(params, arguments) <
          f.length && arguments.length ?
        _curry.call(context, params) : f.apply(context, params);
    } : f;
  };
  return _curry();
}
function deepEq$(x, y, type){
  var toString = {}.toString, hasOwnProperty = {}.hasOwnProperty,
      has = function (obj, key) { return hasOwnProperty.call(obj, key); };
  var first = true;
  return eq(x, y, []);
  function eq(a, b, stack) {
    var className, length, size, result, alength, blength, r, key, ref, sizeB;
    if (a == null || b == null) { return a === b; }
    if (a.__placeholder__ || b.__placeholder__) { return true; }
    if (a === b) { return a !== 0 || 1 / a == 1 / b; }
    className = toString.call(a);
    if (toString.call(b) != className) { return false; }
    switch (className) {
      case '[object String]': return a == String(b);
      case '[object Number]':
        return a != +a ? b != +b : (a == 0 ? 1 / a == 1 / b : a == +b);
      case '[object Date]':
      case '[object Boolean]':
        return +a == +b;
      case '[object RegExp]':
        return a.source == b.source &&
               a.global == b.global &&
               a.multiline == b.multiline &&
               a.ignoreCase == b.ignoreCase;
    }
    if (typeof a != 'object' || typeof b != 'object') { return false; }
    length = stack.length;
    while (length--) { if (stack[length] == a) { return true; } }
    stack.push(a);
    size = 0;
    result = true;
    if (className == '[object Array]') {
      alength = a.length;
      blength = b.length;
      if (first) {
        switch (type) {
        case '===': result = alength === blength; break;
        case '<==': result = alength <= blength; break;
        case '<<=': result = alength < blength; break;
        }
        size = alength;
        first = false;
      } else {
        result = alength === blength;
        size = alength;
      }
      if (result) {
        while (size--) {
          if (!(result = size in a == size in b && eq(a[size], b[size], stack))){ break; }
        }
      }
    } else {
      if ('constructor' in a != 'constructor' in b || a.constructor != b.constructor) {
        return false;
      }
      for (key in a) {
        if (has(a, key)) {
          size++;
          if (!(result = has(b, key) && eq(a[key], b[key], stack))) { break; }
        }
      }
      if (result) {
        sizeB = 0;
        for (key in b) {
          if (has(b, key)) { ++sizeB; }
        }
        if (first) {
          if (type === '<<=') {
            result = size < sizeB;
          } else if (type === '<==') {
            result = size <= sizeB
          } else {
            result = size === sizeB;
          }
        } else {
          first = false;
          result = size === sizeB;
        }
      }
    }
    stack.pop();
    return result;
  }
}