"use strict";var _interopRequireDefault=require("@babel/runtime/helpers/interopRequireDefault");Object.defineProperty(exports,"__esModule",{value:true});exports.assocPathM=exports.assocPath=exports.assocM=exports.assoc=exports.pathOf=exports.path=exports.propOf=exports.prop=exports.defaultTo=exports.die=exports.raise=exports.exception=exports.tryCatch=exports.toThe=exports.moduloWholePart=exports.modulo=exports.subtractFrom=exports.subtract=exports.divideInto=exports.divideBy=exports.multiply=exports.add=exports.condS=exports.cond=exports.condPredicate=exports.F=exports.T=exports.whenBind=exports.ifBind=exports.bindTry=exports.bindTryTo=exports.bindTryProp=exports.bindTryPropTo=exports.bind=exports.bindTo=exports.bindProp=exports.bindPropTo=exports.bindLateProp=exports.bindLatePropTo=exports.whenHasIn=exports.ifHasIn=exports.whenHas=exports.ifHas=exports.hasIn=exports.has=exports.sideN=exports.side5=exports.side4=exports.side3=exports.side2=exports.side1=exports.side=exports.dotN=exports.dot5=exports.dot4=exports.dot3=exports.dot2=exports.dot1=exports.dot=exports.tap=exports.isSymbol=exports.isString=exports.isBoolean=exports.isRegExp=exports.isNumber=exports.isObject=exports.isArray=exports.isFunction=exports.isType=exports.getType=exports.isNo=exports.isYes=exports.whenAlways=exports.ifAlways=exports.whenPredicateWithResults=exports.ifPredicateWithResults=exports.whenPredicateResults=exports.ifPredicateResults=exports.whenPredicateV=exports.ifPredicateV=exports.whenPredicate=exports.ifPredicate=exports.isFalse=exports.isTrue=exports.lte=exports.lt=exports.gte=exports.gt=exports.ne=exports.eq=exports.always=exports.notOk=exports.ok=exports.not=exports.noop=exports.composeAsMethods=exports.composeAsMethodsRight=exports.compose=exports.composeRight=exports.pipe=void 0;exports.spreadTo=exports.passToN=exports.passTo=exports.applyToN=exports.applyTo5=exports.applyTo4=exports.applyTo3=exports.applyTo2=exports.applyTo1=exports.invoke=exports.provideToN=exports.provideTo5=exports.provideTo4=exports.provideTo3=exports.provideTo2=exports.provideTo1=exports.provideTo=exports.callOnN=exports.callOn5=exports.callOn4=exports.callOn3=exports.callOn2=exports.callOn1=exports.callOn=exports.letS=exports.lets=exports.letN=exports.lets6=exports.lets5=exports.lets4=exports.lets3=exports.lets2=exports.lets1=exports.letV=exports.letNV=exports.asterisk5=exports.asterisk4=exports.asterisk3=exports.asterisk2=exports.asterisk1=exports.asteriskN=exports.ampersandN=exports.reduceObjIn=exports.reduceObj=exports.addCollection2=exports.addIndex2=exports.addCollection=exports.addIndex=exports.eachObjIn=exports.eachObj=exports.take=exports.drop=exports.reduceAbort=exports.contains=exports.containsV=exports.reject=exports.filter=exports.findWithIndex=exports.findIndex=exports.find=exports.reduceRightC=exports.reduceRight=exports.reduce=exports.each=exports.map=exports.mergeWhen=exports.mergeWith=exports.mergeAllIn=exports.mergeInSym=exports.mergeInMSym=exports.mergeInToSym=exports.mergeInToMSym=exports.mergeSym=exports.mergeMSym=exports.mergeToSym=exports.mergeToMSym=exports.mergeIn=exports.mergeInTo=exports.mergeInM=exports.mergeInToM=exports.merge=exports.mergeTo=exports.mergeM=exports.mergeToM=exports.concatM=exports.concatToM=exports.concat=exports.concatTo=exports.prependToM=exports.prependM=exports.prepend=exports.prependTo=exports.appendM=exports.appendToM=exports.appendTo=exports.append=exports.updatePath=exports.updatePathM=exports.update=exports.updateM=void 0;exports.orNot=exports.andNot=exports.or=exports.and=exports.deconstructN=exports.deconstruct2=exports.deconstruct=exports.againstEither=exports.againstBoth=exports.anyAgainst=exports.allAgainst=exports.againstAny=exports.againstAll=exports.mixinNM=exports.mixinPreNM=exports.mixinM=exports.mixinPreM=exports.factoryInit=exports.factoryProps=exports.ifXReplaceStrFlags=exports.ifXReplaceStr=exports.ifXReplace=exports.xReplaceStrFlags=exports.xReplaceStr=exports.xReplace=exports.xMatchStrFlags=exports.xMatchStr=exports.xMatch=exports.xMatchGlobal=exports.match=exports.neuN=exports.neu5=exports.neu4=exports.neu3=exports.neu2=exports.neu1=exports.xRegExpStr=exports.xRegExpFlags=exports.xRegExp=exports.rangeToBy=exports.rangeFromByDesc=exports.rangeFromByAsc=exports.rangeFromBy=exports.ifReplace=exports.timesSide=exports.timesF=exports.timesV=exports.repeatSide=exports.repeatF=exports.repeatV=exports.sprintfN=exports.sprintf1=exports.flip5=exports.flip4=exports.flip3=exports.flip=exports.split=exports.join=void 0;var _construct2=_interopRequireDefault(require("@babel/runtime/helpers/construct"));var _defineProperty2=_interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));var _slicedToArray2=_interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));var _toConsumableArray2=_interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));var _sprintfJs=_interopRequireDefault(require("sprintf-js"));var _internal=require("./internal.js");function ownKeys(object,enumerableOnly){var keys=Object.keys(object);if(Object.getOwnPropertySymbols){var symbols=Object.getOwnPropertySymbols(object);enumerableOnly&&(symbols=symbols.filter(function(sym){return Object.getOwnPropertyDescriptor(object,sym).enumerable;})),keys.push.apply(keys,symbols);}return keys;}function _objectSpread(target){for(var i=1;i<arguments.length;i++){var source=null!=arguments[i]?arguments[i]:{};i%2?ownKeys(Object(source),!0).forEach(function(key){(0,_defineProperty2.default)(target,key,source[key]);}):Object.getOwnPropertyDescriptors?Object.defineProperties(target,Object.getOwnPropertyDescriptors(source)):ownKeys(Object(source)).forEach(function(key){Object.defineProperty(target,key,Object.getOwnPropertyDescriptor(source,key));});}return target;}function _createForOfIteratorHelper(o,allowArrayLike){var it=typeof Symbol!=="undefined"&&o[Symbol.iterator]||o["@@iterator"];if(!it){if(Array.isArray(o)||(it=_unsupportedIterableToArray(o))||allowArrayLike&&o&&typeof o.length==="number"){if(it)o=it;var i=0;var F=function F(){};return{s:F,n:function n(){if(i>=o.length)return{done:true};return{done:false,value:o[i++]};},e:function e(_e){throw _e;},f:F};}throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}var normalCompletion=true,didErr=false,err;return{s:function s(){it=it.call(o);},n:function n(){var step=it.next();normalCompletion=step.done;return step;},e:function e(_e2){didErr=true;err=_e2;},f:function f(){try{if(!normalCompletion&&it.return!=null)it.return();}finally{if(didErr)throw err;}}};}function _unsupportedIterableToArray(o,minLen){if(!o)return;if(typeof o==="string")return _arrayLikeToArray(o,minLen);var n=Object.prototype.toString.call(o).slice(8,-1);if(n==="Object"&&o.constructor)n=o.constructor.name;if(n==="Map"||n==="Set")return Array.from(o);if(n==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))return _arrayLikeToArray(o,minLen);}function _arrayLikeToArray(arr,len){if(len==null||len>arr.length)len=arr.length;for(var i=0,arr2=new Array(len);i<len;i++){arr2[i]=arr[i];}return arr2;}var sprintf=_sprintfJs.default.sprintf;// --- @future curry these?
var pipe=function pipe(a,b){return b(a);};exports.pipe=pipe;var composeRight=function composeRight(a,b){return function(){return b(a.apply(void 0,arguments));};};exports.composeRight=composeRight;var compose=function compose(a,b){return function(){return a(b.apply(void 0,arguments));};};// --- @experimental
exports.compose=compose;var composeAsMethodsRight=function composeAsMethodsRight(b,a){return a.compose(b);};exports.composeAsMethodsRight=composeAsMethodsRight;var composeAsMethods=function composeAsMethods(a,b){return a.compose(b);};exports.composeAsMethods=composeAsMethods;var noop=function noop(){};exports.noop=noop;var not=function not(x){return!x;};exports.not=not;var ok=function ok(x){return x!=null;};exports.ok=ok;var notOk=function notOk(x){return x==null;};exports.notOk=notOk;var always=function always(x){return function(_){return x;};};exports.always=always;var eq=function eq(x){return function(y){return x===y;};};exports.eq=eq;var ne=function ne(x){return function(y){return x!==y;};};exports.ne=ne;var gt=function gt(m){return function(n){return n>m;};};exports.gt=gt;var gte=function gte(m){return function(n){return n>=m;};};exports.gte=gte;var lt=function lt(m){return function(n){return n<m;};};exports.lt=lt;var lte=function lte(m){return function(n){return n<=m;};};/* Matches on exactly `true` or `false`
 */exports.lte=lte;var isTrue=eq(true);exports.isTrue=isTrue;var isFalse=eq(false);exports.isFalse=isFalse;var ifPredicate=function ifPredicate(p){return function(yes){return function(no){return function(x){return p(x)?yes(x):no(x);};};};};exports.ifPredicate=ifPredicate;var whenPredicate=function whenPredicate(p){return function(yes){return ifPredicate(p)(yes)(noop);};};exports.whenPredicate=whenPredicate;var ifPredicateV=function ifPredicateV(p){return function(yes){return function(no){return function(x){return p(x)?yes:no;};};};};exports.ifPredicateV=ifPredicateV;var whenPredicateV=function whenPredicateV(p){return function(yes){return ifPredicateV(p)(yes)(void 8);};};/* These are like `ifPredicate`/`whenPredicate`, but pass the result of the predicate test to the
 * condition functions.
 */exports.whenPredicateV=whenPredicateV;var ifPredicateResults=function ifPredicateResults(p){return function(yes){return function(no){return function(x){var y=p(x);return y?yes(y):no(y);};};};};exports.ifPredicateResults=ifPredicateResults;var whenPredicateResults=function whenPredicateResults(p){return function(yes){return ifPredicateResults(p)(yes)(noop);};};/* These are like `ifPredicate`/`whenPredicate`, but pass both `x` and the result of the predicate
 * test to the condition functions.
 */exports.whenPredicateResults=whenPredicateResults;var ifPredicateWithResults=function ifPredicateWithResults(p){return function(yes){return function(no){return function(x){var y=p(x);return y?yes(x,y):no(x,y);};};};};exports.ifPredicateWithResults=ifPredicateWithResults;var whenPredicateWithResults=function whenPredicateWithResults(p){return function(yes){return ifPredicateWithResults(p)(yes)(noop);};};/*
 * An anaphoric if which branches on a constant condition. The piped argument is ignored when
 * deciding the condition, but is passed to the branching functions.
 *
 * Usage:
 * res | ifAlways (someConstantCondition) (
   res => yes ...
   res => no ...
  )
 *
 * This is less lazy in its evaluation of the condition than the normal if/whenPredicate functions. To retain the laziness, use if/whenPredicate, and ignore the argument:
 *
 * res | ifPredicate (() => someConstantCondition) (...)
 *
 * Derivation:
 *   const isAlways = x => _ => x
 *   const ifAlways = x => isAlways (x) | ifPredicate
 *   const isAlways = always
 *   const ifAlways = x => always (x) | ifPredicate
 *   const ifAlways = always >> ifPredicate
 */exports.whenPredicateWithResults=whenPredicateWithResults;var ifAlways=composeRight(always,ifPredicate);exports.ifAlways=ifAlways;var whenAlways=composeRight(always,whenPredicate);/* Truthy / falsey */exports.whenAlways=whenAlways;var isYes=Boolean;exports.isYes=isYes;var isNo=not;// --- @future whenNil, ifNil
// ------ types.
exports.isNo=isNo;var getType=function getType(x){return _internal.oStr.call(x).slice(8,-1);};exports.getType=getType;var isType=function isType(t){return function(x){return getType(x)===t;};};// --- Proxy counts as a function (it's how lodash does it).
exports.isType=isType;var isFunction=function isFunction(o){var type=getType(o);return false||type==='Function'||type==='GeneratorFunction'||type==='AsyncFunction'||type==='Proxy';};exports.isFunction=isFunction;var isArray=/*#__PURE__*/isType('Array');exports.isArray=isArray;var isObject=/*#__PURE__*/isType('Object');exports.isObject=isObject;var isNumber=/*#__PURE__*/isType('Number');exports.isNumber=isNumber;var isRegExp=/*#__PURE__*/isType('RegExp');exports.isRegExp=isRegExp;var isBoolean=/*#__PURE__*/isType('Boolean');exports.isBoolean=isBoolean;var isString=/*#__PURE__*/isType('String');exports.isString=isString;var isSymbol=/*#__PURE__*/isType('Symbol');exports.isSymbol=isSymbol;var tap=function tap(f){return function(o){return f(o),o;};};exports.tap=tap;var dot=function dot(prop){return function(o){return o[prop]();};};exports.dot=dot;var dot1=function dot1(prop){return function(val){return function(o){return o[prop](val);};};};exports.dot1=dot1;var dot2=function dot2(prop){return function(val1){return function(val2){return function(o){return o[prop](val1,val2);};};};};exports.dot2=dot2;var dot3=function dot3(prop){return function(val1){return function(val2){return function(val3){return function(o){return o[prop](val1,val2,val3);};};};};};exports.dot3=dot3;var dot4=function dot4(prop){return function(val1){return function(val2){return function(val3){return function(val4){return function(o){return o[prop](val1,val2,val3,val4);};};};};};};exports.dot4=dot4;var dot5=function dot5(prop){return function(val1){return function(val2){return function(val3){return function(val4){return function(val5){return function(o){return o[prop](val1,val2,val3,val4,val5);};};};};};};};exports.dot5=dot5;var dotN=function dotN(prop){return function(vs){return function(o){return o[prop].apply(o,(0,_toConsumableArray2.default)(vs));};};};exports.dotN=dotN;var side=function side(prop){return function(o){return dot(prop)(o),o;};};exports.side=side;var side1=function side1(prop){return function(val1){return function(o){return dot1(prop)(val1)(o),o;};};};exports.side1=side1;var side2=function side2(prop){return function(val1){return function(val2){return function(o){return dot2(prop)(val1)(val2)(o),o;};};};};exports.side2=side2;var side3=function side3(prop){return function(val1){return function(val2){return function(val3){return function(o){return dot3(prop)(val1)(val2)(val3)(o),o;};};};};};exports.side3=side3;var side4=function side4(prop){return function(val1){return function(val2){return function(val3){return function(val4){return function(o){return dot4(prop)(val1)(val2)(val3)(val4)(o),o;};};};};};};exports.side4=side4;var side5=function side5(prop){return function(val1){return function(val2){return function(val3){return function(val4){return function(val5){return function(o){return dot5(prop)(val1)(val2)(val3)(val4)(val5)(o),o;};};};};};};};exports.side5=side5;var sideN=function sideN(prop){return function(vs){return function(o){return dotN(prop)(vs)(o),o;};};};// --- @future reversed version of `has.
/* These all ignore symbol keys. */exports.sideN=sideN;var has=function has(k){return function(o){return _internal.hasOwnProperty.call(o,k);};};exports.has=has;var hasIn=function hasIn(k){return function(o){return k in o;};};exports.hasIn=hasIn;var ifHas=function ifHas(yes){return function(no){return function(_ref){var _ref2=(0,_slicedToArray2.default)(_ref,2),o=_ref2[0],k=_ref2[1];return has(k)(o)?yes(o[k],o,k):no(o,k);};};};exports.ifHas=ifHas;var whenHas=function whenHas(yes){return ifHas(yes)(noop);};exports.whenHas=whenHas;var ifHasIn=function ifHasIn(yes){return function(no){return function(_ref3){var _ref4=(0,_slicedToArray2.default)(_ref3,2),o=_ref4[0],k=_ref4[1];return hasIn(k)(o)?yes(o[k],o,k):no(o,k);};};};exports.ifHasIn=ifHasIn;var whenHasIn=function whenHasIn(yes){return ifHasIn(yes)(noop);};exports.whenHasIn=whenHasIn;var bindLatePropTo=function bindLatePropTo(o){return function(prop){return function(){return o[prop].apply(o,arguments);};};};exports.bindLatePropTo=bindLatePropTo;var bindLateProp=function bindLateProp(prop){return function(o){return function(){return o[prop].apply(o,arguments);};};};exports.bindLateProp=bindLateProp;var bindPropTo=function bindPropTo(o){return function(prop){return o[prop].bind(o);};};exports.bindPropTo=bindPropTo;var bindProp=function bindProp(prop){return function(o){return o[prop].bind(o);};};exports.bindProp=bindProp;var bindTo=function bindTo(o){return function(f){return f.bind(o);};};exports.bindTo=bindTo;var bind=function bind(f){return function(o){return f.bind(o);};};// --- bindTry* returns `null` if o[prop] is not a function.
exports.bind=bind;var bindTryPropTo=function bindTryPropTo(o){return function(prop){return typeof o[prop]==='function'?bindPropTo(o)(prop):null;};};exports.bindTryPropTo=bindTryPropTo;var bindTryProp=function bindTryProp(prop){return function(o){return typeof o[prop]==='function'?bindPropTo(o)(prop):null;};};exports.bindTryProp=bindTryProp;var bindTryTo=function bindTryTo(o){return function(f){return typeof f==='function'?bindTo(o)(f):null;};};exports.bindTryTo=bindTryTo;var bindTry=function bindTry(f){return function(o){return typeof f==='function'?bindTo(o)(f):null;};};// --- @experimental
// --- @todo this is not that nice to use.
// --- trier is e.g. `bindTryPropTo`
exports.bindTry=bindTry;var ifBind=function ifBind(trier){return ifPredicateWithResults(passToN(trier));};exports.ifBind=ifBind;var whenBind=function whenBind(trier){return function(yes){return ifBind(trier)(yes)(noop);};};/* Usage
 *   cond (
 *     [_ => 3 == 4, _ => 'twilight zone'],
 *     [_ => 3 == 5, _ => 'even stranger'],
 *     [T, _ => 'ok'],
 *   )
 *
 * or with a native idiom:
 *
 * cond (
 *   (_ => 3 == 4) | guard (_ => 'twilight zone'),
 *   (_ => 3 == 5) | guard (_ => 'even stranger'),
 *   otherwise     | guard (_ => 'ok'),
 * )
 *
 * `guardV` is a convenience for a guard which returns a simple expression, so guard (_ => 'twilight zone')
 * could be replaced by guardV ('twilight zone')
 *
 * The advantage of the left side being functions is that they are late-evaluated, they stop as soon
 * as one matches, and they can interact with a piped argument (see `condS` below.
 *
 * If you want simple, eager constant conditions on the left, just use a table :)
 *
 */exports.whenBind=whenBind;var T=always(true);exports.T=T;var F=always(false);exports.F=F;var condPredicate=function condPredicate(exec){return function(pred){return[pred,exec];};};/*
 * Tests on truthiness, not strict equality.
 * This is how `if` works, it's how `cond` works in Ramda, and it's trivial to convert truthy to
 * strict.
 */exports.condPredicate=condPredicate;var cond=function cond(){for(var _len=arguments.length,blocks=new Array(_len),_key=0;_key<_len;_key++){blocks[_key]=arguments[_key];}for(var _i=0,_blocks=blocks;_i<_blocks.length;_i++){var _blocks$_i=(0,_slicedToArray2.default)(_blocks[_i],2),test=_blocks$_i[0],exec=_blocks$_i[1];var result=test();if(result)return exec(result);}};exports.cond=cond;var condS=function condS(blocks){return function(target){var _iterator=_createForOfIteratorHelper(blocks),_step;try{for(_iterator.s();!(_step=_iterator.n()).done;){var _step$value=(0,_slicedToArray2.default)(_step.value,2),test=_step$value[0],exec=_step$value[1];var result=test(target);if(result)return exec(target,result);}}catch(err){_iterator.e(err);}finally{_iterator.f();}};};exports.condS=condS;var add=function add(m){return function(n){return m+n;};};exports.add=add;var multiply=function multiply(m){return function(n){return m*n;};};exports.multiply=multiply;var divideBy=function divideBy(m){return function(n){return n/m;};};exports.divideBy=divideBy;var divideInto=function divideInto(m){return function(n){return m/n;};};exports.divideInto=divideInto;var subtract=function subtract(m){return function(n){return n-m;};};exports.subtract=subtract;var subtractFrom=function subtractFrom(m){return function(n){return m-n;};};exports.subtractFrom=subtractFrom;var modulo=function modulo(m){return function(n){return n%m;};};exports.modulo=modulo;var moduloWholePart=function moduloWholePart(m){return function(n){var div=n/m;var flo=Math.floor(div);return div<0?1+flo:flo;};};exports.moduloWholePart=moduloWholePart;var toThe=function toThe(e){return function(b){return Math.pow(b,e);};};// ------ exceptions
// --- @todo consider adding tryCatchS:
// const tryCatchS = (good) => (bad) => (f) => (v) => {
//  ... successVal = f (v)
// }
exports.toThe=toThe;var tryCatch=function tryCatch(good){return function(bad){return function(f){// @todo simplify: try { return good (f ()) }
var successVal;try{successVal=f();}catch(e){return bad(e);}return good(successVal);};};};/* Despite the name, `die` simply throws an exception, which can of course be caught.
 * (It shouldn't be too surprising to JS users that it doesn't really 'die', i.e. halt the runtime.)
 */exports.tryCatch=tryCatch;var exception=function exception(){for(var _len2=arguments.length,args=new Array(_len2),_key2=0;_key2<_len2;_key2++){args[_key2]=arguments[_key2];}return new Error(args.join(' '));};exports.exception=exception;var raise=function raise(e){throw e;};exports.raise=raise;var die=composeRight(exception,raise);exports.die=die;var defaultTo=function defaultTo(f){return function(x){return ok(x)?x:f();};};// ------ object stuff.
exports.defaultTo=defaultTo;var prop=function prop(p){return function(o){return o[p];};};exports.prop=prop;var propOf=function propOf(o){return function(p){return o[p];};};exports.propOf=propOf;var path=function path(xs){return function(o){var j=o;var _iterator2=_createForOfIteratorHelper(xs),_step2;try{for(_iterator2.s();!(_step2=_iterator2.n()).done;){var i=_step2.value;if(!ok(j))return j;else j=j[i];}}catch(err){_iterator2.e(err);}finally{_iterator2.f();}return j;};};exports.path=path;var pathOf=function pathOf(o){return function(xs){return path(xs)(o);};};// --- @todo update and assoc convert array input to an object unfortunately (due to the merge step).
exports.pathOf=pathOf;var assoc=function assoc(prop){return function(val){return function(o){var oo=mergeInM(o)((0,_internal.safeObject)());oo[prop]=val;return oo;};};};exports.assoc=assoc;var assocM=function assocM(prop){return function(val){return function(o){return o[prop]=val,o;};};};exports.assocM=assocM;var assocPath=function assocPath(xs){return function(x){return function(o){return assocPathM(xs)(x)(mergeInM(o)((0,_internal.safeObject)()));};};};exports.assocPath=assocPath;var assocPathM=function assocPathM(xs){return function(x){return function(o){var reducer=function reducer(ptr,pat,el){if(!ok(pat))return[ptr,el];var pp=ptr[pat];var ppp=isArray(pp)||isObject(pp)?pp:ptr[pat]=(0,_internal.safeObject)();return[ppp,el];};var _xs$reduce=xs.reduce(function(_ref5,x){var _ref6=(0,_slicedToArray2.default)(_ref5,2),p=_ref6[0],s=_ref6[1];return reducer(p,s,x);},[o,null]),_xs$reduce2=(0,_slicedToArray2.default)(_xs$reduce,2),ptr=_xs$reduce2[0],pat=_xs$reduce2[1];ptr[pat]=x;return o;};};};/* These always flatten the prototypes, like `assoc`. */exports.assocPathM=assocPathM;var updateM=function updateM(prop){return function(f){return function(o){return o[prop]=f(o[prop]),o;};};};exports.updateM=updateM;var update=function update(prop){return function(f){return function(o){var oo=merge(o)((0,_internal.safeObject)());oo[prop]=f(o[prop]);return oo;};};};/* These are not separately unit tested because they route through `assocPath/M`, which are. */exports.update=update;var updatePathM=function updatePathM(xs){return function(f){return function(o){var x=path(xs)(o);return assocPathM(xs)(f(x))(o);};};};exports.updatePathM=updatePathM;var updatePath=function updatePath(xs){return function(f){return function(o){var x=path(xs)(o);return assocPath(xs)(f(x))(o);};};};exports.updatePath=updatePath;var append=function append(elem){return function(ary){return[].concat((0,_toConsumableArray2.default)(ary),[elem]);};};exports.append=append;var appendTo=function appendTo(ary){return function(elem){return[].concat((0,_toConsumableArray2.default)(ary),[elem]);};};exports.appendTo=appendTo;var appendToM=function appendToM(tgt){return function(src){return tgt.push(src),tgt;};};exports.appendToM=appendToM;var appendM=function appendM(src){return function(tgt){return tgt.push(src),tgt;};};exports.appendM=appendM;var prependTo=function prependTo(ary){return function(elem){return[elem].concat((0,_toConsumableArray2.default)(ary));};};exports.prependTo=prependTo;var prepend=function prepend(elem){return function(ary){return[elem].concat((0,_toConsumableArray2.default)(ary));};};exports.prepend=prepend;var prependM=function prependM(src){return function(tgt){return tgt.unshift(src),tgt;};};exports.prependM=prependM;var prependToM=function prependToM(tgt){return function(src){return tgt.unshift(src),tgt;};};exports.prependToM=prependToM;var concatTo=function concatTo(tgt){return function(src){return tgt.concat(src);};};exports.concatTo=concatTo;var concat=function concat(src){return function(tgt){return tgt.concat(src);};};/* Note: these only work for arrays, not strings (strings are always immutable in JS)
 */exports.concat=concat;var concatToM=function concatToM(tgt){return function(src){return tgt.push.apply(tgt,(0,_toConsumableArray2.default)(src)),tgt;};};exports.concatToM=concatToM;var concatM=function concatM(src){return function(tgt){return tgt.push.apply(tgt,(0,_toConsumableArray2.default)(src)),tgt;};};// --- @todo these seem to be much faster than Object.assign, check.
exports.concatM=concatM;var mergeToM=function mergeToM(tgt){return function(src){var _iterator3=_createForOfIteratorHelper(Reflect.ownKeys(src)),_step3;try{for(_iterator3.s();!(_step3=_iterator3.n()).done;){var i=_step3.value;tgt[i]=src[i];}}catch(err){_iterator3.e(err);}finally{_iterator3.f();}return tgt;};};exports.mergeToM=mergeToM;var mergeM=function mergeM(src){return function(tgt){var _iterator4=_createForOfIteratorHelper(Reflect.ownKeys(src)),_step4;try{for(_iterator4.s();!(_step4=_iterator4.n()).done;){var i=_step4.value;tgt[i]=src[i];}}catch(err){_iterator4.e(err);}finally{_iterator4.f();}return tgt;};};exports.mergeM=mergeM;var mergeTo=function mergeTo(tgt){return function(src){var a=mergeToM((0,_internal.safeObject)())(tgt);return mergeToM(a)(src);};};exports.mergeTo=mergeTo;var merge=function merge(src){return function(tgt){var a=mergeToM((0,_internal.safeObject)())(tgt);return mergeToM(a)(src);};};/* The 'in' variants ignore all symbol keys.
*/exports.merge=merge;var mergeInToM=function mergeInToM(tgt){return function(src){for(var i in src){tgt[i]=src[i];}return tgt;};};exports.mergeInToM=mergeInToM;var mergeInM=function mergeInM(src){return function(tgt){return mergeInToM(tgt)(src);};};exports.mergeInM=mergeInM;var mergeInTo=function mergeInTo(tgt){return function(src){var a=mergeInToM((0,_internal.safeObject)())(tgt);return mergeInToM(a)(src);};};exports.mergeInTo=mergeInTo;var mergeIn=function mergeIn(src){return function(tgt){return mergeInTo(tgt)(src);};};exports.mergeIn=mergeIn;var getMergeX=function getMergeX(pluck){return function(mergerSym){return ifPredicate(ok)(pluck)(function(_){return die(sprintf('No merge function for symbol "%s"'));})(merges()[mergerSym]);};};// --- throw on failure.
var getMergeFunction=getMergeX(function(_ref7){var f=_ref7.f;return f;});var getMergeInfo=getMergeX(function(_ref8){var to=_ref8.to,mut=_ref8.mut,own=_ref8.own;return{to:to,mut:mut,own:own};});var mergeToMSym=Symbol('mergeToM');exports.mergeToMSym=mergeToMSym;var mergeToSym=Symbol('mergeTo');exports.mergeToSym=mergeToSym;var mergeMSym=Symbol('mergeM');exports.mergeMSym=mergeMSym;var mergeSym=Symbol('merge');exports.mergeSym=mergeSym;var mergeInToMSym=Symbol('mergeInToM');exports.mergeInToMSym=mergeInToMSym;var mergeInToSym=Symbol('mergeInTo');exports.mergeInToSym=mergeInToSym;var mergeInMSym=Symbol('mergeInM');exports.mergeInMSym=mergeInMSym;var mergeInSym=Symbol('mergeIn');// --- like R.mergeAll but also use prototype vals.
// --- to and from not applicable, also not curried or meant to be used piped.
exports.mergeInSym=mergeInSym;var mergeAllIn=function mergeAllIn(xs){return xs.reduce(function(tgt,src){return mergeInToM(tgt)(src);},(0,_internal.safeObject)());};exports.mergeAllIn=mergeAllIn;var merges=function merges(_){var _ref9;return _ref9={},(0,_defineProperty2.default)(_ref9,mergeToMSym,{f:mergeToM,to:true,mut:true,own:true}),(0,_defineProperty2.default)(_ref9,mergeToSym,{f:mergeTo,to:true,mut:false,own:true}),(0,_defineProperty2.default)(_ref9,mergeMSym,{f:mergeM,to:false,mut:true,own:true}),(0,_defineProperty2.default)(_ref9,mergeSym,{f:merge,to:false,mut:false,own:true}),(0,_defineProperty2.default)(_ref9,mergeInToMSym,{f:mergeInToM,to:true,mut:true,own:false}),(0,_defineProperty2.default)(_ref9,mergeInToSym,{f:mergeInTo,to:true,mut:false,own:false}),(0,_defineProperty2.default)(_ref9,mergeInMSym,{f:mergeInM,to:false,mut:true,own:false}),(0,_defineProperty2.default)(_ref9,mergeInSym,{f:mergeIn,to:false,mut:false,own:false}),_ref9;};// --- tgt will be altered.
// only `own` needs to be passed: direction and mutability have already been decided.
//
// in the M case tgt is just the tgt;
// in the non-M case it has been prepared to be a new copy.
//
// `own` refers to both tgt & src -- not possible to mix and match.
// --- a performance hit is acceptable here.
var mergeXWith=function mergeXWith(collision){return function(own){return function(src){return function(tgt){var _ref10=own?[whenHas,ifHas]:[whenHasIn,ifHasIn],_ref11=(0,_slicedToArray2.default)(_ref10,2),_whenHas=_ref11[0],_ifHas=_ref11[1];var doKey=function doKey(key){return _whenHas(function(v,o,k){return _ifHas(function(v,o,k){return tgt[key]=collision(src[key],tgt[key]);})(function(o,k){return tgt[key]=src[key];})([tgt,key]);})([src,key]);};for(var key in src){doKey(key);}// --- for non-own we ignore all symbol keys.
if(own){var _iterator5=_createForOfIteratorHelper(Object.getOwnPropertySymbols(src)),_step5;try{for(_iterator5.s();!(_step5=_iterator5.n()).done;){var _key3=_step5.value;doKey(_key3);}}catch(err){_iterator5.e(err);}finally{_iterator5.f();}}return tgt;};};};};var mergeWith=function mergeWith(collision){return function(mergerSym){// --- fail early instead of continuing with curry (throws)
var merger=getMergeFunction(mergerSym);return function(a){return function(b){var _getMergeInfo=getMergeInfo(mergerSym),to=_getMergeInfo.to,mut=_getMergeInfo.mut,own=_getMergeInfo.own;var _ref12=to?[b,a]:[a,b],_ref13=(0,_slicedToArray2.default)(_ref12,2),src=_ref13[0],tgt=_ref13[1];var tgtM=mut?tgt:to?merger((0,_internal.safeObject)())(tgt):merger(tgt)((0,_internal.safeObject)());return mergeXWith(collision)(own)(src)(tgtM);};};};};// --- like with 'with', mut and direction have already been arranged, and `tgt` will be mutated.
// --- tests `p` for truthiness.
exports.mergeWith=mergeWith;var mergeXWhen=function mergeXWhen(p){return function(own){return function(src){return function(tgt){var f=function f(key){if(p(src[key],tgt[key]))tgt[key]=src[key];};if(own){var _iterator6=_createForOfIteratorHelper(Reflect.ownKeys(src)),_step6;try{for(_iterator6.s();!(_step6=_iterator6.n()).done;){var i=_step6.value;f(i);}}catch(err){_iterator6.e(err);}finally{_iterator6.f();}}else for(var _i2 in src){f(_i2);}return tgt;};};};};// @todo mergeXWhen and mergeXWith should be one function => simpler
var mergeWhen=function mergeWhen(p){return function(mergerSym){// --- fail early instead of continuing with curry (throws)
var merger=getMergeFunction(mergerSym);return function(a){return function(b){var _getMergeInfo2=getMergeInfo(mergerSym),to=_getMergeInfo2.to,mut=_getMergeInfo2.mut,own=_getMergeInfo2.own;var _ref14=to?[b,a]:[a,b],_ref15=(0,_slicedToArray2.default)(_ref14,2),src=_ref15[0],tgt=_ref15[1];var tgtM=mut?tgt:to?merger((0,_internal.safeObject)())(tgt):merger(tgt)((0,_internal.safeObject)());return mergeXWhen(p)(own)(src)(tgtM);};};};};/* The following functions dispatch to the object's prototype method: `map`, `reduce`,
 * `reduceRight`, `find`, `filter`, `reject`.
 *
 * 'Capped' below refers to the `f` or `p` functions, not to the prototype function (e.g. `map`
 * itself).
 *
 * 'capped at 1' means the function will be called with exactly 1 argument.
 */ /* `f` is capped at 1.
 */exports.mergeWhen=mergeWhen;var map=function map(f){return function(xs){return xs.map(function(x){return f(x);});};};exports.map=map;var each=function each(f){return function(xs){return xs.forEach(function(x){return f(x);});};};/* `f` is capped at 2.
 * dispatches to `xs.reduce`
 */exports.each=each;var reduce=function reduce(f){return function(acc){return function(xs){return xs.reduce(function(acc,x){return f(acc,x);},acc);};};};/* `f` is capped at 2.
 *
 * Does not dispatch to `xs.reduceRight`. Also, the order of `acc` and `f` within the reducer is reversed with respect to
 * `xs.reduceRight`, but is the familiar order from Haskell.
 *
 */exports.reduce=reduce;var reduceRight=function reduceRight(f){return function(acc){return function(xs){var acco=acc;var l=xs.length;if(l===0)return acco;for(var i=l-1;i>=0;i-=1){acco=f(xs[i],acco);}return acco;};};};/* Like `reduceRight`, but assumes `f` is curried.
 *
 * This allows you to write `f` in a more point-free way in certain cases, by omitting the accumulator.
 *
 * For example, a function which multiplies each element by 2 and reverses the list:
 *
 *   const reverseDouble = reduceRight (
 *     (x, acc) => {
 *       const clone = [... acc]
 *       clone.push (x * 2)
 *       return clone
 *     },
 *     [],
 *   )
 *
 * can be written as
 *
 *   const reverseDouble = reduceRight (
 *     (x, acc) => acc | append (x * 2),
 *     [],
 *   )
 *
 * Then, using `reduceRightC`:
 *
 *   const reverseDouble = reduceRightC (
 *     x => acc => acc | append (x * 2),
 *     [],
 *   )
 *
 * And finally:
 *
 *   const reverseDouble = reduceRightC (
 *     x => append (x * 2),
 *     [],
 *   )
 *
 * And if you want to go further:
 *
 *   const reverseDouble = reduceRightC (
 *     multiply (2) >> append,
 *     [],
 *   )
 *
 * Note: does not work with addIndex/addCollection (will need separate 'C' versions of those).
 */exports.reduceRight=reduceRight;var reduceRightC=function reduceRightC(f){return function(accInit){return function(xs){var acc=accInit;var l=xs.length;if(l===0)return acc;for(var i=l-1;i>=0;i-=1){acc=f(xs[i])(acc);}return acc;};};};/* `p` is capped at 1.
 *
 * Note that it's not possible to search for `undefined` in a list using this function. If you
 * really need this, consider `findIndex` or `contains`/`containsV`.
 */exports.reduceRightC=reduceRightC;var find=function find(p){return function(xs){return xs.find(function(x){return p(x);});};};exports.find=find;var findIndex=function findIndex(p){return function(xs){var i=0;var _iterator7=_createForOfIteratorHelper(xs),_step7;try{for(_iterator7.s();!(_step7=_iterator7.n()).done;){var x=_step7.value;if(p(x))return i;i++;}}catch(err){_iterator7.e(err);}finally{_iterator7.f();}return undefined;};};exports.findIndex=findIndex;var findWithIndex=function findWithIndex(p){return function(xs){var i=0;var _iterator8=_createForOfIteratorHelper(xs),_step8;try{for(_iterator8.s();!(_step8=_iterator8.n()).done;){var x=_step8.value;if(p(x))return[x,i];i++;}}catch(err){_iterator8.e(err);}finally{_iterator8.f();}return[undefined,undefined];};};/* `f` is capped at 1.
 */exports.findWithIndex=findWithIndex;var filter=function filter(f){return function(xs){return xs.filter(function(x){return f(x);});};};exports.filter=filter;var reject=function reject(f){return function(xs){return xs.filter(function(x){return!f(x);});};};exports.reject=reject;var containsV=function containsV(v){return function(xs){var _iterator9=_createForOfIteratorHelper(xs),_step9;try{for(_iterator9.s();!(_step9=_iterator9.n()).done;){var x=_step9.value;if(x===v)return true;}}catch(err){_iterator9.e(err);}finally{_iterator9.f();}return false;};};exports.containsV=containsV;var contains=function contains(p){return function(xs){var _iterator10=_createForOfIteratorHelper(xs),_step10;try{for(_iterator10.s();!(_step10=_iterator10.n()).done;){var x=_step10.value;if(p(x))return true;}}catch(err){_iterator10.e(err);}finally{_iterator10.f();}return false;};};/* @experimental
 *
 * Not clear this is useful. abortVal should be a predicate instead of a val, and then, why not just
 * allow the caller to add a condition to `f`?
 */exports.contains=contains;var reduceAbort=function reduceAbort(f){return function(accInit){return function(abortVal){return function(xs){var acc=accInit;var _iterator11=_createForOfIteratorHelper(xs),_step11;try{for(_iterator11.s();!(_step11=_iterator11.n()).done;){var x=_step11.value;var g=f(acc,x);if(g===abortVal)return abortVal;acc=g;}}catch(err){_iterator11.e(err);}finally{_iterator11.f();}return acc;};};};};// --- dropping too many returns an empty list.
exports.reduceAbort=reduceAbort;var drop=function drop(x){return function(xs){return xs.slice(x);};};// --- taking too many returns the entire list.
exports.drop=drop;var take=function take(x){return function(xs){return xs.slice(0,x);};};// --- returns obj. Ignores symbol keys of `o`.
exports.take=take;var eachObj=function eachObj(f){return function(o){for(var k in o){if(_internal.hasOwnProperty.call(o,k))f(o[k],k);}return o;};};// --- returns obj
exports.eachObj=eachObj;var eachObjIn=function eachObjIn(f){return function(o){for(var k in o){f(o[k],k);}return o;};};/* `addIndex` and `addCollection` work on lists and objects.
 */exports.eachObjIn=eachObjIn;var addIndex=function addIndex(orig){return function(f){var idx=-1;var g=function g(){for(var _len3=arguments.length,args=new Array(_len3),_key4=0;_key4<_len3;_key4++){args[_key4]=arguments[_key4];}return f.apply(void 0,args.concat([++idx]));};return orig(g);};};exports.addIndex=addIndex;var addCollection=function addCollection(orig){return function(f){return function(coll){var g=function g(){for(var _len4=arguments.length,args=new Array(_len4),_key5=0;_key5<_len4;_key5++){args[_key5]=arguments[_key5];}return f.apply(void 0,args.concat([coll]));};return orig(g)(coll);};};};// --- successive forms can use the same function but just need to be recurried with different
// arity.
exports.addCollection=addCollection;var addIndex2=addIndex;/*
const addIndexC = (orig) => (f) => {
  let idx = -1
  // --- assumes args is non-empty (i.e., the function being mapped/reduced/etc. can't be called
  // without arguments.
  const g = (...args) => {
    const [x, ...ys] = args
    let o = f (x)
    for (const y in ys)
      o = o (y)
    return o (++idx)
  }
  return orig (g)
}
*/exports.addIndex2=addIndex2;var addCollection2=function addCollection2(orig){return function(f){return function(x){return function(coll){var g=function g(){for(var _len5=arguments.length,args=new Array(_len5),_key6=0;_key6<_len5;_key6++){args[_key6]=arguments[_key6];}return f.apply(void 0,args.concat([coll]));};return orig(g)(x)(coll);};};};};/* Ignores symbol keys of `o`. */exports.addCollection2=addCollection2;var reduceObj=function reduceObj(f){return function(acc){return function(o){var curAcc=acc;for(var k in o){if(_internal.hasOwnProperty.call(o,k))curAcc=f(curAcc,[k,o[k]]);}return curAcc;};};};exports.reduceObj=reduceObj;var reduceObjIn=function reduceObjIn(f){return function(acc){return function(o){var curAcc=acc;for(var k in o){curAcc=f(curAcc,[k,o[k]]);}return curAcc;};};};/* --- @experimental: need more intuitive interface.
 * --- e.g. 3 | ampersandN ([inc, double, odd]) = [4, 6, true]
 * --- could alias `pam`:
 *   const pam = ampersandN
 *   3 | pam ([inc, double, odd])
 */exports.reduceObjIn=reduceObjIn;var ampersandN=function ampersandN(fs){return function(x){var mapper=function mapper(f){return f(x);};return map(mapper)(fs);};};/* @todo

asteriskNN: fs => xs
asterisk2N: f => g => xs
asterisk2:  f => g => a => b

asteriskMapNN: fs => xs
asteriskMap2N: f => g => xs
asteriskMap2:  f => g => a => b

asteriskAppNN: xs => fs
asteriskApp2N: a => b => fs
asteriskApp2:  a => b => f => g

anvilNN

asterisk = anvilNN

const arrowSnd = f => timesV (2) >> asteriskN ([id, f])

*/ // --- @experimental
// --- asterisk is like a dot product of vectors: it takes a list of functions and a list of values
// and runs the functions corresponding to position.
// --- @todo make more intuitive.
exports.ampersandN=ampersandN;var asteriskN=function asteriskN(fs){return function(xs){var ret=[];var i=-1;var _iterator12=_createForOfIteratorHelper(fs),_step12;try{for(_iterator12.s();!(_step12=_iterator12.n()).done;){var f=_step12.value;var x=xs[++i];ret.push(f(x));}}catch(err){_iterator12.e(err);}finally{_iterator12.f();}return ret;};};// --- @todo: asteriskN, for when given an array of functions and a corresponding array of callees.
exports.asteriskN=asteriskN;var asterisk1=function asterisk1(f){return function(a){return[f(a)];};};exports.asterisk1=asterisk1;var asterisk2=function asterisk2(f){return function(g){return function(a){return function(b){return[f(a),g(b)];};};};};exports.asterisk2=asterisk2;var asterisk3=function asterisk3(f){return function(g){return function(h){return function(a){return function(b){return function(c){return[f(a),g(b),h(c)];};};};};};};exports.asterisk3=asterisk3;var asterisk4=function asterisk4(f){return function(g){return function(h){return function(i){return function(a){return function(b){return function(c){return function(d){return[f(a),g(b),h(c),i(d)];};};};};};};};};exports.asterisk4=asterisk4;var asterisk5=function asterisk5(f){return function(g){return function(h){return function(i){return function(j){return function(a){return function(b){return function(c){return function(d){return function(e){return[f(a),g(b),h(c),i(d),j(e)];};};};};};};};};};};// --------- lets / let / let...
exports.asterisk5=asterisk5;var letNV=function letNV(xs){return function(f){return f.apply(null,xs);};};/*
 * lets = let* from racket
 * letN = let* + array
 * letS = let* + stick (S implies N)
 * lets1, lets2, etc.: wrapped by lets, but can be called directly too.
 * letNV = like letV, with array
 * letV = let with values instead of functions
 */ // --- last arg must be a function.
// --- 1 arg is possible but trivial.
exports.letNV=letNV;var letV=function letV(){for(var _len6=arguments.length,xs=new Array(_len6),_key7=0;_key7<_len6;_key7++){xs[_key7]=arguments[_key7];}var f=xs.pop();return letNV(xs)(f);};/*
 * For example, our `defaultTo` takes a function:
 * null | defaultTo (_ -> 'bad news')
 * For simple values, defaultToV can be more convenient:
 * null | defaultToV ('bad news')
*/ // --- trivial form.
exports.letV=letV;var lets1=function lets1(f){return invoke(f);};// --- @experimental lets2 - lets6 can be called directly as an optimisation (there is no pushing to
// an array or spreading values)
exports.lets1=lets1;var lets2=function lets2(f1,f2){var n1=f1();return f2(n1);};exports.lets2=lets2;var lets3=function lets3(f1,f2,f3){var n1=f1();var n2=f2(n1);return f3(n1,n2);};exports.lets3=lets3;var lets4=function lets4(f1,f2,f3,f4){var n1=f1();var n2=f2(n1);var n3=f3(n1,n2);return f4(n1,n2,n3);};exports.lets4=lets4;var lets5=function lets5(f1,f2,f3,f4,f5){var n1=f1();var n2=f2(n1);var n3=f3(n1,n2);var n4=f4(n1,n2,n3);return f5(n1,n2,n3,n4);};exports.lets5=lets5;var lets6=function lets6(f1,f2,f3,f4,f5,f6){var n1=f1();var n2=f2(n1);var n3=f3(n1,n2);var n4=f4(n1,n2,n3);var n5=f5(n1,n2,n3,n4);return f6(n1,n2,n3,n4,n5);};exports.lets6=lets6;var letN=function letN(xs){return lets.apply(void 0,(0,_toConsumableArray2.default)(xs));};exports.letN=letN;var lets=function lets(){var acc=[];var last;for(var _len7=arguments.length,fs=new Array(_len7),_key8=0;_key8<_len7;_key8++){fs[_key8]=arguments[_key8];}for(var _i3=0,_fs=fs;_i3<_fs.length;_i3++){var f=_fs[_i3];var ret=f.apply(void 0,acc);acc.push(ret);last=ret;}return last;};exports.lets=lets;var letS=function letS(specAry){return function(tgt){return lets.apply(void 0,[function(_){return tgt;}].concat((0,_toConsumableArray2.default)(specAry)));};};// ------ call/provide
exports.letS=letS;var callOn=function callOn(o){return function(f){return f.call(o);};};exports.callOn=callOn;var callOn1=function callOn1(o){return function(val1){return function(f){return f.call(o,val1);};};};exports.callOn1=callOn1;var callOn2=function callOn2(o){return function(val1){return function(val2){return function(f){return f.call(o,val1,val2);};};};};exports.callOn2=callOn2;var callOn3=function callOn3(o){return function(val1){return function(val2){return function(val3){return function(f){return f.call(o,val1,val2,val3);};};};};};exports.callOn3=callOn3;var callOn4=function callOn4(o){return function(val1){return function(val2){return function(val3){return function(val4){return function(f){return f.call(o,val1,val2,val3,val4);};};};};};};exports.callOn4=callOn4;var callOn5=function callOn5(o){return function(val1){return function(val2){return function(val3){return function(val4){return function(val5){return function(f){return f.call(o,val1,val2,val3,val4,val5);};};};};};};};exports.callOn5=callOn5;var callOnN=function callOnN(o){return function(vs){return function(f){return f.apply(o,vs);};};};exports.callOnN=callOnN;var provideTo=function provideTo(f){return function(o){return f.call(o);};};exports.provideTo=provideTo;var provideTo1=function provideTo1(f){return function(val){return function(o){return f.call(o,val);};};};exports.provideTo1=provideTo1;var provideTo2=function provideTo2(f){return function(val1){return function(val2){return function(o){return f.call(o,val1,val2);};};};};exports.provideTo2=provideTo2;var provideTo3=function provideTo3(f){return function(val1){return function(val2){return function(val3){return function(o){return f.call(o,val1,val2,val3);};};};};};exports.provideTo3=provideTo3;var provideTo4=function provideTo4(f){return function(val1){return function(val2){return function(val3){return function(val4){return function(o){return f.call(o,val1,val2,val3,val4);};};};};};};exports.provideTo4=provideTo4;var provideTo5=function provideTo5(f){return function(val1){return function(val2){return function(val3){return function(val4){return function(val5){return function(o){return f.call(o,val1,val2,val3,val4,val5);};};};};};};};exports.provideTo5=provideTo5;var provideToN=function provideToN(f){return function(vs){return function(o){return f.apply(o,vs);};};};exports.provideToN=provideToN;var invoke=function invoke(f){return f();};exports.invoke=invoke;var applyTo1=function applyTo1(val1){return function(f){return f(val1);};};exports.applyTo1=applyTo1;var applyTo2=function applyTo2(val1){return function(val2){return function(f){return f(val1,val2);};};};exports.applyTo2=applyTo2;var applyTo3=function applyTo3(val1){return function(val2){return function(val3){return function(f){return f(val1,val2,val3);};};};};exports.applyTo3=applyTo3;var applyTo4=function applyTo4(val1){return function(val2){return function(val3){return function(val4){return function(f){return f(val1,val2,val3,val4);};};};};};exports.applyTo4=applyTo4;var applyTo5=function applyTo5(val1){return function(val2){return function(val3){return function(val4){return function(val5){return function(f){return f(val1,val2,val3,val4,val5);};};};};};};exports.applyTo5=applyTo5;var applyToN=function applyToN(vs){return function(f){return f.apply(null,vs);};};exports.applyToN=applyToN;var passTo=function passTo(f){return function(val){return f(val);};};exports.passTo=passTo;var passToN=function passToN(f){return function(vs){return f.apply(null,vs);};};exports.passToN=passToN;var spreadTo=passToN;// ------ join, split etc.
exports.spreadTo=spreadTo;var join=dot1('join');exports.join=join;var split=dot1('split');exports.split=split;var flip=function flip(f){return function(a){return function(b){return f(b)(a);};};};exports.flip=flip;var flip3=function flip3(f){return function(a){return function(b){return function(c){return f(b)(a)(c);};};};};exports.flip3=flip3;var flip4=function flip4(f){return function(a){return function(b){return function(c){return function(d){return f(b)(a)(c)(d);};};};};};exports.flip4=flip4;var flip5=function flip5(f){return function(a){return function(b){return function(c){return function(d){return function(e){return f(b)(a)(c)(d)(e);};};};};};};// ------ sprintf
exports.flip5=flip5;var sprintf1=function sprintf1(str){return function(a){return sprintf(str,a);};};exports.sprintf1=sprintf1;var sprintfN=function sprintfN(str){return function(xs){return sprintf.apply(null,[str].concat((0,_toConsumableArray2.default)(xs)));};};// ------ repeat, times
exports.sprintfN=sprintfN;var repeatV=function repeatV(x){return function(n){var ret=[];for(var i=0;i<n;i++){ret.push(x);}return ret;};};exports.repeatV=repeatV;var repeatF=function repeatF(f){return function(n){var ret=[];for(var i=0;i<n;i++){ret.push(f(i));}return ret;};};exports.repeatF=repeatF;var repeatSide=function repeatSide(f){return function(n){for(var i=0;i<n;i++){f(i);}};};exports.repeatSide=repeatSide;var timesV=function timesV(n){return function(x){return repeatV(x)(n);};};exports.timesV=timesV;var timesF=function timesF(n){return function(f){return repeatF(f)(n);};};exports.timesF=timesF;var timesSide=function timesSide(n){return function(f){return repeatSide(f)(n);};};// ------ replace / match
exports.timesSide=timesSide;var ifReplace=function ifReplace(yes){return function(no){return function(re){return function(replArg){return function(target){var success=0;var repl=typeof replArg==='function'?function(){return++success,replArg.apply(void 0,arguments);}:function(_){return++success,replArg;};var out=target.replace(re,repl);return success?yes(out,success):no(target);};};};};};// --- by should be negative to count down.
exports.ifReplace=ifReplace;var rangeFromBy=function rangeFromBy(by){return function(from){return function(to){return from<to?rangeFromByAsc(by)(from)(to):from>to?rangeFromByDesc(by)(from)(to):[];};};};// --- no corresponding to version.
exports.rangeFromBy=rangeFromBy;var rangeFromByAsc=function rangeFromByAsc(by){return function(from){return function(to){var ret=[];for(var i=from;i<to;i+=by){ret.push(i);}return ret;};};};// --- no corresponding to version.
exports.rangeFromByAsc=rangeFromByAsc;var rangeFromByDesc=function rangeFromByDesc(by){return function(from){return function(to){var ret=[];for(var i=from;i>to;i+=by){ret.push(i);}return ret;};};};exports.rangeFromByDesc=rangeFromByDesc;var rangeToBy=function rangeToBy(by){return function(to){return function(from){return from<to?rangeFromByAsc(by)(from)(to):from>to?rangeFromByDesc(by)(from)(to):[];};};};// --------- regex.
// --- these deviate somewhat from the naming conventions: we're assuming you generally want to pipe
// the target to the match functions.
exports.rangeToBy=rangeToBy;var removeSpaces=/*#__PURE__*/dot2('replace')(/\s+/g)('');// --- input: regex.
var xRegExp=function xRegExp(re){return new RegExp(removeSpaces(re.source),re.flags);};// @todo curry
// --- beware, overwrites any flags that the re already had.
exports.xRegExp=xRegExp;var xRegExpFlags=function xRegExpFlags(re,flags){return new RegExp(removeSpaces(re.source),flags);};// @todo curry, check default flags.
// --- input: string.
exports.xRegExpFlags=xRegExpFlags;var xRegExpStr=function xRegExpStr(reStr){var flags=arguments.length>1&&arguments[1]!==undefined?arguments[1]:'';return lets(function(_){return removeSpaces(reStr);},function(_){return flags;},function(x,y){return neu2(RegExp)(x)(y);});};// --- not every function (currently) has a matching 'replace' version.
// @todo make xMatch incur only a compile-time cost.
exports.xRegExpStr=xRegExpStr;var neu1=function neu1(x){return function(val1){return new x(val1);};};exports.neu1=neu1;var neu2=function neu2(x){return function(val1){return function(val2){return new x(val1,val2);};};};exports.neu2=neu2;var neu3=function neu3(x){return function(val1){return function(val2){return function(val3){return new x(val1,val2,val3);};};};};exports.neu3=neu3;var neu4=function neu4(x){return function(val1){return function(val2){return function(val3){return function(val4){return new x(val1,val2,val3,val4);};};};};};exports.neu4=neu4;var neu5=function neu5(x){return function(val1){return function(val2){return function(val3){return function(val4){return function(val5){return new x(val1,val2,val3,val4,val5);};};};};};};exports.neu5=neu5;var neuN=function neuN(x){return function(vs){return(0,_construct2.default)(x,(0,_toConsumableArray2.default)(vs));};};exports.neuN=neuN;var match=function match(re){return function(target){return re.exec(target);};};exports.match=match;var xMatchGlobal=function xMatchGlobal(re){return function(mapper){return function(target){var out=[];var reGlobal=xRegExpFlags(re,'g');var m;while(m=reGlobal.exec(target)){appendToM(out)(mapper.apply(void 0,(0,_toConsumableArray2.default)(m)));}return out;};};};exports.xMatchGlobal=xMatchGlobal;var xMatch=function xMatch(re){return function(target){return xRegExp(re).exec(target);};};exports.xMatch=xMatch;var xMatchStr=function xMatchStr(reStr){return function(target){return xMatch(new RegExp(reStr))(target);};};exports.xMatchStr=xMatchStr;var xMatchStrFlags=function xMatchStrFlags(reStr){return function(flags){return function(target){return xMatch(new RegExp(reStr,flags))(target);};};};exports.xMatchStrFlags=xMatchStrFlags;var xReplace=function xReplace(re){return function(repl){return function(target){return target.replace(xRegExp(re),repl);};};};exports.xReplace=xReplace;var xReplaceStr=function xReplaceStr(reStr){return function(repl){return function(target){return target.replace(xRegExpStr(reStr),repl);};};};exports.xReplaceStr=xReplaceStr;var xReplaceStrFlags=function xReplaceStrFlags(reStr){return function(flags){return function(repl){return function(target){return target.replace(xRegExpStr(reStr,flags),repl);};};};};exports.xReplaceStrFlags=xReplaceStrFlags;var ifXReplace=function ifXReplace(re){return function(repl){return function(yes){return function(no){return function(target){return ifReplace(yes)(no)(xRegExp(re))(repl)(target);};};};};};exports.ifXReplace=ifXReplace;var ifXReplaceStr=function ifXReplaceStr(reStr){return function(repl){return function(yes){return function(no){return function(target){return ifReplace(yes)(no)(xRegExpStr(reStr))(repl)(target);};};};};};exports.ifXReplaceStr=ifXReplaceStr;var ifXReplaceStrFlags=function ifXReplaceStrFlags(reStr){return function(flags){return function(repl){return function(yes){return function(no){return function(target){return ifReplace(yes)(no)(xRegExpStr(reStr,flags))(repl)(target);};};};};};};/* The first arg can be either a function or an object. If it's a function, then it will be called
 * on each `create`. The object form avoids this extra call, but beware that if you use it then the
 * values should all be primitive types, or undefined or null. If they're reference types they will
 * be shared among all object instances and that's probably not what you want. You can initialise
 * the values during `init`. The optional object passed to `create` is assumed to only contain
 * string keys (symbol keys are ignored).
 */exports.ifXReplaceStrFlags=ifXReplaceStrFlags;var factoryProps=function factoryProps(propsArg){return function(factory){var orig=function orig(){return factory.create.apply(factory,arguments);};// @todo don't clone factory?
return _objectSpread(_objectSpread({},factory),{},{create:function create(args){var props=typeof propsArg==='function'?propsArg():propsArg;var o=orig(props);var src=args,tgt=o;for(var i in args){if(_internal.hasOwnProperty.call(src,i)&&ok(src[i]))tgt[i]=src[i];}return tgt;}});};};exports.factoryProps=factoryProps;var factoryInit=function factoryInit(init){return function(proto){return{proto:proto,create:function create(props){var o=Object.create(proto);init(o,props);return o;}};};};// --- alters the prototype by merging in the mixins.
// --- if a key exists in the proto and its value is not nil, then it is not overwritten.
// --- the result is as if you had merged the proto into the mixin, keeping the proto's own
// prototype chain intact, then called that result the proto; except that the proto is altered in
// place of course because of the M.
exports.factoryInit=factoryInit;var mixinPreM=function mixinPreM(mixin){return function(proto){var chooseTgtWhenOk=function chooseTgtWhenOk(src,tgt){return ok(tgt)?tgt:src;};var mergeInToChooseTgtWhenOkM=mergeWith(chooseTgtWhenOk)(mergeInToMSym);return mergeInToChooseTgtWhenOkM(proto)(mixin);};};// --- alters the prototype by merging in the mixins.
// --- if a key exists in the proto then it is overwritten, unless the corresponding value from the
//     mixin is nil.
exports.mixinPreM=mixinPreM;var mixinM=function mixinM(mixin){return function(proto){var srcOk=function srcOk(src,_){return ok(src);};var mergeInToWhenSrcOkM=mergeWhen(srcOk)(mergeInToMSym);return mergeInToWhenSrcOkM(proto)(mixin);};};exports.mixinM=mixinM;var mixinPreNM=function mixinPreNM(ms){return function(proto){return ms.reduce(function(protoAcc,mixin){return mixinPreM(mixin)(protoAcc);},proto);};};exports.mixinPreNM=mixinPreNM;var mixinNM=function mixinNM(ms){return function(proto){return ms.reduce(function(protoAcc,mixin){return mixinM(mixin)(protoAcc);},proto);};};// ------ all/any.
// --- no point in using an N marker, because non-n is only possible once you know the predicate.
// ------ these all return truthy values instead of strict booleans; rationale: it's trivial to
// convert to a boolean afterwards using for example `| Boolean` or `>> Boolean`, but not the other
// way around.
// --- returns the return value of the last function, if they all return truthy, otherwise `false`.
exports.mixinNM=mixinNM;var againstAll=function againstAll(fs){return function(x){var y=false;var _iterator13=_createForOfIteratorHelper(fs),_step13;try{for(_iterator13.s();!(_step13=_iterator13.n()).done;){var f=_step13.value;y=f(x);if(!y)return false;}}catch(err){_iterator13.e(err);}finally{_iterator13.f();}return y;};};// --- returns the return value of the first function which returns truthy, otherwise `false`.
exports.againstAll=againstAll;var againstAny=function againstAny(fs){return function(x){var y;var _iterator14=_createForOfIteratorHelper(fs),_step14;try{for(_iterator14.s();!(_step14=_iterator14.n()).done;){var f=_step14.value;y=f(x);if(y)return y;}}catch(err){_iterator14.e(err);}finally{_iterator14.f();}return false;};};// --- returns the return value of the last function if they are all truthy, otherwise `false`.
exports.againstAny=againstAny;var allAgainst=function allAgainst(f){return function(xs){var y=false;var _iterator15=_createForOfIteratorHelper(xs),_step15;try{for(_iterator15.s();!(_step15=_iterator15.n()).done;){var x=_step15.value;y=f(x);if(!y)return false;}}catch(err){_iterator15.e(err);}finally{_iterator15.f();}return y;};};// --- returns the return value of the first function which returns truthy, otherwise `false`.
exports.allAgainst=allAgainst;var anyAgainst=function anyAgainst(f){return function(xs){var y;var _iterator16=_createForOfIteratorHelper(xs),_step16;try{for(_iterator16.s();!(_step16=_iterator16.n()).done;){var x=_step16.value;y=f(x);if(y)return y;}}catch(err){_iterator16.e(err);}finally{_iterator16.f();}return false;};};exports.anyAgainst=anyAgainst;var againstBoth=function againstBoth(f){return function(g){return function(x){return f(x)&&g(x);};};};exports.againstBoth=againstBoth;var againstEither=function againstEither(f){return function(g){return function(x){return f(x)||g(x);};};};// @todo equiv: like R.equals
// @todo pop, popFrom, popM, popFromM, also shift.
/*
 * Usage:
 *   const x = { some: 2, vals: 10, }
 *   x | deconstruct ({ some, vals, }) => {
 *     ...
 *   }
 *   x | deconstruct ({ some, vals, }, orig) => {
 *     ...
 *   }
 */exports.againstEither=againstEither;var deconstruct=function deconstruct(f){return function(o){return f(o,o);};};/*
 * @experimental: need better name.
 *
 * Useful for when you want to deconstruct some arguments, but also keep a reference to the
 * original:
 *
 * Usage:
 *
 *   const x = { some: 3, vals: 4, }
 *   x | deconstruct2 (({ some, vals, }) => merge ({
 *     new: some + 2,
 *     extra: vals + 3,
 *   }) // => { some: 3, vals: 4, new: 5, extra: 7, }
 *
 * Or say you have a redux reducer function like:
 *
 *   [CONSTANT]: (actionPayload) => (state) => newState
 *
 * where the newState depends on a value in the state:
 *
 *   [INCREASE_COUNTER]: ({ data: increaseBy, }) => deconstruct2 (
 *     ({ counter, }) => state => state | merge ({
 *       counter: counter + increaseBy,
 *     }),
 *   ),
 *
 * or:
 *
 *   [INCREASE_COUNTER]: ({ data: increaseBy, }) => deconstruct2 (
 *     ({ counter, }) => merge ({
 *       counter: counter + increaseBy,
 *     }),
 *   ),
 */exports.deconstruct=deconstruct;var deconstruct2=function deconstruct2(f){return function(o){return f(o)(o);};};/*
 * Deconstruct an array of values:
 *
 * Usage, e.g. in a React component method:
 *   componentDidMount = this | deconstruct (
 *     ({ props, state, }) => [props, state] | deconstructN (
 *       ({ prop1, prop2, }, { state1, state2, }) => ...
 *     )
 *   )
 */exports.deconstruct2=deconstruct2;var deconstructN=function deconstructN(f){return function(xs){return f.apply(void 0,(0,_toConsumableArray2.default)(xs));};};exports.deconstructN=deconstructN;var and=function and(y){return function(x){return x&&y;};};exports.and=and;var or=function or(y){return function(x){return x||y;};};/* export const andNot = not >> and
 * export const orNot  = not >> or
 */exports.or=or;var andNot=function andNot(y){return function(x){return x&&!y;};};exports.andNot=andNot;var orNot=function orNot(y){return function(x){return x||!y;};};exports.orNot=orNot;