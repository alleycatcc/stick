// --- we don't want @babel/env at all (any more) for the es build -- we want as few transformations
// as possible (e.g. const to var) so that tree shaking works.

const presets = {
  es: [],
  cjs: [[
    '@babel/env',
    {
      modules: 'commonjs',
      targets: {
        ie: 11,
      },
    },
  ]],
}

const config = (env) => ({
  presets: presets [env],
  plugins: [
    // --- reduce code size and avoid namespace pollution (e.g. global
    // polyfills; be sure to add @babel/runtime to runtime deps)
    '@babel/transform-runtime',
  ],
})

module.exports = (api) => {
  api.cache.forever ()
  return {
    env: {
      // --- keyed on BABEL_ENV
      es: config ('es'),
      cjs: config ('cjs'),
    },
  }
}
