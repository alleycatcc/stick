#!/bin/bash

set -eu
set -o pipefail

bindir=$(realpath "$(dirname "$0")")

. "$bindir"/functions.bash
. "$bindir"/vars

USAGE="Usage: $0 [-w | --watch] [-p | --publish]"

opt_w=
opt_p=
while getopts hpw-: arg; do
    case $arg in
        h) warn "$USAGE"; exit 0 ;;
        w) opt_w=yes ;;
        p) opt_p=yes ;;
        -) OPTARG_VALUE="${OPTARG#*=}"
            case $OPTARG in
                help)  warn "$USAGE"; exit 0 ;;
                watch)  opt_w=yes ;;
                publish)  opt_p=yes ;;
                '')    break ;;
                *)     error "Illegal option --$OPTARG" ;;
                esac ;;
        *) error "$USAGE" ;;
    esac
done
shift $((OPTIND-1))

npmfiles=(
    curry.js
    index.js
    manual.js
    map.js
    mapManual.js
    operator.js
)

build () {
    local watch
    local opt
    watch="${1:-}"
    if [ "$watch" = yes ]; then opt=-wd;
    else opt=-d
    fi

    xport BABEL_ENV cjs
    cmd "$nodebindir"/babel --no-comments --compact true "$opt" "$cjslibdir" "$srcdir"
    if [ "$watch" = yes ]; then
        return
    fi
    xport BABEL_ENV es
    cmd "$nodebindir"/babel --no-comments --compact true "$opt" "$eslibdir" "$srcdir"
}

# --- only builds cjs.
build-watch () {
    fun build yes
}

copy () {
    local path
    path="$1"
    cpa "$cjslibdir"/"$path" "$rootdir"
}

copy-files () {
    local i
    for i in "${npmfiles[@]}"; do
        fun copy "$i"
    done
}

publish () {
    cmd npm publish
}

if [ "$opt_w" = yes ]; then
    fun build-watch
    error "watch not implemented"
else
    fun build
    info 'preparing npm package'
    fun copy-files
    if [ "$opt_p" = yes ]; then fun publish; fi
fi
