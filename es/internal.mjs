const{toString:oStr,hasOwnProperty,propertyIsEnumerable}={};export{oStr};export{hasOwnProperty};export{propertyIsEnumerable};// --- an object with a null prototype, which can therefore not be attacked through prototype
// pollution.
export const safeObject=()=>Object.create(null);export default null;