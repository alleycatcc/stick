// --- see index.js for docs.
export const roll=f=>(...args)=>{let g=f;for(const i of args)g=g(i);return g;};/* A surprising result is that `recurry` actually works fine if `n` is exactly one smaller than the
 * necessary arity.
 * So, this check is not strictly necessary, but is intended to avoid confusion down the road.
 */const check_dn=(dn,f)=>{if(dn>=0)return;throw new Error(`recurry: arity error (perhaps you should call \`recurry\` `+`with a higher value for \`n\`; \`f\` was `+f.toString());};export const recurry=n=>f=>(...args)=>{const rolled=roll(f)(...args);const dn=n-args.length;check_dn(dn,f);return dn<1?rolled:recurry(dn)(rolled);};