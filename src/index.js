import { recurry, roll, } from './curry.js'
import { hasOwnProperty, safeObject, } from './internal.js'

import {
  add as manualAdd,
  addCollection2 as manualAddCollection2,
  addCollection as manualAddCollection,
  addIndex2 as manualAddIndex2,
  addIndex as manualAddIndex,
  againstAll as manualAgainstAll,
  againstAny as manualAgainstAny,
  againstBoth as manualAgainstBoth,
  againstEither as manualAgainstEither,
  allAgainst as manualAllAgainst,
  always as manualAlways,
  ampersandN as manualAmpersandN,
  and as manualAnd,
  andNot as manualAndNot,
  anyAgainst as manualAnyAgainst,
  append as manualAppend,
  appendM as manualAppendM,
  appendTo as manualAppendTo,
  appendToM as manualAppendToM,
  applyTo1 as manualApplyTo1,
  applyTo2 as manualApplyTo2,
  applyTo3 as manualApplyTo3,
  applyTo4 as manualApplyTo4,
  applyTo5 as manualApplyTo5,
  applyToN as manualApplyToN,
  assoc as manualAssoc,
  assocM as manualAssocM,
  assocPath as manualAssocPath,
  assocPathM as manualAssocPathM,
  asterisk1 as manualAsterisk1,
  asterisk2 as manualAsterisk2,
  asterisk3 as manualAsterisk3,
  asterisk4 as manualAsterisk4,
  asterisk5 as manualAsterisk5,
  asteriskN as manualAsteriskN,
  bind as manualBind,
  bindLateProp as manualBindLateProp,
  bindLatePropTo as manualBindLatePropTo,
  bindProp as manualBindProp,
  bindPropTo as manualBindPropTo,
  bindTo as manualBindTo,
  bindTry as manualBindTry,
  bindTryProp as manualBindTryProp,
  bindTryPropTo as manualBindTryPropTo,
  bindTryTo as manualBindTryTo,
  callOn1 as manualCallOn1,
  callOn2 as manualCallOn2,
  callOn3 as manualCallOn3,
  callOn4 as manualCallOn4,
  callOn5 as manualCallOn5,
  callOn as manualCallOn,
  callOnN as manualCallOnN,
  compose as manualCompose,
  composeAsMethods as manualComposeAsMethods,
  composeAsMethodsRight as manualComposeAsMethodsRight,
  composeRight as manualComposeRight,
  concat as manualConcat,
  concatM as manualConcatM,
  concatTo as manualConcatTo,
  concatToM as manualConcatToM,
  cond as manualCond,
  condPredicate as manualCondPredicate,
  condS as manualCondS,
  contains as manualContains,
  containsV as manualContainsV,
  deconstruct2 as manualDeconstruct2,
  deconstruct as manualDeconstruct,
  deconstructN as manualDeconstructN,
  defaultTo as manualDefaultTo,
  die as manualDie,
  divideBy as manualDivideBy,
  divideInto as manualDivideInto,
  dot1 as manualDot1,
  dot2 as manualDot2,
  dot3 as manualDot3,
  dot4 as manualDot4,
  dot5 as manualDot5,
  dot as manualDot,
  dotN as manualDotN,
  drop as manualDrop,
  each as manualEach,
  eachObj as manualEachObj,
  eachObjIn as manualEachObjIn,
  eq as manualEq,
  exception as manualException,
  factoryInit as manualFactoryInit,
  factoryProps as manualFactoryProps,
  F as manualF,
  filter as manualFilter,
  find as manualFind,
  findIndex as manualFindIndex,
  findWithIndex as manualFindWithIndex,
  flip3 as manualFlip3,
  flip4 as manualFlip4,
  flip5 as manualFlip5,
  flip as manualFlip,
  getType as manualGetType,
  gt as manualGt,
  gte as manualGte,
  has as manualHas,
  hasIn as manualHasIn,
  ifAlways as manualIfAlways,
  ifBind as manualIfBind,
  ifHas as manualIfHas,
  ifHasIn as manualIfHasIn,
  ifPredicate as manualIfPredicate,
  ifPredicateResults as manualIfPredicateResults,
  ifPredicateV as manualIfPredicateV,
  ifPredicateWithResults as manualIfPredicateWithResults,
  ifReplace as manualIfReplace,
  ifXReplace as manualIfXReplace,
  ifXReplaceStr as manualIfXReplaceStr,
  ifXReplaceStrFlags as manualIfXReplaceStrFlags,
  invoke as manualInvoke,
  isArray as manualIsArray,
  isBoolean as manualIsBoolean,
  isFalse as manualIsFalse,
  isFunction as manualIsFunction,
  isNo as manualIsNo,
  isNumber as manualIsNumber,
  isObject as manualIsObject,
  isRegExp as manualIsRegExp,
  isString as manualIsString,
  isSymbol as manualIsSymbol,
  isTrue as manualIsTrue,
  isType as manualIsType,
  isYes as manualIsYes,
  join as manualJoin,
  letN as manualLetN,
  letNV as manualLetNV,
  lets1 as manualLets1,
  lets2 as manualLets2,
  lets3 as manualLets3,
  lets4 as manualLets4,
  lets5 as manualLets5,
  lets6 as manualLets6,
  lets as manualLets,
  letS as manualLetS,
  letV as manualLetV,
  lt as manualLt,
  lte as manualLte,
  map as manualMap,
  match as manualMatch,
  mergeAllIn as manualMergeAllIn,
  merge as manualMerge,
  mergeIn as manualMergeIn,
  mergeInM as manualMergeInM,
  mergeInMSym as manualMergeInMSym,
  mergeInSym as manualMergeInSym,
  mergeInTo as manualMergeInTo,
  mergeInToM as manualMergeInToM,
  mergeInToMSym as manualMergeInToMSym,
  mergeInToSym as manualMergeInToSym,
  mergeM as manualMergeM,
  mergeMSym as manualMergeMSym,
  mergeSym as manualMergeSym,
  mergeTo as manualMergeTo,
  mergeToM as manualMergeToM,
  mergeToMSym as manualMergeToMSym,
  mergeToSym as manualMergeToSym,
  mergeWhen as manualMergeWhen,
  mergeWith as manualMergeWith,
  mixinM as manualMixinM,
  mixinNM as manualMixinNM,
  mixinPreM as manualMixinPreM,
  mixinPreNM as manualMixinPreNM,
  modulo as manualModulo,
  moduloWholePart as manualModuloWholePart,
  multiply as manualMultiply,
  ne as manualNe,
  neu1 as manualNeu1,
  neu2 as manualNeu2,
  neu3 as manualNeu3,
  neu4 as manualNeu4,
  neu5 as manualNeu5,
  neuN as manualNeuN,
  noop as manualNoop,
  not as manualNot,
  notOk as manualNotOk,
  ok as manualOk,
  or as manualOr,
  orNot as manualOrNot,
  passTo as manualPassTo,
  passToN as manualPassToN,
  path as manualPath,
  pathOf as manualPathOf,
  pipe as manualPipe,
  prepend as manualPrepend,
  prependM as manualPrependM,
  prependTo as manualPrependTo,
  prependToM as manualPrependToM,
  prop as manualProp,
  propOf as manualPropOf,
  provideTo1 as manualProvideTo1,
  provideTo2 as manualProvideTo2,
  provideTo3 as manualProvideTo3,
  provideTo4 as manualProvideTo4,
  provideTo5 as manualProvideTo5,
  provideTo as manualProvideTo,
  provideToN as manualProvideToN,
  raise as manualRaise,
  rangeFromByAsc as manualRangeFromByAsc,
  rangeFromBy as manualRangeFromBy,
  rangeFromByDesc as manualRangeFromByDesc,
  rangeToBy as manualRangeToBy,
  reduceAbort as manualReduceAbort,
  reduce as manualReduce,
  reduceObj as manualReduceObj,
  reduceObjIn as manualReduceObjIn,
  reduceRight as manualReduceRight,
  reduceRightC as manualReduceRightC,
  reject as manualReject,
  repeatF as manualRepeatF,
  repeatSide as manualRepeatSide,
  repeatV as manualRepeatV,
  side1 as manualSide1,
  side2 as manualSide2,
  side3 as manualSide3,
  side4 as manualSide4,
  side5 as manualSide5,
  side as manualSide,
  sideN as manualSideN,
  split as manualSplit,
  spreadTo as manualSpreadTo,
  sprintf1 as manualSprintf1,
  sprintfN as manualSprintfN,
  subtract as manualSubtract,
  subtractFrom as manualSubtractFrom,
  take as manualTake,
  tap as manualTap,
  T as manualT,
  timesF as manualTimesF,
  timesSide as manualTimesSide,
  timesV as manualTimesV,
  toThe as manualToThe,
  tryCatch as manualTryCatch,
  update as manualUpdate,
  updateM as manualUpdateM,
  updatePath as manualUpdatePath,
  updatePathM as manualUpdatePathM,
  whenAlways as manualWhenAlways,
  whenBind as manualWhenBind,
  whenHas as manualWhenHas,
  whenHasIn as manualWhenHasIn,
  whenPredicate as manualWhenPredicate,
  whenPredicateResults as manualWhenPredicateResults,
  whenPredicateV as manualWhenPredicateV,
  whenPredicateWithResults as manualWhenPredicateWithResults,
  xMatch as manualXMatch,
  xMatchGlobal as manualXMatchGlobal,
  xMatchStr as manualXMatchStr,
  xMatchStrFlags as manualXMatchStrFlags,
  xRegExp as manualXRegExp,
  xRegExpFlags as manualXRegExpFlags,
  xRegExpStr as manualXRegExpStr,
  xReplace as manualXReplace,
  xReplaceStr as manualXReplaceStr,
  xReplaceStrFlags as manualXReplaceStrFlags,
} from './manual.js'

import {
  bitwiseNot as operatorBitwiseNot,
  bitwiseAnd as operatorBitwiseAnd,
  bitwiseOr as operatorBitwiseOr,
  bitwiseXor as operatorBitwiseXor,
  bitwiseLeft as operatorBitwiseLeft,
  bitwiseRight as operatorBitwiseRight,
  bitwiseRightZeroFill as operatorBitwiseRightZeroFill,
  bitwiseLeftBy as operatorBitwiseLeftBy,
  bitwiseRightBy as operatorBitwiseRightBy,
  bitwiseRightZeroFillBy as operatorBitwiseRightZeroFillBy,
} from './operator.js'

import {
  withFilter as mapWithFilter,
  remapKeys as mapRemapKeys,
  remapValues as mapRemapValues,
  remapTuples as mapRemapTuples,
  mapKeys as mapMapKeys,
  mapValues as mapMapValues,
  mapTuples as mapMapTuples,
  fromPairs as mapFromPairs,
  toPairs as mapToPairs,
} from './map.js'

export const pipe = manualPipe
export const composeRight = manualComposeRight
export const compose = manualCompose
export const composeAsMethodsRight = manualComposeAsMethodsRight
export const composeAsMethods = manualComposeAsMethods

// @todo composeAsMany(Left/Right), composeAsMany(Left/Right)N

/*
 * Similar to `ramda.uncurryN`, but faster, as it has fewer features.
 *
 * We use this function internally; a more useful version for most cases is `recurry`.
 *
 * Takes a manually curried function like
 *
 *   f = a => b => ... => z => body
 *
 * And produces a new function `g`, which can be called like
 *
 *   g (a, b, ..., z)
 *
 * Caveats:
 *
 * `g` is curried, but only if you use the manual style:
 *
 *   g (1) (2) (3)
 *
 * and not
 *
 *   g (1, 2) (3)
 *
 * `g` does not have a well-defined arity, i.e., `g.length` is not useful. If this is a problem,
 * consider using `ramda.uncurryN`, which (despite the name) results in a curried function which can
 * be called using either style and which also has a well-defined arity.
 */

export { roll, }

/*
 * Takes a manually curried function and allows it to be called using either of the two calling
 * styles.
 *
 *   const add = recurry (3) (
 *     (x) => (y) => (z) => x + y + z,
 *   )
 *   add (1) (2) (3) = 6
 *   add (1, 2) (3) = 6
 *   add (1, 2, 3) = 6
 *
 * Must be called using the manual style.
 *
 * As with `roll`, the recurried function does not have a well-defined arity.
 */

export { recurry, }

/*
 * @experimental
 *
 * Identical to `recurry`, but can be called with either style.
 *   const add = recurry2 (3) (x => y => z => x + y + z)
 *   const add = recurry2 (3, x => y => z => x + y + z)
 *
 * This is accomplished by calling `recurry` on itself.
 */

export const recurry2 = recurry (2) (recurry)

/* The implementation of `recurry` used in this module; currently identical to
 * `recurry`.
*/
const _recurry = recurry

export const blush = manualAlways
export const always = manualAlways

// --- different from R.equals, which considers two different objects equal if their contents are
//     the same (equivalent).
// --- different from R.identical, which has some different semantics involving e.g. 0 and -0.
// --- literally just wraps ===.
// rationale: must be able to confidently refactor working code which uses ===

export const eq  = /*#__PURE__*/ _recurry (2) (manualEq)
export const ne  = /*#__PURE__*/ _recurry (2) (manualNe)
export const gt  = /*#__PURE__*/ _recurry (2) (manualGt)
export const gte = /*#__PURE__*/ _recurry (2) (manualGte)
export const lt  = /*#__PURE__*/ _recurry (2) (manualLt)
export const lte = /*#__PURE__*/ _recurry (2) (manualLte)

export const id  = x => x
export const tap = /*#__PURE__*/ _recurry (2) (manualTap)

export const dot  = /*#__PURE__*/ _recurry (2) (manualDot)
export const dot1 = /*#__PURE__*/ _recurry (3) (manualDot1)
export const dot2 = /*#__PURE__*/ _recurry (4) (manualDot2)
export const dot3 = /*#__PURE__*/ _recurry (5) (manualDot3)
export const dot4 = /*#__PURE__*/ _recurry (6) (manualDot4)
export const dot5 = /*#__PURE__*/ _recurry (7) (manualDot5)
export const dotN = /*#__PURE__*/ _recurry (3) (manualDotN)

export const side  = /*#__PURE__*/ _recurry (2) (manualSide)
export const side1 = /*#__PURE__*/ _recurry (3) (manualSide1)
export const side2 = /*#__PURE__*/ _recurry (4) (manualSide2)
export const side3 = /*#__PURE__*/ _recurry (5) (manualSide3)
export const side4 = /*#__PURE__*/ _recurry (6) (manualSide4)
export const side5 = /*#__PURE__*/ _recurry (7) (manualSide5)
export const sideN = /*#__PURE__*/ _recurry (3) (manualSideN)

/* Note that something like ifNotPredicate would be confusing:
 * should it match falsy or false? If falsy, it breaks symmetry with ifPredicate; if
 * false, it behaves differently than ifPredicate (pred >> not), which is also confusing.
 */

export const isTrue = manualIsTrue
export const isFalse = manualIsFalse
export const isYes = manualIsYes
export const isNo = manualIsNo
export const isTruthy = isYes
export const isFalsy  = isNo

export const ifPredicate   = /*#__PURE__*/ _recurry (4) (manualIfPredicate)
export const whenPredicate = /*#__PURE__*/ _recurry (3) (manualWhenPredicate)

export const ifPredicateResults = /*#__PURE__*/ _recurry (4) (manualIfPredicateResults)
export const whenPredicateResults = /*#__PURE__*/ _recurry (3) (manualWhenPredicateResults)
export const ifPredicateWithResults = /*#__PURE__*/ _recurry (4) (manualIfPredicateWithResults)
export const whenPredicateWithResults = /*#__PURE__*/ _recurry (3) (manualWhenPredicateWithResults)

export const ifPredicateV = /*#__PURE__*/ _recurry (4) (manualIfPredicateV)
export const whenPredicateV = /*#__PURE__*/ _recurry (3) (manualWhenPredicateV)

// --- same arity as ifPredicate / whenPredicate, because of composeRight.
export const ifAlways      = /*#__PURE__*/ _recurry (4) (manualIfAlways)
export const whenAlways    = /*#__PURE__*/ _recurry (3) (manualWhenAlways)

export const ok    = /*#__PURE__*/ manualOk
export const notOk = /*#__PURE__*/ manualNotOk
export const not   = /*#__PURE__*/ manualNot
export const nil   = /*#__PURE__*/ notOk
export const noop  = /*#__PURE__*/ manualNoop

// --- we do these here instead of in manual so we don't have to recurry them all
// (we use our (recurried) versions of ifPredicate/whenPredicate).

export const ifOk          = /*#__PURE__*/ ifPredicate   (ok)
export const whenOk        = /*#__PURE__*/ whenPredicate (ok)
export const ifNotOk       = /*#__PURE__*/ ifPredicate   (notOk)
export const whenNotOk     = /*#__PURE__*/ whenPredicate (notOk)
export const ifTrue        = /*#__PURE__*/ ifPredicate   (isTrue)
export const whenTrue      = /*#__PURE__*/ whenPredicate (isTrue)
export const ifFalse       = /*#__PURE__*/ ifPredicate   (isFalse)
export const whenFalse     = /*#__PURE__*/ whenPredicate (isFalse)
export const ifYes         = /*#__PURE__*/ ifPredicate   (isYes)
export const whenYes       = /*#__PURE__*/ whenPredicate (isYes)
export const ifNo          = /*#__PURE__*/ ifPredicate   (isNo)
export const whenNo        = /*#__PURE__*/ whenPredicate (isNo)

export const ifNil         = /*#__PURE__*/ ifNotOk
export const whenNil       = /*#__PURE__*/ whenNotOk
export const ifTruthy      = /*#__PURE__*/ ifYes
export const whenTruthy    = /*#__PURE__*/ whenYes
export const ifFalsy       = /*#__PURE__*/ ifNo
export const whenFalsy     = /*#__PURE__*/ whenNo

// --- these have a different calling convention, so their names are a bit misleading based on the
// above pattern.
export const ifHas     = /*#__PURE__*/ _recurry (3) (manualIfHas)
export const whenHas   = /*#__PURE__*/ _recurry (2) (manualWhenHas)
export const ifHasIn   = /*#__PURE__*/ _recurry (3) (manualIfHasIn)
export const whenHasIn = /*#__PURE__*/ _recurry (2) (manualWhenHasIn)

export const ifBind    = /*#__PURE__*/ _recurry (3) (manualIfBind)
export const whenBind  = /*#__PURE__*/ _recurry (2) (manualWhenBind)

export const cond = /*#__PURE__*/ manualCond
export const condN = /*#__PURE__*/ (blocks) => cond (...blocks)
export const condS = /*#__PURE__*/ _recurry (2) (manualCondS)

// ------ exceptions.

export const exception         = /*#__PURE__*/ manualException
export const raise             = /*#__PURE__*/ manualRaise
export const tryCatch          = /*#__PURE__*/ _recurry (3) (manualTryCatch)
export const die               = /*#__PURE__*/ manualDie

// ------ cascade

export const cascade = /*#__PURE__*/ (val, ...fxs) => fxs
  .reduce ((a, b) => b (a), val)

// --------- data.

// ------ defaultTo.

export const defaultTo = /*#__PURE__*/ _recurry (2) (manualDefaultTo)

// ------ join, split etc.
export const split  = /*#__PURE__*/ _recurry (2) (manualSplit)
export const join   = /*#__PURE__*/ _recurry (2) (manualJoin)

// ------ objects.

export const has   = /*#__PURE__*/ _recurry (2) (manualHas)
export const hasIn = /*#__PURE__*/ _recurry (2) (manualHasIn)

export const prop   = /*#__PURE__*/ _recurry (2) (manualProp)
export const propOf = /*#__PURE__*/ _recurry (2) (manualPropOf)

// --- only traverses "typeof = 'object'" nodes; thinks like Date result in undefined.
export const path       = /*#__PURE__*/ _recurry (2) (manualPath)
export const pathOf     = /*#__PURE__*/ _recurry (2) (manualPathOf)
export const assoc      = /*#__PURE__*/ _recurry (3) (manualAssoc)
export const assocM     = /*#__PURE__*/ _recurry (3) (manualAssocM)
export const assocPath  = /*#__PURE__*/ _recurry (3) (manualAssocPath)
export const assocPathM = /*#__PURE__*/ _recurry (3) (manualAssocPathM)

export const updateM     = /*#__PURE__*/ _recurry (3) (manualUpdateM)
export const update      = /*#__PURE__*/ _recurry (3) (manualUpdate)
export const updatePathM = /*#__PURE__*/ _recurry (3) (manualUpdatePathM)
export const updatePath  = /*#__PURE__*/ _recurry (3) (manualUpdatePath)

// ------ append.

// --- 4 | appendTo ([1, 2, 3])
// --- ([1, 2, 3]) | append (4)

export const append    = /*#__PURE__*/ _recurry (2) (manualAppend)
export const appendTo  = /*#__PURE__*/ _recurry (2) (manualAppendTo)
export const appendM   = /*#__PURE__*/ _recurry (2) (manualAppendM)
export const appendToM = /*#__PURE__*/ _recurry (2) (manualAppendToM)

// ------ prepend.

// --- 1 | prependTo ([2, 3, 4])
// --- ([2, 3, 4]) | prepend (1)
export const prependTo  = /*#__PURE__*/ _recurry (2) (manualPrependTo)
export const prepend    = /*#__PURE__*/ _recurry (2) (manualPrepend)
export const prependM   = /*#__PURE__*/ _recurry (2) (manualPrependM)
export const prependToM = /*#__PURE__*/ _recurry (2) (manualPrependToM)

// --- arrays or strings
// --- ramda's concat does more type checking and also allows fantasy land semigroups.
// --- [4] | concatTo ([1, 2, 3])
// --- [1, 2, 3] | concat ([4])
// --- [1, 2, 3] | concat ([4])
export const concatTo = /*#__PURE__*/ _recurry (2) (manualConcatTo)
export const concat   = /*#__PURE__*/ _recurry (2) (manualConcat)
export const precatTo = /*#__PURE__*/ concat
export const precat   = /*#__PURE__*/ concatTo

// --- only arrays (strings will throw)
export const concatToM = /*#__PURE__*/ _recurry (2) (manualConcatToM)
export const concatM   = /*#__PURE__*/ _recurry (2) (manualConcatM)

// --- own properties, including null/undefined.
// --- 2x faster than Object.assign.
// --- @todo: why is it so much faster?
// --- reminder: Object.assign and {...} only take own values.

// --- { b: 2 } | mergeTo ({ a: 1, b: null })
// --- ({ a: 1, b: null }) | merge ({ b: 2 })
// --- ({ a: 1, b: null }) | merge     ({ b: 2 })

// ---- these are the eight basis functions.
// they are not composed using our 'decorator' pattern, and the implementation is as fast (and ugly)
// possible.
export const mergeTo  = /*#__PURE__*/ _recurry (2) (manualMergeTo)
export const merge    = /*#__PURE__*/ _recurry (2) (manualMerge)

export const mergeM   = /*#__PURE__*/ _recurry (2) (manualMergeM)
export const mergeToM = /*#__PURE__*/ _recurry (2) (manualMergeToM)

// --- all enumerable properties (non-own and own) on the src will be copied to the tgt.
export const mergeInToM = /*#__PURE__*/ _recurry (2) (manualMergeInToM)
export const mergeInM   = /*#__PURE__*/ _recurry (2) (manualMergeInM)
// /---

// --- all enumerable properties (non-own and own) on both the src and tgt will be copied to the new
// object.
export const mergeIn = /*#__PURE__*/ _recurry (2) (manualMergeIn)
export const mergeInTo = /*#__PURE__*/ _recurry (2) (manualMergeInTo)
// /---

// /---- basis


// --- 'when' forms run the predicate on both the src and tgt, testing for truthiness.
export const mergeWhen    = /*#__PURE__*/ _recurry (3) (manualMergeWhen)

// --- the 'own'-ness ('in') of the merge function will take effect on both tgt & src
// -- not possible to mix and match.
export const mergeWith = /*#__PURE__*/ _recurry (4) (manualMergeWith)

export const mergeAllIn = manualMergeAllIn

export const mergeToMSym   = manualMergeToMSym
export const mergeToSym    = manualMergeToSym
export const mergeMSym     = manualMergeMSym
export const mergeSym      = manualMergeSym
export const mergeInToMSym = manualMergeInToMSym
export const mergeInToSym  = manualMergeInToSym
export const mergeInMSym   = manualMergeInMSym
export const mergeInSym    = manualMergeInSym

// ------ map.

// --- simple dispatches to Array.prototype functions, but capped.

export const map    = /*#__PURE__*/ _recurry (2) (manualMap)
export const each   = /*#__PURE__*/ _recurry (2) (manualEach)

export const reduce       = /*#__PURE__*/ _recurry (3) (manualReduce)
export const reduceRight  = /*#__PURE__*/ _recurry (3) (manualReduceRight)
export const reduceRightC = /*#__PURE__*/ _recurry (3) (manualReduceRightC)
export const reduceAbort  = /*#__PURE__*/ _recurry (4) (manualReduceAbort)

export const filter        = /*#__PURE__*/ _recurry (2) (manualFilter)
export const reject        = /*#__PURE__*/ _recurry (2) (manualReject)
export const find          = /*#__PURE__*/ _recurry (2) (manualFind)
export const findIndex     = /*#__PURE__*/ _recurry (2) (manualFindIndex)
export const contains      = /*#__PURE__*/ _recurry (2) (manualContains)
export const containsV     = /*#__PURE__*/ _recurry (2) (manualContainsV)
export const findWithIndex = /*#__PURE__*/ _recurry (2) (manualFindWithIndex)

export const reverseM = /*#__PURE__*/ dot ('reverse')
export const reverse = /*#__PURE__*/ xs => reduceRight ((x, acc) => (acc.push (x), acc), [], xs)

// --- undef on empty array, like ramda
export const last = xs => xs [xs.length - 1]
// --- undef on empty array, like ramda
export const head = xs => xs [0]
export const tail = xs => xs.slice (1)

export const drop = /*#__PURE__*/ _recurry (2) (manualDrop)
export const take = /*#__PURE__*/ _recurry (2) (manualTake)

export const addIndex       = /*#__PURE__*/ _recurry (2) (manualAddIndex)
export const addCollection  = /*#__PURE__*/ _recurry (3) (manualAddCollection)
export const addIndex2      = /*#__PURE__*/ _recurry (3) (manualAddIndex2)
export const addCollection2 = /*#__PURE__*/ _recurry (4) (manualAddCollection2)

export const eachObj = /*#__PURE__*/ _recurry (2) (manualEachObj)
export const eachObjIn = /*#__PURE__*/ _recurry (2) (manualEachObjIn)

export const reduceObj = /*#__PURE__*/ _recurry (3) (manualReduceObj)
export const reduceObjIn = /*#__PURE__*/ _recurry (3) (manualReduceObjIn)

// --- fs `ampersand` x = map map' fs where map' f = f x
export const ampersandN = /*#__PURE__*/ _recurry (2) (manualAmpersandN)

export const asterisk1 = /*#__PURE__*/ _recurry (2)  (manualAsterisk1)
export const asterisk2 = /*#__PURE__*/ _recurry (4)  (manualAsterisk2)
export const asterisk3 = /*#__PURE__*/ _recurry (6)  (manualAsterisk3)
export const asterisk4 = /*#__PURE__*/ _recurry (8)  (manualAsterisk4)
export const asterisk5 = /*#__PURE__*/ _recurry (10) (manualAsterisk5)
export const asteriskN = /*#__PURE__*/ _recurry (2)  (manualAsteriskN)

// --- 'call' and 'provide' always mean pass a context.
// --- 'apply' always means 'apply this function to some params'
// --- 'pass' means 'pass these params to a function'
// --- 'invoke' means just call this function, no context or params.

// ------ ; {}.toString | callOn ([])

export const callOn = /*#__PURE__*/ _recurry (2) (manualCallOn)
export const callOn1 = /*#__PURE__*/ _recurry (3) (manualCallOn1)
export const callOn2 = /*#__PURE__*/ _recurry (4) (manualCallOn2)
export const callOn3 = /*#__PURE__*/ _recurry (5) (manualCallOn3)
export const callOn4 = /*#__PURE__*/ _recurry (6) (manualCallOn4)
export const callOn5 = /*#__PURE__*/ _recurry (7) (manualCallOn5)
export const callOnN = /*#__PURE__*/ _recurry (3) (manualCallOnN)

// ------ ; [] | provideTo ({}.toString)

export const provideTo = /*#__PURE__*/ _recurry (2) (manualProvideTo)
export const provideTo1 = /*#__PURE__*/ _recurry (3) (manualProvideTo1)
export const provideTo2 = /*#__PURE__*/ _recurry (4) (manualProvideTo2)
export const provideTo3 = /*#__PURE__*/ _recurry (5) (manualProvideTo3)
export const provideTo4 = /*#__PURE__*/ _recurry (6) (manualProvideTo4)
export const provideTo5 = /*#__PURE__*/ _recurry (7) (manualProvideTo5)
export const provideToN = /*#__PURE__*/ _recurry (3) (manualProvideToN)

export const invoke = /*#__PURE__*/ manualInvoke

// ------ sum | applyToN ([1, 2, 3])
export const applyTo1 = /*#__PURE__*/ _recurry (2) (manualApplyTo1)
export const applyTo2 = /*#__PURE__*/ _recurry (3) (manualApplyTo2)
export const applyTo3 = /*#__PURE__*/ _recurry (4) (manualApplyTo3)
export const applyTo4 = /*#__PURE__*/ _recurry (5) (manualApplyTo4)
export const applyTo5 = /*#__PURE__*/ _recurry (6) (manualApplyTo5)
export const applyToN = /*#__PURE__*/ _recurry (2) (manualApplyToN)

// --- passTo is not called apply ...
// --- 1 | passTo (double)
export const passTo = /*#__PURE__*/ _recurry (2) (manualPassTo)
// --- ; [1, 2, 3] | passToN (sum)
export const passToN = /*#__PURE__*/ _recurry (2) (manualPassToN)
export const spreadTo = /*#__PURE__*/ manualSpreadTo

// --- flip first and second args of a curried function, even for functions with more than 2 args
// and for manually curried functions, unlike R.flip.
// --- does not work with non-curried functions.

export const flip  = /*#__PURE__*/ _recurry (3) (manualFlip)
export const flip3 = /*#__PURE__*/ _recurry (4) (manualFlip3)
export const flip4 = /*#__PURE__*/ _recurry (5) (manualFlip4)
export const flip5 = /*#__PURE__*/ _recurry (6) (manualFlip5)

// ------ sprintf
export const sprintf1 = /*#__PURE__*/ _recurry (2) (manualSprintf1)
export const sprintfN = /*#__PURE__*/ _recurry (2) (manualSprintfN)

// --- R.zip only takes two.
export const zipAll = (...xss) => {
  const ret = []
  const l = xss [0].length
  for (let i = 0; i < l; i++) ret.push (
    xss.map (xs => xs [i])
  )
  return ret
}

// ------ repeat, side

export const repeatV    = /*#__PURE__*/ _recurry (2) (manualRepeatV)
export const repeatF    = /*#__PURE__*/ _recurry (2) (manualRepeatF)
export const repeatSide = /*#__PURE__*/ _recurry (2) (manualRepeatSide)
export const timesV     = /*#__PURE__*/ _recurry (2) (manualTimesV)
export const timesF     = /*#__PURE__*/ _recurry (2) (manualTimesF)
export const timesSide  = /*#__PURE__*/ _recurry (2) (manualTimesSide)

export const getType    = /*#__PURE__*/ manualGetType
export const isType     = /*#__PURE__*/ manualIsType

export const isFunction = /*#__PURE__*/ manualIsFunction
export const isArray    = /*#__PURE__*/ manualIsArray
export const isObject   = /*#__PURE__*/ manualIsObject
export const isNumber   = /*#__PURE__*/ manualIsNumber
export const isRegExp   = /*#__PURE__*/ manualIsRegExp
export const isBoolean  = /*#__PURE__*/ manualIsBoolean
export const isString   = /*#__PURE__*/ manualIsString
export const isSymbol   = /*#__PURE__*/ manualIsSymbol

// --- assumed to be a Number.
export const isInteger = x => x === Math.floor (x)

// --- excl, so it's like ramda.
// --- note that `by` should be negative to count down -- we always add.

export const rangeFromBy     = /*#__PURE__*/ _recurry (3) (manualRangeFromBy)
export const rangeFromByAsc  = /*#__PURE__*/ _recurry (3) (manualRangeFromByAsc)
export const rangeFromByDesc = /*#__PURE__*/ _recurry (3) (manualRangeFromByDesc)
export const rangeToBy       = /*#__PURE__*/ _recurry (3) (manualRangeToBy)
// export const rangeToByAsc    = /*#__PURE__*/ _recurry (3) (manualRangeToByAsc)
// export const rangeToByDesc   = /*#__PURE__*/ _recurry (3) (manualRangeToByDesc)

export const rangeTo         = /*#__PURE__*/ rangeToBy (1)
export const rangeFrom       = /*#__PURE__*/ rangeFromBy (1)

export const compact   = /*#__PURE__*/ filter (Boolean)
export const compactOk = /*#__PURE__*/ reject (notOk)

// --- turn positional args into a list with those values.
export const list = /*#__PURE__*/ (...args) => args

// --------- lets / let...

export const letN = /*#__PURE__*/ manualLetN

export const lets = /*#__PURE__*/ manualLets
export const lets1 = /*#__PURE__*/ manualLets1
export const lets2 = /*#__PURE__*/ manualLets2
export const lets3 = /*#__PURE__*/ manualLets3
export const lets4 = /*#__PURE__*/ manualLets4
export const lets5 = /*#__PURE__*/ manualLets5
export const lets6 = /*#__PURE__*/ manualLets6
export const letV = /*#__PURE__*/ manualLetV

export const letNV = /*#__PURE__*/ _recurry (2) (manualLetNV)
export const letS = /*#__PURE__*/ _recurry (2) (manualLetS)


// --------- regex.

export const match = /*#__PURE__*/ _recurry (2) (manualMatch)

// --- input: regex.
export const xMatch             = /*#__PURE__*/ _recurry (2) (manualXMatch)
export const xMatchGlobal       = /*#__PURE__*/ _recurry (3) (manualXMatchGlobal)
// --- input: string.
export const xMatchStr          = /*#__PURE__*/ _recurry (2) (manualXMatchStr)
// --- input: string.
export const xMatchStrFlags     = /*#__PURE__*/ _recurry (3) (manualXMatchStrFlags)
export const xRegExp            = /*#__PURE__*/ manualXRegExp
export const xRegExpFlags       = /*#__PURE__*/ manualXRegExpFlags
export const xRegExpStr         = /*#__PURE__*/ manualXRegExpStr
export const xReplace           = /*#__PURE__*/ _recurry (3) (manualXReplace)
export const xReplaceStr        = /*#__PURE__*/ _recurry (3) (manualXReplaceStr)
export const xReplaceStrFlags   = /*#__PURE__*/ _recurry (4) (manualXReplaceStrFlags)

export const ifReplace          = /*#__PURE__*/ _recurry (5) (manualIfReplace)
export const ifXReplace         = /*#__PURE__*/ _recurry (5) (manualIfXReplace)
export const ifXReplaceStr      = /*#__PURE__*/ _recurry (5) (manualIfXReplaceStr)
export const ifXReplaceStrFlags = /*#__PURE__*/ _recurry (6) (manualIfXReplaceStrFlags)

// --------- new.

export const neu = /*#__PURE__*/ x => new x
export const neu1 = /*#__PURE__*/ _recurry (2) (manualNeu1)
export const neu2 = /*#__PURE__*/ _recurry (3) (manualNeu2)
export const neu3 = /*#__PURE__*/ _recurry (4) (manualNeu3)
export const neu4 = /*#__PURE__*/ _recurry (5) (manualNeu4)
export const neu5 = /*#__PURE__*/ _recurry (6) (manualNeu5)
export const neuN = /*#__PURE__*/ _recurry (2) (manualNeuN)

// --- returns a copy with prototype vals discarded.
export const discardPrototype = /*#__PURE__*/ (o) => ({ ...o })

// --- returns a copy with prototype vals surfaced.
export const flattenPrototype = /*#__PURE__*/ mergeInToM (safeObject ())

// --- using rest params to pluck it is about 4 times faster than writing the args out -- but even
// the latter can do 1e5 iterations per ms.

export const arg0 = /*#__PURE__*/ (...args) => args [0]
export const arg1 = /*#__PURE__*/ (...args) => args [1]
export const arg2 = /*#__PURE__*/ (...args) => args [2]
export const arg3 = /*#__PURE__*/ (...args) => args [3]
export const arg4 = /*#__PURE__*/ (...args) => args [4]
export const arg5 = /*#__PURE__*/ (...args) => args [5]
export const arg6 = /*#__PURE__*/ (...args) => args [6]

export const mixinPreM    = /*#__PURE__*/ _recurry (2) (manualMixinPreM )
export const mixinM       = /*#__PURE__*/ _recurry (2) (manualMixinM    )
export const mixinPreNM   = /*#__PURE__*/ _recurry (2) (manualMixinPreNM)
export const mixinNM      = /*#__PURE__*/ _recurry (2) (manualMixinNM   )

export const factoryProps = /*#__PURE__*/ _recurry (2) (manualFactoryProps)
export const factoryInit  = /*#__PURE__*/ _recurry (2) (manualFactoryInit)

export const factory = /*#__PURE__*/ factoryInit ((o, props) => {
  if (props == null) return
  for (const i in props) if (hasOwnProperty.call (props, i))
    o[i] = props[i]
})

export const factoryStatics = /*#__PURE__*/ mergeM

// ------ bind

// 'log'   | bindPropTo (console)
// console | bindProp   ('log')

// --- dies if o[prop] is not a function.
export const bindPropTo = /*#__PURE__*/ _recurry (2) (manualBindPropTo)
export const bindProp   = /*#__PURE__*/ _recurry (2) (manualBindProp)

export const bindTryPropTo = /*#__PURE__*/ _recurry (2) (manualBindTryPropTo)
export const bindTryProp   = /*#__PURE__*/ _recurry (2) (manualBindTryProp)
export const bindTryTo     = /*#__PURE__*/ _recurry (2) (manualBindTryTo)
export const bindTry       = /*#__PURE__*/ _recurry (2) (manualBindTry)

// console.log | bindTo (console)
// console     | bind   (console.log)

export const bindTo = /*#__PURE__*/ _recurry (2) (manualBindTo)
export const bind = /*#__PURE__*/ _recurry (2) (manualBind)

// --- returns a thunk representing the bind:
//     doesn't actually try to bind until that function is invoked.
export const bindLatePropTo = /*#__PURE__*/ _recurry (2) (manualBindLatePropTo)
export const bindLateProp   = /*#__PURE__*/ _recurry (2) (manualBindLateProp)

export const subtract = /*#__PURE__*/ _recurry (2) (manualSubtract)
export const subtractFrom = /*#__PURE__*/ _recurry (2) (manualSubtractFrom)
export const minus = /*#__PURE__*/ subtract
export const add = /*#__PURE__*/ _recurry (2) (manualAdd)
export const plus = /*#__PURE__*/ add
export const multiply   = /*#__PURE__*/ _recurry (2) (manualMultiply)
export const divideBy   = /*#__PURE__*/ _recurry (2) (manualDivideBy)
export const divideInto = /*#__PURE__*/ _recurry (2) (manualDivideInto)
export const modulo = /*#__PURE__*/ _recurry (2) (manualModulo)
export const moduloWholePart = /*#__PURE__*/ _recurry (2) (manualModuloWholePart)
export const toThe = /*#__PURE__*/ _recurry (2) (manualToThe)

export const T = /*#__PURE__*/ manualT
export const F = /*#__PURE__*/ manualF

export const condElse = /*#__PURE__*/ T
export const condPredicate = /*#__PURE__*/ _recurry (2) (manualCondPredicate)

export const guard = /*#__PURE__*/ condPredicate
export const guardV = /*#__PURE__*/ composeRight (always, guard)
export const otherwise = /*#__PURE__*/ condElse

export const defaultToV = /*#__PURE__*/ composeRight (always, defaultTo)

export const withFilter  = /*#__PURE__*/ mapWithFilter
export const remapKeys   = /*#__PURE__*/ mapRemapKeys
export const remapValues = /*#__PURE__*/ mapRemapValues
export const remapTuples = /*#__PURE__*/ mapRemapTuples
export const mapKeys     = /*#__PURE__*/ mapMapKeys
export const mapValues   = /*#__PURE__*/ mapMapValues
export const mapTuples   = /*#__PURE__*/ mapMapTuples

// --- @todo deprecate & move to cookbook?
export const fromPairs = /*#__PURE__*/ mapFromPairs
export const toPairs = /*#__PURE__*/ mapToPairs

// --- @todo use ownKeys?
export const keys = /*#__PURE__*/ (o) => {
  const ret = []
  for (const k in o) if (hasOwnProperty.call (o, k)) ret.push (k)
  return ret
}

// --- @todo use ownKeys?
export const values = (o) => {
  const ret = []
  for (const k in o) if (hasOwnProperty.call (o, k)) ret.push (o [k])
  return ret
}

export const againstAll = /*#__PURE__*/ _recurry (2) (manualAgainstAll)
export const againstAny = /*#__PURE__*/ _recurry (2) (manualAgainstAny)
export const allAgainst = /*#__PURE__*/ _recurry (2) (manualAllAgainst)
export const anyAgainst = /*#__PURE__*/ _recurry (2) (manualAnyAgainst)

// --- @experimental
export const bothAgainst   = (p) => (...args) => allAgainst (p) (args)
// --- @experimental
export const eitherAgainst = (p) => (...args) => anyAgainst (p) (args)

export const againstBoth = /*#__PURE__*/ _recurry (3) (manualAgainstBoth)
export const againstEither = /*#__PURE__*/ _recurry (3) (manualAgainstEither)

export const deconstruct = /*#__PURE__*/ _recurry (2) (manualDeconstruct)
export const deconstruct2 = /*#__PURE__*/ _recurry (2) (manualDeconstruct2)
export const deconstructN = /*#__PURE__*/ _recurry (2) (manualDeconstructN)

export const and    = /*#__PURE__*/ _recurry (2) (manualAnd)
export const andNot = /*#__PURE__*/ _recurry (2) (manualAndNot)
export const or     = /*#__PURE__*/ _recurry (2) (manualOr)
export const orNot  = /*#__PURE__*/ _recurry (2) (manualOrNot)

// ------ bitwise operators
export const bitwiseNot = /*#__PURE__*/ operatorBitwiseNot
export const bitwiseAnd = /*#__PURE__*/ _recurry (2) (operatorBitwiseAnd)
export const bitwiseOr = /*#__PURE__*/ _recurry (2) (operatorBitwiseOr)
export const bitwiseXor = /*#__PURE__*/ _recurry (2) (operatorBitwiseXor)
export const bitwiseLeft = /*#__PURE__*/ _recurry (2) (operatorBitwiseLeft)
export const bitwiseRight = /*#__PURE__*/ _recurry (2) (operatorBitwiseRight)
export const bitwiseRightZeroFill = /*#__PURE__*/ _recurry (2) (operatorBitwiseRightZeroFill)
export const bitwiseLeftBy = /*#__PURE__*/ _recurry (2) (operatorBitwiseLeftBy)
export const bitwiseRightBy = /*#__PURE__*/ _recurry (2) (operatorBitwiseRightBy)
export const bitwiseRightZeroFillBy = /*#__PURE__*/ _recurry (2) (operatorBitwiseRightZeroFillBy)
