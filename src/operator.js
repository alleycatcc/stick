export const bitwiseNot             = a => ~ a
export const bitwiseAnd             = a => b => a & b
export const bitwiseOr              = a => b => a | b
export const bitwiseXor             = a => b => a ^ b
export const bitwiseLeft            = a => b => a << b
export const bitwiseRight           = a => b => a >> b
export const bitwiseRightZeroFill   = a => b => a >>> b
export const bitwiseLeftBy          = b => a => a << b
export const bitwiseRightBy         = b => a => a >> b
export const bitwiseRightZeroFillBy = b => a => a >>> b
